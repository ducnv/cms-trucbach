<?php

if (!function_exists('parse_package_config')) {
    function parse_package_config($groupByKey)
    {
        $merged_arr = [];
        $config = collect(config('core'));
        foreach ($config as $item) {
            $merged_arr = array_merge($merged_arr, $item[$groupByKey]);
        }
        return collect($merged_arr)->sortBy('priority', SORT_REGULAR, true);
    }
}

if (!function_exists('annotation_reader')) {
    function annotation_reader($class, $method)
    {
        $annotationReader = new Doctrine\Common\Annotations\AnnotationReader();
        $reflectionMethod = new ReflectionMethod($class, $method);
        $methodAnnotation = $annotationReader->getMethodAnnotations($reflectionMethod);

        foreach ($methodAnnotation as $access) {
            if (isset($access->permission)) {
                return $access->permission;
            }
        }
        return null;
    }
}

if (!function_exists('has_permission')) {
    function has_permission($user_id, $permissions)
    {
        return true;
        if ($user_id == 1) {
            return true;
        }

        $role_permissions = env('APP_DEBUG')
            ? App\Models\User::findOrFail($user_id)->role->permissions
            : Cache::remember('user_permission_' . $user_id, 60, function () use ($user_id) {
                return App\Models\User::findOrFail($user_id)->role->permissions;
            });

        $permissions = explode('|', $permissions);

        foreach ($permissions as $per) {
            if (in_array($per, $role_permissions)) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('getVideoAttributes')) {
    function getVideoAttributes($filepath)
    {
        $command = 'ffmpeg -i ' . storage_path('app/public/' . $filepath) . ' -vstats 2>&1';

        $output = shell_exec($command);

        $regex_sizes = '/(\b[^0]\d+x[^0]\d+\b)/';

        try {
            preg_match($regex_sizes, $output, $regs);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
        if (!empty($regs)) {
            list($width, $height) = explode('x', reset($regs));
        } else {
            $codec = '';
            $width = '';
            $height = '';
        }

        $regex_duration = '/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/';
        if (preg_match($regex_duration, $output, $regs)) {
            $hours = $regs[1] ? $regs[1] : null;
            $mins = $regs[2] ? $regs[2] : null;
            $secs = $regs[3] ? $regs[3] : null;
            $ms = $regs[4] ? $regs[4] : null;
        }

        return [
            'width' => $width,
            'height' => $height,
            'hours' => $hours,
            'mins' => $mins,
            'secs' => $secs,
            'ms' => $ms,
        ];
    }
}
if (!function_exists('convertjsondecode')) {
    function convertjsondecode($str)
    {
        $arr = json_decode($str, true);

        return $arr;
    }
}
if (!function_exists('service_state')) {
    function service_state($value)
    {
        if ($value == 1) {
            return "Hoạt động";
        } else if ($value == 3) {
            return 'Hủy';
        }
        return "Pending";
    }
}
if (!function_exists('showTimeUnreg')) {
    function showTimeUnreg($value, $time)
    {
        if ($value == 1) {
            return "";
        } else if ($value == 3) {
            return $time;
        }

        return "";
    }
}

if (!function_exists('find_packages')) {
    function find_packages($value)
    {
        $package = \App\Models\Packages::where('code', $value)->first();

        if ($package) {
            return $package->reg_fee;
        }
        return '';
    }
}

if (!function_exists('convert_vi')) {
    function convert_vi($value)
    {

        if ($value === 'FIRST_REG') {
            return 'Đăng ký lần đầu';
        } elseif ($value === 'RE_REG') {
            return 'Đăng ký';
        } elseif ($value === 'UNREG') {
            return 'Hủy';
        } elseif ($value === 'RENEW') {
            return 'Gia hạn';
        }
        return $value;
    }
}

if (!function_exists('findMo')) {
    function findMo($value)
    {

        $mo = \App\Models\MO::where('mo.id', $value);

        $mo = $mo->leftJoin('his', function ($leftJoin) {
            $leftJoin->on('mo.trans_id', '=', 'his.trans_id');
        })->select('mo.created_time', 'mo.mo', 'his.result', 'his.c_fee', 'mo.channel', 'his.pkg_code', 'mo.mo_id')->first();

        if ($mo) {
            return $mo;
        }

        return false;
    }
}
