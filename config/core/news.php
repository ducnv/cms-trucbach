<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
        'news' => [
            'label' => 'Tin tức',
            'icon' => 'fa fa-newspaper-o',
            'url' => 'news',
            'type' => 'dropdown',
            'permission' => '',
            'priority' => 90,
            'group' => 'main.management',
            'active' => 'news/*/*',
            'child' => [
                'news' => [
                    'label' => 'Tin tức',
                    'url' => '/news',
                    'permission' => ''
                ],
                'category' => [
                    'label' => 'Chuyên mục',
                    'url' => '/news/category',
                    'permission' => ''
                ],
//                'keywords' => [
//                    'label' => 'Từ khóa',
//                    'url' => '/news/keywords',
//                    'permission' => ''
//                ]
            ],
        ],
    ],

    /**
     * List of permission. Etc: 'user: create something'
     */
    'permission' => [
        'news' => [
            'label' => 'Quản lý tin tức',
            'icon' => '',
            'permissions' => [

            ],
        ],
    ],
];
