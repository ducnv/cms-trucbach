<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
        'cskh' => [
            'label' => 'Chăm sóc khách hàng',
            'icon' => 'fa fa-gears',
            'url' => 'system',
            'type' => 'dropdown',
            'permission' => '',
            'priority' => 70,
            'group' => 'main.management',
            'active' => 'cskh/*/*',
            'child' => [
                'tra-cuu-thue-bao' => [
                    'label' => 'Tra cứu thuê bao',
                    'url' => '/cskh/tra-cuu-thue-bao',
                    'permission' => 'log_sub: access'
                ],
                'lich-su-dang-ky-huy' => [
                    'label' => 'Lịch sử đăng ký hủy',
                    'url' => '/cskh/lich-su-dang-ky-huy',
                    'permission' => 'log_register_un: access'
                ],
                'lich-su-mo' => [
                    'label' => 'Lịch sử MO',
                    'url' => '/cskh/lich-su-mo',
                    'permission' => 'log_mo: access'
                ],
                'lich-su-mt' => [
                    'label' => 'Lịch sử  MT',
                    'url' => '/cskh/lich-su-mt',
                    'permission' => 'log_mt: access'
                ],
                'tra-cuu-log-charge' => [
                    'label' => 'Lịch sử trừ cước',
                    'url' => '/cskh/lich-su-tru-cuoc',
                    'permission' => 'log_charge: access'
                ],
                'add-black-list' => [
                    'label' => 'Add black list',
                    'url' => '/system/add-black-list',
                    'permission' => 'add-black-list: access'
                ]
            ],
        ],
    ],

    /**
     * List of permission. Etc: 'user: create something'
     */
    'permission' => [
        'cskh' => [
            'label' => 'Chăm sóc khách hàng',
            'icon' => '',
            'permissions' => [
                'log_sub: access' => 'Tra cứu thuê bao',
                'log_register_un: access' => 'Lịch sử đăng ký hủy',
                'log_mo: access' => 'Lịch sử MO',
                'log_mt: access' => 'Lịch sử MT',
                'log_charge: access' => 'Lịch sử trừ cước',
                'add-black-list: access' => 'Add black list',
            ],
        ],
    ],
];
