<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
//        'system' => [
//            'label' => 'Quản trị hệ thống',
//            'icon' => 'fa fa-cogs',
//            'url' => 'system',
//            'type' => 'dropdown',
//            'permission' => '',
//            'priority' => 70,
//            'group' => 'main.management',
//            'active' => 'system/*/*',
//            'child' => [
//                'tra-cuu-thue-bao' => [
//                    'label' => 'Tra cứu thuê bao',
//                    'url' => '/system/tra-cuu-thue-bao',
//                    'permission' => 'log_sub: access'
//                ],
//                'tra-cuu-log-dang-ky' => [
//                    'label' => 'Tra cứu log đăng ký',
//                    'url' => '/system/tra-cuu-log-dang-ky',
//                    'permission' => 'log_register: access'
//                ],
//                'tra-cuu-log-huy' => [
//                    'label' => 'Tra cứu log hủy',
//                    'url' => '/system/tra-cuu-log-huy',
//                    'permission' => 'log_unregister: access'
//                ],
//                'tra-cuu-log-charge' => [
//                    'label' => 'Tra cứu log charge',
//                    'url' => '/system/tra-cuu-log-charge',
//                    'permission' => 'log_charge: access'
//                ],
//                'tra-cuu-log-mo' => [
//                    'label' => 'Tra cứu log MO',
//                    'url' => '/system/tra-cuu-log-mo',
//                    'permission' => 'log_mo: access'
//                ],
//                'tra-cuu-log-mt' => [
//                    'label' => 'Tra cứu log MT',
//                    'url' => '/system/tra-cuu-log-mt',
//                    'permission' => 'log_mt: access'
//                ],
//                'tra-cuu-log-ussd' => [
//                    'label' => 'Tra cứu log USSD',
//                    'url' => '/system/tra-cuu-log-ussd',
//                    'permission' => 'log_ussd: access'
//                ],
//                'add-black-list' => [
//                    'label' => 'Add Blacklist',
//                    'url' => '/system/add-black-list',
//                    'permission' => 'black_list: access'
//                ]
//            ],
//        ],
    ],

    /**
     * List of permission. Etc: 'user: create something'
     */
    'permission' => [
        'system' => [
            'label' => 'Quản trị hệ thống',
            'icon' => '',
            'permissions' => [
//                'log_sub: access' => 'Tra cứu thuê bao',
//                'log_register: access' => 'Xem log đăng ký',
//                'log_unregister: access' => 'Xem log hủy',
//                'log_mo: access' => 'Xem log MO',
//                'log_mt: access' => 'Xem log MT',
//                'log_ussd: access' => 'Xem log USSD',
//                'log_charge: access' => 'Xem log charge',
//                'black_list: access' => 'Xem black list',
//                'black_list_update: access' => 'Cập nhật black list',
            ],
        ],
    ],
];
