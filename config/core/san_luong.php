<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
//        'san-luong' => [
//            'label' => 'Quản trị sản lượng',
//            'icon' => 'fa fa-fw fa-list',
//            'url' => 'san-luong',
//            'type' => 'dropdown',
//            'permission' => 'sanluong: access',
//            'priority' => 100,
//            'group' => 'main.management',
//            'active' => 'san-luong/*/*',
//            'child' => [
//                'thong-ke-dich-vu' => [
//                    'label' => 'Thống kê dịch vụ',
//                    'url' => '/san-luong/thong-ke-dich-vu',
//                    'permission' => 'sanluong: access'
//                ],
////                'thong-ke-goi-viec-lam' => [
////                    'label' => 'Thống kê gói việc làm',
////                    'type' => 'dropdown',
////                    'icon' => 'fa fa-angle-double-right',
////                    'url' => '/san-luong/thong-ke-goi-viec-lam',
////                    'permission' => 'sanluong: access',
////                    'group' => 'main.management',
////                    'active' => 'san-luong/*/*',
////                    'priority' => 100,
////                    'grandChild' =>[
////                        'thong-ke-nguon-cung' => [
////                            'label' => 'Thống kê nguồn cung',
////                            'url' => '/san-luong/thong-ke-goi-viec-lam/thong-ke-nguon-cung',
////                            'permission' => 'sanluong: access'
////                        ],
////                        'thong-ke-nguon-cau' => [
////                            'label' => 'Thống kê nguồn cầu',
////                            'url' => '/san-luong/thong-ke-goi-viec-lam/thong-ke-nguon-cau',
////                            'permission' => 'sanluong: access'
////                        ],
////                    ]
////                ],
////                'thong-ke-goi-van-tai' => [
////                    'label' => 'Thống kê gói vận tải',
////                    'type' => 'dropdown',
////                    'icon' => 'fa fa-angle-double-right',
////                    'url' => '/san-luong/thong-ke-goi-van-tai',
////                    'permission' => 'sanluong: access',
////                    'group' => 'main.management',
////                    'active' => 'san-luong/*/*',
////                    'priority' => 100,
////                    'grandChild' =>[
////                        'thong-ke-nguon-cung' => [
////                            'label' => 'Thống kê nguồn cung',
////                            'url' => '/san-luong/thong-ke-goi-van-tai/thong-ke-nguon-cung',
////                            'permission' => 'sanluong: access'
////                        ],
////                        'thong-ke-nguon-cau' => [
////                            'label' => 'Thống kê nguồn cầu',
////                            'url' => '/san-luong/thong-ke-goi-van-tai/thong-ke-nguon-cau',
////                            'permission' => 'sanluong: access'
////                        ],
////                    ]
////                ],
////                'thong-ke-goi-bat-dong-san' => [
////                    'label' => 'Thống kê gói bds',
////                    'type' => 'dropdown',
////                    'icon' => 'fa fa-angle-double-right',
////                    'url' => '/san-luong/thong-ke-goi-bat-dong-san',
////                    'permission' => 'sanluong: access',
////                    'group' => 'main.management',
////                    'active' => 'san-luong/*/*',
////                    'priority' => 100,
////                    'grandChild' =>[
////                        'thong-ke-nguon-cung' => [
////                            'label' => 'Thống kê nguồn cung',
////                            'url' => '/san-luong/thong-ke-goi-bat-dong-san/thong-ke-nguon-cung',
////                            'permission' => 'sanluong: access'
////                        ],
////                        'thong-ke-nguon-cau' => [
////                            'label' => 'Thống kê nguồn cầu',
////                            'url' => '/san-luong/thong-ke-goi-bat-dong-san/thong-ke-nguon-cau',
////                            'permission' => 'sanluong: access'
////                        ],
////                    ]
////                ],
////                'bao-cao-invite' => [
////                    'label' => 'Báo cáo invite USSD',
////                    'url' => '/san-luong/bao-cao-invite-ussd',
////                    'permission' => 'sanluong: access'
////                ],
////                'bao-cao-ket-noi' => [
////                    'label' => 'Báo cáo kết nối',
////                    'url' => '/san-luong/bao-cao-ket-noi',
////                    'permission' => 'sanluong: access'
////                ],
//                'bao-cao-thue-bao-huy' => [
//                    'label' => 'Báo cáo thuê bao hủy',
//                    'url' => '/san-luong/bao-cao-thue-bao-huy',
//                    'permission' => 'sanluong: access'
//                ],
//                'bao-cao-thue-bao-pending' => [
//                    'label' => 'Báo cáo thuê bao pending',
//                    'url' => '/san-luong/bao-cao-thue-bao-pending',
//                    'permission' => 'sanluong: access'
//                ],
//                'doi-soat-san-luong' => [
//                    'label' => 'Đối soát sản lượng',
//                    'url' => '/san-luong/doi-soat-san-luong',
//                    'permission' => 'sanluong: access'
//                ],
//                'thong-ke-charge' => [
//                    'label' => 'Thống kê tỉ lệ charge',
//                    'url' => '/san-luong/thong-ke-charge',
//                    'permission' => 'sanluong: access'
//                ],
//                'theo-doi-san-luong-truyen-thong' => [
//                    'label' => 'Theo dõi sản lượng truyền thông',
//                    'url' => '/san-luong/theo-doi-san-luong-truyen-thong',
//                    'permission' => 'sanluong: access'
//                ],
//            ],
//        ],
    ],

    /**
     * List of permission. Etc: 'user: create something'
     */
    'permission' => [
        'sanluong' => [
            'label' => 'Quản trị sản lượng',
            'icon' => '',
            'permissions' => [
                'sanluong: access' => 'Truy cập khu vực quản trị sản lượng',
            ],
        ],
    ],
];
