<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
//        'trung-thuong' => [
//            'label' => 'Quản trị trúng thưởng',
//            'icon' => 'fa fa-fw fa-outdent',
//            'url' => 'trung-thuong',
//            'type' => 'dropdown',
//            'permission' => 'trungthuong: access',
//            'priority' => 90,
//            'group' => 'main.management',
//            'active' => 'trung-thuong/*/*',
//            'child' => [
//                'tra-cuu-diem-so' => [
//                    'label' => 'Tra cứu điểm số',
//                    'url' => '/trung-thuong/tra-cuu-diem-so',
//                    'permission' => 'trungthuong: access'
//                ],
//                'thue-bao-trung-thuong-ngay' => [
//                    'label' => 'Thuê bao trúng thưởng ngày',
//                    'url' => '/trung-thuong/thue-bao-trung-thuong-ngay',
//                    'permission' => 'trungthuong: access'
//                ],
//                'thue-bao-trung-thuong-thang' => [
//                    'label' => 'Thuê bao trúng thưởng tháng',
//                    'url' => '/trung-thuong/thue-bao-trung-thuong-thang',
//                    'permission' => 'trungthuong: access'
//                ],
//                'bao-cao-diem-so' => [
//                    'label' => 'Báo cáo điểm số',
//                    'url' => '/trung-thuong/bao-cao-diem-so',
//                    'permission' => 'trungthuong: access'
//                ],
//                'thue-bao-trung-thuong-nhap-ho-so' => [
//                    'label' => 'Thuê bao trúng thưởng nhập hồ sơ',
//                    'url' => '/trung-thuong/thue-bao-trung-thuong-nhap-ho-so',
//                    'permission' => 'trungthuong: access'
//                ],
//                'thue-bao-trung-thuong-gia-han-3-ngay' => [
//                    'label' => 'Thuê bao trúng thưởng gia hạn 3 ngày',
//                    'url' => '/trung-thuong/thue-bao-trung-thuong-gia-han-3-ngay',
//                    'permission' => 'trungthuong: access'
//                ],
//                'thue-bao-trung-thuong-tap-pending' => [
//                    'label' => 'Thuê bao trúng thưởng tập pending',
//                    'url' => '/trung-thuong/thue-bao-trung-thuong-tap-pending',
//                    'permission' => 'trungthuong: access'
//                ],
//                'ket-noi-nhanh-tay-tien-ve-tui-ngay' => [
//                    'label' => 'Ket noi nhanh tay- Tien ve tui ngay',
//                    'url' => '/trung-thuong/ket-noi-nhanh-tay-tien-ve-tui-ngay',
//                    'permission' => 'trungthuong: access'
//                ],
//            ],
//        ],
    ],

    /**
     * List of permission. Etc: 'user: create something'
     */
    'permission' => [
        'trungthuong' => [
            'label' => 'Quản trị trúng thưởng',
            'icon' => '',
            'permissions' => [
                'trungthuong: access' => 'Truy cập khu vực quản trị trúng thưởng',
            ],
        ],
    ],
];
