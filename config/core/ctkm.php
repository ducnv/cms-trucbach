<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
//        'ctkm' => [
//            'label' => 'Quản trị CTKM',
//            'icon' => 'fa fa-calendar',
//            'url' => 'ctkm',
//            'type' => 'dropdown',
//            'permission' => 'ctkm: access',
//            'priority' => 80,
//            'group' => 'main.management',
//            'active' => 'ctkm/*/*',
//            'child' => [
//                'chuong-trinh-khuyen-mai' => [
//                    'label' => 'Chương trình khuyến mãi',
//                    'url' => '/ctkm/chuong-trinh-khuyen-mai',
//                    'permission' => 'ctkm: access'
//                ],
//                'thue-bao-dang-ky' => [
//                    'label' => 'Thuê bao đăng ký',
//                    'url' => '/ctkm/thue-bao-dang-ky',
//                    'permission' => 'ctkm: access'
//                ],
//                'thue-bao-trung-thuong' => [
//                    'label' => 'Thuê bao trúng thưởng',
//                    'url' => '/ctkm/thue-bao-trung-thuong',
//                    'permission' => 'ctkm: access'
//                ],
////                'tinh-trang-trao-thuong' => [
////                    'label' => 'Tình trạng trao thưởng',
////                    'url' => '/ctkm/tinh-trang-trao-thuong',
////                    'permission' => 'ctkm: access'
////                ],
//            ],
//        ],
    ],

    /**
     * List of permission. Etc: 'ctkm: create something'
     */
    'permission' => [
        'ctkm' => [
            'label' => 'Quản trị CTKM',
            'icon' => '',
            'permissions' => [
                'ctkm: access' => 'Truy cập khu vực quản trị CTKM',
            ],
        ],
    ],
];
