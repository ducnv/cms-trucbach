<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
        'setting' => [
            'label' => 'Cài đặt',
            'icon' => 'fa fa-gear',
            'url' => 'news',
            'type' => 'dropdown',
            'permission' => '',
            'priority' => 70,
            'group' => 'main.management',
            'active' => 'setting/*/*',
            'child' => [
                'setting' => [
                    'label' => 'Cấu hình',
                    'url' => '/setting',
                    'permission' => ''
                ]
            ],
        ],
    ],

    /**
     * List of permission. Etc: 'user: create something'
     */
    'permission' => [
        'news' => [
            'label' => 'Quản lý cài đặt',
            'icon' => '',
            'permissions' => [

            ],
        ],
    ],
];
