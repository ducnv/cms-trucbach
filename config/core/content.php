<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
        'noi-dung' => [
            'label' => 'Quản trị nội dung',
            'icon' => 'fa fa-files-o',
            'url' => 'noi-dung',
            'type' => 'dropdown',
            'permission' => 'content: access',
            'priority' => 80,
            'group' => 'main.management',
            'active' => 'noi-dung/*/*',
            'child' => [
                'mt-noi-dung' => [
                    'label' => 'MT nội dung',
                    'url' => '/noi-dung/quan-ly-mt-noi-dung',
                    'permission' => 'content: access'
                ],
                'tin-nhan-thue-bao' => [
                    'label' => 'Tin nhắn thuê bao',
                    'url' => '/noi-dung/tin-nhan-thue-bao',
                    'permission' => 'content: access'
                ],
//                'thong-ke-cau-hoi' => [
//                    'label' => 'Thống kê câu hỏi',
//                    'url' => '/noi-dung/thong-ke-cau-hoi',
//                    'permission' => 'content: access'
//                ],
//                'quan-ly-cau-hoi' => [
//                    'label' => 'Quản lý câu hỏi',
//                    'url' => '/noi-dung/quan-ly-cau-hoi',
//                    'permission' => 'content: access'
//                ],
//                'quan-ly-chuyen-muc' => [
//                    'label' => 'Quản lý chuyên mục',
//                    'url' => '/noi-dung/quan-ly-chuyen-muc',
//                    'permission' => 'content: access'
//                ]
            ],
        ],
    ],

    /**
     * List of permission. Etc: 'user: create something'
     */
    'permission' => [
        'content' => [
            'label' => 'Quản trị nội dung',
            'icon' => '',
            'permissions' => [
                'content: access' => 'Truy cập khu vực quản trị nội dung',
            ],
        ],
    ],
];
