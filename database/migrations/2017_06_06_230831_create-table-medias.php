<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMedias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('media_storage');

        Schema::create('media_storage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('media_type');
            $table->string('uuid')->unique();
            $table->string('name');
            $table->string('alt')->nullable();
            $table->string('caption')->nullable();
            $table->string('link')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('file_name');
            $table->string('file_path');
            $table->string('file_url');
            $table->string('file_size')->nullable();
            $table->string('extension');
            $table->string('mime');
            $table->string('height')->nullable();
            $table->string('width')->nullable();
            $table->string('duration')->nullable();
            $table->string('orientation')->nullable();
            $table->string('version')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_storage');
    }
}
