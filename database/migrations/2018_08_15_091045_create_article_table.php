<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('article');

        Schema::create('article', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('category_id');
            $table->string('status');
            $table->string('title');
            $table->string('slug');
            $table->string('thumb')->nullable();
            $table->mediumText('description')->nullable();
            $table->text('body');
            $table->mediumText('tags')->nullable();
            $table->integer('one_day_stats')->defautl(1);
            $table->integer('seven_days_stats')->defautl(1);
            $table->integer('thirty_days_stats')->defautl(1);
            $table->integer('all_time_stats')->defautl(1);
            $table->text('raw_stats')->nullable();
            $table->timestamp('featured_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
