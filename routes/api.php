<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/profile', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api']], function () {
    //Setting
    Route::get('/setting', 'SettingController@apiGetSetting');
    Route::put('/setting/update', 'SettingController@apiUpdateSetting');
    // USER
    Route::get('/users', 'UserController@apiGetUsers');
    Route::post('/user', 'UserController@apiCreateUser');
    Route::put('/user/{id}', 'UserController@apiUpdateUser');
    Route::get('/roles', 'UserController@apiGetRoles');
    Route::get('/permissions', 'UserController@getPermissions');
    Route::put('/roles/update-permissions', 'UserController@roleUpdatePermissions');

    //carrer
    Route::get('/carrer', 'CarrerController@apiGetCarrer');
    Route::post('/carrer/create', 'CarrerController@postCarrer');
    Route::put('/carrer/update', 'CarrerController@putCarrer');
    Route::delete('/carrer/delete/{id}', 'CarrerController@deleteCarrer');

    //salary
    Route::get('/salary', 'SalaryController@apiGetSalary');
    Route::post('/salary/create', 'SalaryController@postSalary');
    Route::put('/salary/update', 'SalaryController@putSalary');
    Route::delete('/salary/delete/{id}', 'SalaryController@deleteSalary');

    //level
    Route::get('/level', 'LevelController@apiGetLevel');
    Route::post('/level/create', 'LevelController@postLevel');
    Route::put('/level/update', 'LevelController@putLevel');
    Route::delete('/level/delete/{id}', 'LevelController@deleteLevel');

    //experience
    Route::get('/experience', 'ExperienceController@apiGetExperience');
    Route::post('/experience/create', 'ExperienceController@postExperience');
    Route::put('/experience/update', 'ExperienceController@putExperience');
    //position
    Route::get('/position', 'PositionController@apiGetPosition');
    Route::post('/position/create', 'PositionController@postPosition');
    Route::put('/position/update', 'PositionController@putPosition');
    //industrialzone
    Route::get('/industrialzone', 'IndustrialZoneController@apiGetIndustrialZone');
    Route::post('/industrialzone/create', 'IndustrialZoneController@postIndustrialZone');
    Route::put('/industrialzone/update', 'IndustrialZoneController@putIndustrialZone');
    Route::get('/industrialzone/province', 'IndustrialZoneController@apiGetProvince');

    //industrialzone
    Route::get('/company', 'CompanyController@apiGetCompany');
    Route::post('/company/create', 'CompanyController@postCompany');
    Route::put('/company/update', 'CompanyController@putCompany');
    //category
    Route::get('/category', 'CategoryController@apiGetCategory');
    Route::post('/category/create', 'CategoryController@postCategory');
    Route::put('/category/update', 'CategoryController@putCategory');

    //keywords
    Route::get('/keyword', 'CategoryController@apiGetKeywords');
    Route::post('/keyword/create', 'CategoryController@postKeywords');
    Route::put('/keyword/update', 'CategoryController@putKeywords');

    //news
    Route::get('/news', 'NewsController@apiGetNews');
    Route::post('/news/create', 'NewsController@postNews');
    Route::put('/news/update', 'NewsController@putNews');
    Route::get('/news/category', 'NewsController@apiGetCategory');
    Route::get('/news/keywords', 'NewsController@apiGetKeywords');
    //pages
    Route::get('/pages', 'PagesController@apiGetPages');
    Route::post('/pages/create', 'PagesController@postPages');
    Route::put('/pages/update', 'PagesController@putPages');
    //seeker
    Route::get('/seeker', 'SeekerController@apiGetSeeker');
    Route::post('/seeker/create', 'SeekerController@postSeeker');
    Route::put('/seeker/update', 'SeekerController@putSeeker');


    //jobpost
    Route::get('/job/type', 'JobPostController@apiGetType');
    Route::get('/job/level', 'JobPostController@apiGetLevel');
    Route::get('/job/exper', 'JobPostController@apiGetExper');
    Route::get('/job/salary', 'JobPostController@apiGetSalary');
    Route::get('/job/position', 'JobPostController@apiGetPosition');
    Route::get('/job/carrer', 'JobPostController@apiGetCarrer');
    Route::get('/job/company', 'JobPostController@apiGetCompany');
    Route::post('/job/create', 'JobPostController@postJobPost');
    Route::put('/job/update', 'JobPostController@putJobPost');
    Route::get('/job', 'JobPostController@apiGetJob');

    Route::get('/media', 'MediaController@getMedia');
    Route::post('media', 'MediaController@createMedia')->name('api.post_media');
    Route::put('media/{id}', 'MediaController@updateMedia')->name('api.put_media');
    Route::post('mediacloselogo', 'MediaController@mediaCloseLogo')->name('api.media_close_logo');
    Route::post('delete/media', 'MediaController@deleteMedia')->name('api.delete_media');

    // CRAWLER
    Route::get('/crawl', 'CrawlerController@apiGetCrawl');
    Route::get('/crawl/province', 'CrawlerController@apiGetProvince');
    Route::get('/crawl/district', 'CrawlerController@apiGetDistrict');
    Route::get('/crawl/industrial-zone', 'CrawlerController@apiGetIndustrialZone');
    Route::get('/crawl/carrers', 'CrawlerController@apiGetCarrer');
    Route::get('/crawl/keywords', 'CrawlerController@apiGetKeywords');
    Route::get('/crawl/site-crawler', 'CrawlerController@apiGetSiteCrawler');
    Route::post('/crawl/create-site-crawler', 'CrawlerController@postSiteCrawler');
    Route::put('/crawl/update-site-crawler', 'CrawlerController@putSiteCrawler');
    Route::post('/crawl/create-category', 'CrawlerController@postCategory');
    Route::put('/crawl/update-category', 'CrawlerController@putCategory');
    Route::get('/crawl/category', 'CrawlerController@apiGetCategory');

    Route::get('/crawl/company', 'CrawlerController@apiGetCompany');

    Route::put('/crawl/update-job', 'CrawlerController@putJobPost');
    Route::put('/crawl/create-job', 'CrawlerController@postJobPost');

    Route::put('/crawl/update-company', 'CrawlerController@putCompany');
    Route::put('/crawl/create-company', 'CrawlerController@postCompany');

    Route::get('/crawl/site-client', 'CrawlerController@apiGetSiteClient');
    Route::post('/crawl/create-site-client', 'CrawlerController@postSiteClient');
    Route::put('/crawl/update-site-client', 'CrawlerController@putSiteClient');
    Route::put('/crawl/add-category-siteclient', 'CrawlerController@putCategorySiteClient');

    Route::post('/crawl/publish-job', 'CrawlerController@postPublishJob');
    Route::delete('/crawl/delete-job/{id}', 'CrawlerController@deleteJob');

    // quantity
    Route::get('/quantity/statistic', 'QuantityController@apiStatistic');
    Route::get('/quantity/statistic-invite-ussd', 'QuantityController@apiStatisticInviteUssd');
    Route::get('/quantity/statistic-question-ussd', 'QuantityController@apiStatisticQuestion');
    Route::get('/quantity/statistic-pending-ussd', 'QuantityController@apiStatisticPendingUssd');
    Route::get('/quantity/statistic-cancel-ussd', 'QuantityController@apiStatisticCancelUssd');
    Route::get('/quantity/statistic-control', 'QuantityController@apiStatisticControl');
    Route::get('/quantity/statistic-charge', 'QuantityController@apiStatisticCharge');
    Route::get('/quantity/statistic-media', 'QuantityController@apiStatisticMedia');

    //tool
    Route::get('/tool/thong-ke-gui-loi-moi', 'ToolController@apiThongKeGuiLoiMoi');
    Route::get('/tool/bao-cao-tong-quan-thang', 'ToolController@apiBaoCaoTongQuanThang');
    Route::get('/tool/bao-cao-tong-quan-ngay', 'ToolController@apiBaoCaoTongQuanNgay');
    Route::get('/tool/bao-cao-gui-loi-moi', 'ToolController@apiBaoCaoGuiLoiMoi');
    Route::get('/tool/thong-ke-thue-bao-pending', 'ToolController@apiThongKeThueBaoPending');
    Route::get('/tool/doi-soat-theo-thang', 'ToolController@apiDoiSoatTheoThang');
    Route::get('/tool/ti-le-charge-dich-vu', 'ToolController@apiTiLeChargeDichVu');
    Route::get('/tool/theo-doi-san-luong-dang-ki-theo-ngay', 'ToolController@apiTheoDoiSanluongDangKi');


    Route::get('/export/quantity/statistic', 'QuantityController@apiExportStatistic');
    Route::get('/export/quantity/statistic-invite-ussd', 'QuantityController@apiExportStatisticInviteUssd');
    Route::get('/export/quantity/statistic-question-ussd', 'QuantityController@apiExportStatisticQuestion');
    Route::get('/export/quantity/statistic-pending-ussd', 'QuantityController@apiExportStatisticPendingUssd');
    Route::get('/export/quantity/statistic-cancel-ussd', 'QuantityController@apiExportStatisticCancelUssd');
    Route::get('/export/quantity/statistic-control', 'QuantityController@apiExportStatisticControl');

    // Content
    Route::get('/content', 'ContentController@apiGetContent');
    Route::post('/content/create', 'ContentController@apiPostContent');
    Route::put('/content/update', 'ContentController@apiPutContent');

    Route::get('/content/packages', 'ContentController@apiGetPackages');

    Route::get('/content/messages', 'ContentController@apiGetMessage');
    Route::post('/messages/create', 'ContentController@apiPostMessage');
    Route::put('/messages/update', 'ContentController@apiPutMessage');

//    Route::get('/content/category', 'ContentController@apiGetCategory');
//    Route::post('/category/create', 'ContentController@apiPostCategory');
//    Route::put('/category/update', 'ContentController@apiPutCategory');

    Route::get('/content/questions', 'ContentController@apiGetQuestion');
    Route::post('/questions/create', 'ContentController@apiPostQuestion');
    Route::put('/questions/update', 'ContentController@apiPutQuestion');

    Route::put('/questions/update-timeout', 'ContentController@apiPutTimeoutQuestion');

    Route::get('/content/cat-all', 'ContentController@apiGetCatAll');

    // Bonus
    Route::get('/bonus/sub-bonus', 'ManageBonusController@apiGetSubBonus');
    Route::get('/bonus/sub-bonus-current', 'ManageBonusController@apiGetSubBonusCurrent');
    Route::get('/bonus/sub-bonus-free', 'ManageBonusController@apiGetSubBonusFree');
    Route::get('/bonus/report-point', 'ManageBonusController@apiGetReportPoint');
    Route::get('/export/sub-bonus', 'ManageBonusController@apiExportSubBonus');
    Route::get('/export/sub-bonus-current', 'ManageBonusController@apiExportSubBonusCurrent');
    Route::get('/bonus/search-point', 'ManageBonusController@apiGetSearchPoint');
    Route::get('/bonus/day', 'ManageBonusController@apiGetBonusDay');
    Route::get('/bonus/month', 'ManageBonusController@apiGetBonusMonth');

    Route::get('/bonus/thue-bao-trung-thuong-nhap-ho-so', 'ManageBonusController@apiManageNhapHS');
    Route::get('/bonus/thue-bao-trung-thuong-gia-han-3-ngay', 'ManageBonusController@apiManage3Day');
    Route::get('/bonus/thue-bao-trung-thuong-tap-pending', 'ManageBonusController@apiManagePending');
    Route::get('/bonus/ket-noi-nhanh-tay-tien-ve-tui-ngay', 'ManageBonusController@apiMoneyNow');


    //System
    Route::get('/system/search-sub', 'SystemController@apiGetInfoSub');
    Route::get('/system/log-ussd', 'SystemController@apiGetLogUssd');
    Route::get('/system/log-mo', 'SystemController@apiGetLogMo');
    Route::get('/system/log-mt', 'SystemController@apiGetLogMt');
    Route::get('/system/log-charge', 'SystemController@apiGetLogCharge');
    Route::get('/system/log-register-un', 'SystemController@apiGetLogRegisterUn');
    Route::get('/system/log-unregister', 'SystemController@apiGetLogUnRegister');
    Route::get('/system/blacklist', 'SystemController@apiGetBlackList');
    Route::post('/blacklist/create', 'SystemController@apiPostBlackList');
    Route::put('/blacklist/update', 'SystemController@apiPutBlackList');
    Route::delete('/blacklist/delete', 'SystemController@apiDeleteBlackList');
    Route::post('/config-invite/create', 'SystemController@apiPostConfigInvite');
    Route::post('/config-invite/rollback', 'SystemController@apiRollBackConfig');
    Route::get('/system/config-invite', 'SystemController@apiGetConfigInvite');
    Route::get('/system/view-config-invite', 'SystemController@apiViewFileConfigInvite');

    Route::get('/export/log-charge', 'SystemController@apiExportLogCharge');
    Route::get('/export/log-mo', 'SystemController@apiExportLogMo');
    Route::get('/export/log-mt', 'SystemController@apiExportLogMt');
    Route::get('/export/log-register', 'SystemController@apiExportLogRegister');
    Route::get('/export/log-unregister', 'SystemController@apiExportLogUnRegister');
    Route::get('/export/log-ussd', 'SystemController@apiExportLogUssd');

    //post
    Route::get('/post/postSupply', 'PostController@apiGetPostSupply');

    Route::get('/post/hoSoDemandsources', 'PostController@apiGetHoSoDemandsources');
    Route::get('/post/hoSoSupply', 'PostController@apiGetHoSoSupply');
    Route::put('/post/changeStatus', 'PostController@apiChangeStatus');
    Route::put('/post/changeStatusDeman', 'PostController@apiChangeStatusDeman');

    //ctkm
    Route::get('/ctkm/showCtkm', 'CtkmController@apiGetCtkm');
    Route::get('/ctkm/allCtkm', 'CtkmController@allCtkm');
    Route::get('/ctkm/activeProfile', 'CtkmController@activeProfile');
    Route::get('/ctkm/winnerCTKM', 'CtkmController@winnerCTKM');

    Route::post('/ctkm/addCtkm', 'CtkmController@apiAddCtkm');
    Route::post('/ctkm/editCtkm', 'CtkmController@apiEditCtkm');
    Route::post('/ctkm/deleteCtkm', 'CtkmController@apiDeleteCtkm');
    Route::post('/ctkm/reward', 'CtkmController@apiReward');

});
