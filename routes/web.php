<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();
// overwrite some route
Route::get('/register', function () {
    return redirect('/');
});
Route::post('/register', function () {
    return redirect('/');
});

Route::post('/test', 'HomeController@test')->name('test');

Route::group(['middleware' => ['auth', 'permissions']], function () {
    // User
    Route::get('/user/manage', 'UserController@manageUser');
    Route::get('/user/roles', 'UserController@manageRoles');

    //carrer
    Route::get('/job/carrer', 'CarrerController@manage');
    //salary
    Route::get('/job/salary', 'SalaryController@manage');
    //level
    Route::get('/job/level', 'LevelController@manage');
    //experience
    Route::get('/job/experience', 'ExperienceController@manage');
    //position
    Route::get('/job/position', 'PositionController@manage');
    //industrialzone
    Route::get('/job/industrialzone', 'IndustrialZoneController@manage');
    //company
    Route::get('/job/company', 'CompanyController@manage');
    //jobpost
    Route::get('/job/create', 'JobPostController@getCreate');
    Route::get('/job/manage', 'JobPostController@manage');
    Route::get('/job/edit/{id}', 'JobPostController@getEdit');

    Route::get('/media', 'MediaController@index');

    //category
    Route::get('/news/category', 'CategoryController@manage');

    //keywords
    Route::get('/news/keywords', 'CategoryController@keywords');

    //news
    Route::get('/news', 'NewsController@manage');
    Route::get('/news/create', 'NewsController@getCreate');
    Route::get('/news/edit/{id}', 'NewsController@getEdit');

    //pages
    Route::get('/pages', 'PagesController@manage');
    Route::get('/pages/create', 'PagesController@getCreate');
    Route::get('/pages/edit/{id}', 'PagesController@getEdit');

    // Setting
    Route::get('/setting', 'SettingController@manage');

    // Seeker
    Route::get('/seeker', 'SeekerController@manage');
    Route::get('/seeker/edit/{id}', 'SeekerController@getEdit');


    //Crawler
    Route::get('/crawl/manage', 'CrawlerController@manageCrawl');

    Route::get('/crawl/site-crawler', 'CrawlerController@siteCrawler');

    Route::get('/crawl/category', 'CrawlerController@category');

    Route::get('/crawl/company', 'CrawlerController@company');

    Route::get('/crawl/edit-company/{id}', 'CrawlerController@editCompany');

    Route::get('/crawl/edit-job/{id}', 'CrawlerController@editJobPost');

    Route::get('/crawl/site-client', 'CrawlerController@siteClient');

    Route::get('/crawl/add-category-siteclient/{id}', 'CrawlerController@addCategorySiteClient');

    Route::get('/user/profile', 'UserController@profileUser');

    Route::get('/san-luong/thong-ke-dich-vu', 'QuantityController@thongKeDichVu');
    Route::get('/san-luong/bao-cao-invite-ussd', 'QuantityController@inviteUssd');
    Route::get('/san-luong/bao-cao-cau-hoi-ussd', 'QuantityController@cauHoiUssd');
    Route::get('/san-luong/bao-cao-thue-bao-huy', 'QuantityController@thueBaoHuy');
    Route::get('/san-luong/bao-cao-thue-bao-pending', 'QuantityController@thueBaoPending');
    Route::get('/san-luong/doi-soat-san-luong', 'QuantityController@doiSoatSanLuong');
    Route::get('/san-luong/thong-ke-charge', 'QuantityController@thongKeCharge');
    Route::get('/san-luong/theo-doi-san-luong-truyen-thong', 'QuantityController@theoDoiMedia');

    // Tool
    Route::get('/tool/thong-ke-gui-loi-moi', 'ToolController@thongKeGuiLoiMoi');
    Route::get('/tool/bao-cao-tong-quan-thang', 'ToolController@baoCaoTongQuanThang');
    Route::get('/tool/bao-cao-tong-quan-ngay', 'ToolController@baoCaoTongQuanNgay');
    Route::get('/tool/bao-cao-gui-loi-moi', 'ToolController@baoCaoGuiLoiMoi');
    Route::get('/tool/thong-ke-thue-bao-pending', 'ToolController@thongKeThueBaoPending');
    Route::get('/tool/doi-soat-theo-thang', 'ToolController@doiSoatTheoThang');
    Route::get('/tool/ti-le-charge-dich-vu', 'ToolController@tiLeChargeDichVu');
    Route::get('/tool/theo-doi-san-luong-dang-ki-theo-ngay', 'ToolController@theoDoiSanluongDangKi');

    // Content
    Route::get('/noi-dung/quan-ly-mt-noi-dung', 'ContentController@manageContent');
    Route::get('/noi-dung/tin-nhan-thue-bao', 'ContentController@manageMessages');
    Route::get('/noi-dung/thong-ke-cau-hoi', 'ContentController@thongKeCauHoi');
    Route::get('/noi-dung/quan-ly-cau-hoi', 'ContentController@manageQuestion');
    Route::get('/noi-dung/quan-ly-chuyen-muc', 'ContentController@manageCategory');
    Route::get('/noi-dung/tao-cau-hoi', 'ContentController@createQuestion');

    //Bonus
    Route::get('/trung-thuong/thue-bao-trung-thuong', 'ManageBonusController@manageSubBonus');
    Route::get('/trung-thuong/diem-so-hien-tai', 'ManageBonusController@manageSubBonusCurrent');
    Route::get('/trung-thuong/bao-cao-diem-so', 'ManageBonusController@manageReportPoint');
    Route::get('/trung-thuong/diem-so-dang-ky-mien-phi-hom-truoc', 'ManageBonusController@managePointFree');
    Route::get('/trung-thuong/tra-cuu-diem-so', 'ManageBonusController@searchPoint');
    Route::get('/trung-thuong/thue-bao-trung-thuong-ngay', 'ManageBonusController@manageSubBonusDay');
    Route::get('/trung-thuong/thue-bao-trung-thuong-thang', 'ManageBonusController@manageSubBonusMonth');
    Route::get('/trung-thuong/thue-bao-trung-thuong-nhap-ho-so', 'ManageBonusController@manageNhapHS');
    Route::get('/trung-thuong/thue-bao-trung-thuong-gia-han-3-ngay', 'ManageBonusController@manage3Day');
    Route::get('/trung-thuong/thue-bao-trung-thuong-tap-pending', 'ManageBonusController@managePending');
    Route::get('/trung-thuong/ket-noi-nhanh-tay-tien-ve-tui-ngay', 'ManageBonusController@moneyNow');


    //System
    Route::get('/cskh/tra-cuu-thue-bao', 'SystemController@searchSub')->name('traCuuThueBao');



    Route::get('/cskh/lich-su-dang-ky-huy', 'SystemController@logRegisterUn')->name('lichSuDangKyHuy');
    Route::get('/cskh/thoi-gian-giu-loc', 'SystemController@thoiGianGiuLoc');
    Route::get('/cskh/thue-bao-giu-loc-hien-tai', 'SystemController@thueBaoGiuLoc');
    Route::get('/cskh/lich-su-tru-cuoc', 'SystemController@logCharge')->name('lichSuTruCuoc');
    Route::get('/cskh/lich-su-mo', 'SystemController@logMo');
    Route::get('/cskh/lich-su-mt', 'SystemController@logMt');
    Route::get('/cskh/thong-tin-dich-vu', 'SystemController@informationServices')->name('thongTinDichVu');
    Route::get('/cskh/lich-su-momt', 'SystemController@logMoMt')->name('lichSuMoMt');
    Route::get('/cskh/lich-su-su-dung-nguon-cau', 'SystemController@logUseDeman')->name('lichSuSuDungNguonCau');
    Route::get('/cskh/lich-su-su-dung-nguon-cung', 'SystemController@logUseSupply')->name('lichSuSuDungNguonCung');
//    Route::get('/system/tra-cuu-log-ussd', 'SystemController@logUssd');
    Route::get('/system/add-black-list', 'SystemController@blackList');
//    Route::get('/system/cau-hinh-moi-ussd', 'SystemController@configInvite');

    //Post
    Route::get('/tin-dang/trang-thai-tin-nguon-cung', 'PostController@changeStatusSupply')->name('changeStatusSupply');
    Route::get('/tin-dang/ho-so-nguon-cau', 'PostController@hoSoDemandsources')->name('hoSoDemandsources');
    Route::get('/tin-dang/ho-so-nguon-cung', 'PostController@hoSoSupply')->name('hoSoSupply');

    Route::get('/tin-dang/trang-thai-tin-nguon-cung/dang-tin','PostController@getAddPost');
//    Route::post('/checkPackage','ProfileController@checkPackage')->name('profile.checkPackage');
    Route::post('/ckeckProvince','PostController@ckeckProvince')->name('profile.ckeckProvince');
    Route::post('/ckeckSDT','PostController@ckeckSDTSupply')->name('profile.ckeckSDT');

    Route::post('/ckeckHSSDTSupply','PostController@ckeckHSSDTSupply')->name('profile.ckeckHSSDTSupply');

    Route::post('/ckeckHSSDTDeman/{PKGCODE}','PostController@ckeckHSSDTDeman')->name('profile.ckeckHSSDTDeman');

    Route::post('/postAddPost/dang-tin','PostController@postAddPost')->name('post.postAddPost');


    Route::get('/cap-nhat-tin-dang/{slug}','PostController@getEditPost')->name('post.getEditPost');
    Route::post('/cap-nhat-tin-dang/{slug}','PostController@postEditPost')->name('post.postEditPost');

    Route::group(['prefix' => 'profile'],function (){
        Route::get('/ho-so-viec-lam','ProfileController@jobProfile')->name('profile.jobProfile');
        Route::get('/sua-ho-so-viec-lam/{sdt}','ProfileController@jobProfileEdit')->name('profile.jobProfileEdit');

        Route::get('/ho-so-bds','ProfileController@bdsProfile')->name('profile.bdsProfile');
        Route::get('/sua-ho-so-bds/{sdt}','ProfileController@bdsProfileEdit')->name('profile.bdsProfileEdit');

        Route::get('/ho-so-giao-thong','ProfileController@transportProfile')->name('profile.transportProfile');
        Route::get('/sua-ho-so-giao-thong/{sdt}','ProfileController@transportProfileEdit')->name('profile.transportProfileEdit');

        Route::post('/profileDemandSources','ProfileController@profileDemandSources')->name('profile.profileDemandSources');
        Route::post('/editDemandSources','ProfileController@editDemandSources')->name('profile.editDemandSources');
//
        Route::get('/ho-so','ProfileController@getProfileSupply')->name('profile.getProfileSupply');
        Route::post('/profileSupply','ProfileController@profileSupply')->name('profile.profileSupply');
//
        Route::get('/sua-ho-so/{sdt}','ProfileController@getProfileSupplyEdit')->name('profile.getProfileSupplyEdit');
        Route::post('/profileSupplyEdit','ProfileController@profileSupplyEdit')->name('profile.profileSupplyEdit');
//
        Route::post('/checkPackage','ProfileController@checkPackage')->name('profile.checkPackage');

    });
    Route::group(['prefix' => 'ctkm'],function (){
        Route::get('/chuong-trinh-khuyen-mai','CtkmController@showCtkm')->name('ctkm.showCtkm');
        Route::get('/thue-bao-dang-ky','CtkmController@TBDKCtkm')->name('ctkm.TBDKCtkm');
        Route::get('/thue-bao-trung-thuong','CtkmController@TBTTCtkm')->name('ctkm.TBTTCtkm');
        Route::get('/tinh-trang-trao-thuong','CtkmController@TTTTCtkm')->name('ctkm.TTTTCtkm');
    });


});
