<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $level;

    public $introLines = [];

    public $outroLines = [];
    public $actionUrl;
    public $actionText;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $password)
    {
        $this->level = 'success';
        $this->introLines[] = 'Tài khoản của bạn đã được khởi tạo thành công. Vui lòng đăng nhập bằng cách nhấn vào nút Đăng nhập bên dưới';
        $this->introLines[] = 'Sử dụng tài khoản sau để đăng nhập: **' . $user->email . '/' . $password . '**';
        $this->outroLines[] = 'Vui lòng thay đổi mật khẩu mặc định ngay lần đầu tiên đăng nhập.';
        $this->actionUrl = env('APP_URL') . '/login';
        $this->actionText = 'Đăng nhập';
        $this->subject = 'Tài khoản mới ' . env('APP_NAME');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->markdown('email.user_created');
    }
}
