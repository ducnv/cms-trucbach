<?php

namespace App\Jobs;

use function GuzzleHttp\Psr7\str;
use Log;
use File;
use Image;
use Storage;
use App\SystemEvents;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ResizeImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $quality = 85;

    public $path;
    public $link;
    public $orientation;
    public $version = ['640x480', '640x360', '200x200'];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($path, $orientation)
    {
        $this->path = $path;
        $this->orientation = $orientation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->version as $ver) {
            list($width, $height) = explode('x', $ver);
            $this->resizeImage($this->path, $width, $height, $this->orientation);
        }
    }

    private function resizeImage($path, $width, $height, $orientation)
    {
        try {
            $image = Image::make(storage_path('app/public/' . $path));

            $image->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $image->fit($width, $height)->save(storage_path('app/public/' . dirname($path) . '/' . $image->filename . '-' . $width . 'x' . $height . '.png'), $this->quality);

        } catch (\Exception $ex) {

        }
    }
}
