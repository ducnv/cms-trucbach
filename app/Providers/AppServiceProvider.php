<?php

namespace App\Providers;

use Auth;
//use Horizon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        Horizon::auth(function ($request) {
//            return Auth::check() && auth()->user()->id === 1;
//        });
    }
}
