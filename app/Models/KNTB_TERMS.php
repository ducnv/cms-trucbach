<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class KNTB_TERMS extends Model
{
    protected $table = 'KNTB_TERMS';
    protected $primaryKey = 'ID';

    protected $guarded = [];
    public $timestamps = false;

    public function showPackage($package)
    {
        if ($package) {
            $exigency = self::select('name', 'id')->where('taxonomy', Config::get('terms.' . $package . '.exigency'))->get();
            $classify = self::select('name', 'id')->where('taxonomy', Config::get('terms.' . $package . '.classify'))->get();

            $data = [
                'exigency' => $exigency,
                'classify' => $classify,
                'package' => $package,
            ];
            return $data;
        } else {
            $data = ['error' => true];
            return $data;
        }
    }

    public function showProvince($province)
    {
        if ($province) {
            $district = self::select('name', 'id')->where([
                'taxonomy' => 'nip_district',
                'parent' => $province,
            ])->get();
            $data = ['district' => $district];
            return $data;
        } else {
            $data = ['error' => true];
            return $data;
        }
    }

}
