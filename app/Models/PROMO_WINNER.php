<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PROMO_WINNER extends Model
{
    protected $table = 'PROMO_WINNER';
    public $timestamps = false;
}
