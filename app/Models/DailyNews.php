<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyNews extends Model
{

    public $timestamps = false;

    public $sequence = 'DAILYNEWS_SEQ';

    protected $table = 'dailynews';



}