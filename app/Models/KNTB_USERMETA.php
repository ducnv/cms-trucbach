<?php

namespace App\Models;

use App\Models\Packages;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Config;

class KNTB_USERMETA extends Model
{
    protected $table = 'KNTB_USERMETA';
    protected $primaryKey = 'ID';
//    public $sequence = 'KNTB_USERMETA_ID_SEQ';
    public $timestamps = false;

//    public $incrementing = false;

    public function packages()
    {
        return $this->hasOne(Packages::class, 'code', 'pkg_code');
    }

    public function insertUserMeta($string,$value,$sdt,$pkg_code = null,$id = 0)
    {
//        dd($string,$value,$sdt,$pkg_code ,$id );
//        dd((int)implode(explode('-', Carbon::now()->toDateString())));

        $userMeta = self::findOrNew($id);
        if ($id != 0) {
            $userMeta->ID = $id;
        } else {
            $userMeta->ID = self::max('id') + 1;
        }
//        $userMeta->MSISDN = Config::get('package.numberPhone');
        $userMeta->MSISDN = $sdt;
        $userMeta->META_KEY = $string;
        $userMeta->META_VALUE = $value;
        $userMeta->PKG_CODE = $pkg_code;
        $userMeta->CREATED_TIME = Carbon::now();
        $userMeta->CREATED_DATE = (int)implode(explode('-', Carbon::now()->toDateString()));
        $userMeta->save();
    }
}
