<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlackList extends Model
{

    public $timestamps = false;

    //public $sequence = 'BLACKLIST_SEQ';

    protected $table = 'blacklist';

    protected $primaryKey = 'msisdn';

    public $incrementing = false;

}