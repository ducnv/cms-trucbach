<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KNTB_TERM_RELATIONSHIPS extends Model
{
    protected $table = 'KNTB_TERM_RELATIONSHIPS';
    protected $primaryKey = ['POST_ID', 'TERM_ID'];
    public $timestamps = false;
    public $incrementing = false;

    public function terms()
    {
        return $this->hasOne(KNTB_TERMS::class, 'id', 'term_id');
    }

    public function insertTermRelationships($postId, $request)
    {
        $term = new KNTB_TERM_RELATIONSHIPS();
        $term->POST_ID = $postId;
        $term->TERM_ID = (int)$request;
        $term->save();
    }


}
