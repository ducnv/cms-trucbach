<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KNTB_POSTMETA extends Model
{
    protected $table = 'KNTB_POSTMETA';
    protected $primaryKey = 'ID';
//  public $sequence = 'KNTB_TERMS_ID_SEQ';
    public $timestamps = false;


    public function insertPostMeta($string, $value, $postId, $id = 0)
    {
        $postMeta = self::findOrNew($id);
        if ($id != 0) {
            $postMeta->ID = $id;
        } else {
            $postMeta->ID = self::max('id') + 1;
        }
        $postMeta->POST_ID = $postId;
        $postMeta->META_KEY = $string;
        $postMeta->META_VALUE = $value;
        $postMeta->save();
    }

    public function idPostMeta($postId, $metaKey)
    {
        $idMeta = self::where([
            'post_id' => $postId,
            'meta_key' => $metaKey,
        ])->value('id');

        return $idMeta;
    }
}
