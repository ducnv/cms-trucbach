<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{

    protected $table = 'subscriber';

    public function packages()
    {
        return $this->hasOne(Packages::class, 'code', 'pkg_code');
    }

    public function userMeta()
    {
        return $this->hasMany(KNTB_USERMETA::class, 'msisdn', 'msisdn');
    }

}