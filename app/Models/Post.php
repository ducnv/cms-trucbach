<?php

namespace App\Models;


use Carbon\Carbon;
use Config;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'KNTB_POST';
    protected $primaryKey = 'ID';
    public $timestamps = false;
    public function subscriber()
    {
        return $this->hasOne(Subscriber::class, 'pkg_code', 'pkg_code');
    }

    //web KNTB

    public function postMeta()
    {
        return $this->hasMany(KNTB_POSTMETA::class, 'post_id', 'id');
    }


    public function termRelationships()
    {
        return $this->hasMany(KNTB_TERM_RELATIONSHIPS::class, 'post_id', 'id');
    }



    public function insertPost($data,$sdt, $id = 0)
    {
        $post = self::findOrNew($id);
        if ($id != 0) {
            $post->ID = $id;
            $post->MODIFIED_TIME = Carbon::now();
        } else {
            $digits = 5;
            $randomNumber = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
            $packageType = Config::get('package.attributesPackage.' . $data['benefitPackage']);
            if( $packageType == 'nip_job'){
                $postCode = '1'.$randomNumber;
            }elseif ($packageType == 'nip_bds'){
                $postCode = '5'.$randomNumber;
            }elseif ($packageType == 'nip_transport'){
                $postCode = '3'.$randomNumber;
            }

            $post->ID = self::max('id') + 1;
            $post->POST_CODE = $postCode;
            $post->CREATED_TIME = Carbon::now();
            $post->CREATE_DATE = (int)implode(explode('-', Carbon::now()->toDateString()));
        }
        $post->MSISDN = $sdt;
//        $post->MSISDN = Config::get('package.numberPhone');

        $post->PKG_CODE = $data['benefitPackage'];
        $post->SUB_TYPE = 1;
        $post->POST_TYPE = Config::get('package.attributesPackage.' . $data['benefitPackage']);
        $post->POST_STATUS = 'draft';
        $post->POST_FLAG = 1;
        $post->EXPIRED_TIME = $data['timeOut'];
        $post->POST_NAME = $data['title'];
        $post->POST_TITLE = $data['title'];
        $post->POST_SLUG = str_slug($data['title']);
        $post->POST_CONTENT = !empty($data['content'])?$data['content']:' ';
        $post->save();

        return $post->ID;
    }

    public function insertPostInProfile($data,$sdt, $id = 0)
    {
        $post = self::findOrNew($id);
        if ($id != 0) {
            $post->ID = $id;
            $post->MODIFIED_TIME = Carbon::now();
        } else {
            $digits = 5;
            $randomNumber = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
            $packageType = Config::get('package.attributesPackage.' . $data['benefitPackage']);
            if( $packageType == 'nip_job'){
                $postCode = '0'.$randomNumber;
            }elseif ($packageType == 'nip_bds'){
                $postCode = '4'.$randomNumber;
            }elseif ($packageType == 'nip_transport'){
                $postCode = '2'.$randomNumber;
            }

            $post->ID = self::max('id') + 1;
            $post->POST_CODE = $postCode;
            $post->CREATED_TIME = Carbon::now();
            $post->CREATE_DATE = (int)implode(explode('-', Carbon::now()->toDateString()));
        }
//        $post->MSISDN = Config::get('package.numberPhone');
        $post->MSISDN = $sdt;
        $post->PKG_CODE = $data['benefitPackage'];
        $post->SUB_TYPE = 2;
        $post->POST_TYPE = Config::get('package.attributesPackage.' . $data['benefitPackage']);
        $post->POST_STATUS = 'draft';
        $post->POST_FLAG = 1;
        $post->EXPIRED_TIME = '';
        $post->POST_NAME = $data['nip_name'];
        $post->POST_TITLE = $data['nip_name'];
        $post->POST_SLUG = str_slug($data['nip_name']);
        $post->POST_CONTENT = ' ';
        $post->save();

        return $post->ID;
    }


}
