<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
    ];

    protected $casts = [

    ];

    public function article()
    {
        return $this->hasMany(Articles::class, 'category_id');
    }

}
