<?php

namespace App\Http\Controllers;

use App\Models\Packages;
use App\Models\Post;
use App\Models\KNTB_POSTMETA;
use App\Models\KNTB_TERM_RELATIONSHIPS;
use App\Models\KNTB_TERMS;
use App\Models\KNTB_USERMETA;
//use App\Models\PACKAGES;
use App\Models\Subscriber;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use DB;
use Illuminate\Support\Facades\Schema;
//use Config;
use Illuminate\Support\Facades\Session;
use Config;

class ProfileController extends Controller
{
    protected $TERMS = '';
    protected $USERMETA = '';
    protected $PACKAGES = '';
    protected $SUBSCRIBER = '';
    protected $KNTB_TERM_RELATIONSHIPS = '';
    protected $KNTB_POST = '';
    protected $KNTB_POSTMETA = '';

    public function __construct(KNTB_TERMS $TERMS, KNTB_USERMETA $USERMETA, Packages $PACKAGES, Subscriber $SUBSCRIBER, KNTB_POSTMETA $KNTB_POSTMETA, Post $KNTB_POST, KNTB_TERM_RELATIONSHIPS $KNTB_TERM_RELATIONSHIPS)
    {
        $this->TERMS = $TERMS;
        $this->USERMETA = $USERMETA;
        $this->PACKAGES = $PACKAGES;
        $this->SUBSCRIBER = $SUBSCRIBER;
        $this->KNTB_TERM_RELATIONSHIPS = $KNTB_TERM_RELATIONSHIPS;
        $this->KNTB_POST = $KNTB_POST;
        $this->KNTB_POSTMETA = $KNTB_POSTMETA;
    }

    public function jobProfile()
      {
//          dd(1);
          $subscriber = $this->SUBSCRIBER->with('packages')->where([ 'pkg_code' => Config::get('package.namePackage.job_deman')])->first();
//          if (!empty($subscriber)) {
//              $check = $this->USERMETA->where(['msisdn' => Config::get('package.numberPhone'), 'pkg_code' => Config::get('package.namePackage.job_deman')])->first();
//              if (!empty($check)) {
//                  return redirect()->route('profile.jobProfileEdit');
//              }
              $province = $this->TERMS->select('name', 'id')->where('taxonomy', 'nip_province')->get()->toArray();
              $wage = $this->TERMS->where('TAXONOMY', 'nip_wage')->get();
              $package = $subscriber['packages'];

              return view('profileDemandSources.job.jobProfile', compact('province', 'package', 'wage'));
//          } else {
//              return redirect()->route('home.index')->with(['alert' => 'error', 'message' => 'Bạn không có quyền truy cập']);
//          }
      }

      public function jobProfileEdit($sdt)
      {
          $subscriber = $this->SUBSCRIBER->with('packages')->where(['msisdn' => $sdt, 'pkg_code' => Config::get('package.namePackage.job_deman')])->first();
          $package = $subscriber['packages'];
          $data = $this->getEditProfile($sdt, Config::get('package.namePackage.job_deman'));
          $data['package'] = $package;
          $data['sdt'] = $sdt;
//          dd($data);
          return view('profileDemandSources.job.jobProfileEdit', $data);

      }

      public function bdsProfile()
      {
          $subscriber = $this->SUBSCRIBER->with('packages')->where([ 'pkg_code' => Config::get('package.namePackage.bds_deman')])->first();
//          if (!empty($subscriber)) {

//              $check = $this->USERMETA->where(['msisdn' => Config::get('package.numberPhone'), 'pkg_code' => Config::get('package.namePackage.bds_deman')])->first();
//              if (!empty($check)) {
//                  return redirect()->route('profile.bdsProfileEdit');
//              }

              $province = $this->TERMS->select('name', 'id')->where('taxonomy', 'nip_province')->get()->toArray();
              $wage = $this->TERMS->where('TAXONOMY', 'nip_wage')->get();
              $package = $subscriber['packages'];

              return view('profileDemandSources.bds.bdsProfile', compact('province', 'package', 'wage'));
//          } else {
//              return redirect()->route('home.index')->with(['alert' => 'error', 'message' => 'Bạn không có quyền truy cập']);
//          }


      }

      public function bdsProfileEdit($sdt)
      {
          $subscriber = $this->SUBSCRIBER->with('packages')->where(['msisdn' => $sdt, 'pkg_code' => Config::get('package.namePackage.bds_deman')])->first();
//          if (!empty($subscriber)) {

              $package = $subscriber['packages'];
              $data = $this->getEditProfile($sdt, Config::get('package.namePackage.bds_deman'));
              $data['package'] = $package;
              $data['sdt'] = $sdt;
              return view('profileDemandSources.bds.bdsProfileEdit', $data);
//          } else {
//              return redirect()->route('home.index')->with(['alert' => 'error', 'message' => 'Bạn không có quyền truy cập']);
//          }
      }

      public function transportProfile()
      {
          $subscriber = $this->SUBSCRIBER->with('packages')->where([ 'pkg_code' => Config::get('package.namePackage.transport_deman')])->first();
//          if (!empty($subscriber)) {

//              $check = $this->USERMETA->where(['msisdn' => Config::get('package.numberPhone'), 'pkg_code' => Config::get('package.namePackage.transport_deman')])->first();
//              if (!empty($check)) {
//                  return redirect()->route('profile.transportProfileEdit');
//              }

              $province = $this->TERMS->select('name', 'id')->where('taxonomy', 'nip_province')->get()->toArray();
              $wage = $this->TERMS->where('TAXONOMY', 'nip_wage')->get();
              $package = $subscriber['packages'];

              return view('profileDemandSources.transport.transportProfile', compact('province', 'package', 'wage'));
//          } else {
//              return redirect()->route('home.index')->with(['alert' => 'error', 'message' => 'Bạn không có quyền truy cập']);
//          }


      }

      public function transportProfileEdit($sdt)
      {
          $subscriber = $this->SUBSCRIBER->with('packages')->where(['msisdn' => $sdt, 'pkg_code' => Config::get('package.namePackage.transport_deman')])->first();
//          if (!empty($subscriber)) {

              $package = $subscriber['packages'];
              $data = $this->getEditProfile($sdt, Config::get('package.namePackage.transport_deman'));
              $data['package'] = $package;
              $data['sdt'] = $sdt;
              return view('profileDemandSources.transport.transportProfileEdit', $data);
//          } else {
//              return redirect()->route('home.index')->with(['alert' => 'error', 'message' => 'Bạn không có quyền truy cập']);
//          }
      }

      public function profileDemandSources(Request $request)
      {
          //save profile
          $sdt = $request->sdt;
          $data = $request->all();
          if ($request->benefitPackage == Config::get('package.namePackage.job_deman') && !empty($request->wage)) {
              $post = $this->KNTB_POST->insertPostInProfile($data,$sdt);
              $postId = $post;
              $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->wage);
          } elseif ($request->benefitPackage != Config::get('package.namePackage.job_deman') && !empty($request->price)) {

              $post = $this->KNTB_POST->insertPostInProfile($data,$sdt);
              $postId = $post;
              $price = $this->denominations($request->denominations,$request->price);
              $this->KNTB_POSTMETA->insertPostMeta('nip_price', $price, $postId);

          } else {
              return redirect()->back()->with(['alert' => 'success', 'message' => 'ERROR']);
          }

          $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->exigency);

          $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->classify);

          if ($request->nip_province) {
              $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->nip_province);
          }
          $this->USERMETA->insertUserMeta('_nip_name', $request->nip_name,$sdt, $request->benefitPackage);
          $this->USERMETA->insertUserMeta('_nip_address', $request->nip_address,$sdt, $request->benefitPackage);

  //        $postCode = $this->KNTB_POST->where('id',$postId)->value('post_code');
  //        $this->KNTB_POST->sendApiPostAdd($postCode);
//              $subname = $this->SUBSCRIBER->checkBack();
//              if ($subname == 'error'){
//                  return redirect()->back()->with(['alert' => 'error', 'message' => 'Error']);
//              }
              return redirect()->back()->with(['alert' => 'success', 'message' => 'Cập nhật hồ sơ thành công. Mời quý khách tiếp tục sử dụng dịch vụ.']);

      }

      public function editDemandSources(Request $request)
      {
          //edit profile
          $data = $request->all();
          $id = $request->postId;
          $sdt = $request->sdt;
          if ($request->benefitPackage == Config::get('package.namePackage.job_deman') && !empty($request->wage)) {

              $post = $this->KNTB_POST->insertPostInProfile($data,$sdt, $id);
              $postId = $post;
              $checkPrice = $this->KNTB_POSTMETA->where(['POST_ID' => $postId, 'META_KEY' => 'nip_price'])->first();
              if (!empty($checkPrice)) {
                  $this->KNTB_POSTMETA->where(['POST_ID' => $postId, 'META_KEY' => 'nip_price'])->delete();
              }

              $this->KNTB_TERM_RELATIONSHIPS->where('post_id', $postId)->delete();
              $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->wage);
          } elseif ($request->benefitPackage != Config::get('package.namePackage.job_deman') && !empty($request->price)) {
              $post = $this->KNTB_POST->insertPostInProfile($data,$sdt, $id);
              $postId = $post;

              $this->KNTB_TERM_RELATIONSHIPS->where('post_id', $postId)->delete();
              $postMetaId = $this->KNTB_POSTMETA->where(['POST_ID' => $postId, 'META_KEY' => 'nip_price'])->value('id');
              $price = $this->denominations($request->denominations,$request->price);
              $this->KNTB_POSTMETA->insertPostMeta('nip_price', $price, $postId, $postMetaId);

          } else {
              return redirect()->back()->with(['alert' => 'success', 'message' => 'ERROR']);
          }

          $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->exigency);
          $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->classify);
          $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->nip_province);

          $nipNameId = $this->USERMETA->where(['msisdn' => $sdt, 'pkg_code' => $request->benefitPackage, 'meta_key' => '_nip_name'])->value('id');
          $nipAddressId = $this->USERMETA->where(['msisdn' => $sdt, 'pkg_code' => $request->benefitPackage, 'meta_key' => '_nip_address'])->value('id');
//          dd($nipNameId);

          $this->USERMETA->insertUserMeta('_nip_name', $request->nip_name,$sdt, $request->benefitPackage, $nipNameId);
          $this->USERMETA->insertUserMeta('_nip_address', $request->nip_address,$sdt, $request->benefitPackage, $nipAddressId);

  //        $postCode = $this->KNTB_POST->where('id',$postId)->value('post_code');
  //        $this->KNTB_POST->sendApiPostEdit($postCode);
//              $subname = $this->SUBSCRIBER->checkBack();
//              if ($subname == 'error'){
//                  return redirect()->back()->with(['alert' => 'error', 'message' => 'Error']);
//              }

              return redirect()->route('hoSoDemandsources')->with(['alert' => 'success', 'message' => 'Cập nhật hồ sơ thành công. Hồ sơ ở trạng thái chờ duyệt.']);

      }

      public function getEditProfile($sdt, $packageCode)
      {
          $userMeta = $this->USERMETA->where(['msisdn' => $sdt, 'pkg_code' => $packageCode])->get()->pluck('meta_value', 'meta_key');
          $post = $this->KNTB_POST->where(['msisdn' => $sdt, 'pkg_code' => $packageCode])->with(['termRelationships' => function ($query) {
              $query->with('terms');
          }])->first();
          $valueRelationships = $post['termRelationships']->pluck('terms')->pluck('id', 'taxonomy');
          $showMeta = $this->TERMS->showPackage($post->pkg_code);

          $province = $this->TERMS->select('name', 'id')->where('taxonomy', 'nip_province')->get()->toArray();

          if ($post->pkg_code == Config::get('package.namePackage.job_deman')) {
              $price = '';
              $wage = $this->TERMS->where('TAXONOMY', 'nip_wage')->get();
          } else {
              $wage = '';
              $price = $this->KNTB_POSTMETA->where(['post_id' => $post->id, 'meta_key' => 'nip_price'])->value('meta_value');
          }

          $data = [
              'userMeta' => $userMeta,
              'post' => $post,
              'showMeta' => $showMeta,
              'province' => $province,
              'wage' => $wage,
              'price' => $price,
              'postId' => $post->id,
              'valueRelationships' => $valueRelationships,
          ];
          return $data;
      }

      public function checkPackage(Request $request)
      {
//          return response($request->all());
          $package = $request->package;
          return response($this->TERMS->showPackage($package));
      }

      // supply
      public function getProfileSupply()
      {

//          $check = $this->USERMETA->where(['msisdn' => Config::get('package.numberPhone'), 'PKG_CODE' => null])->get();
//          if (count($check)>0) {
//              return redirect()->route('profile.getProfileSupplyEdit');
//          }
          return view('profileSupply.profileSupply');
      }

      public function profileSupply(Request $request)
      {
//dd($request->all());
          $sdt = $request->sdt;
          $this->USERMETA->insertUserMeta('_nip_name', $request->nip_name,$sdt);
          $this->USERMETA->insertUserMeta('_nip_address', $request->nip_address,$sdt);
          if ($request->nip_email) {
              $this->USERMETA->insertUserMeta('_nip_email', $request->nip_email,$sdt);
          }
  //        Session::flash('status', 'success');


              return redirect()->back()->with(['alert' => 'success', 'message' => 'Cập nhật hồ sơ thành công. Mời QK đăng tin để nhận hồ sơ thích hợp']);

      }

      public function getProfileSupplyEdit($sdt)
      {

          $check = $this->USERMETA->where(['msisdn' =>$sdt, 'PKG_CODE' => null])->get();
          $profile = $check->pluck('meta_value', 'meta_key');
          return view('profileSupply.profileSupplyEdit', compact('profile','sdt'));
      }

      public function profileSupplyEdit(Request $request)
      {
          $sdt = $request->sdt;
          $this->USERMETA->where(['msisdn' => $sdt, 'PKG_CODE' => null])->delete();
          $this->USERMETA->insertUserMeta('_nip_name', $request->nip_name,$sdt);
          $this->USERMETA->insertUserMeta('_nip_address', $request->nip_address,$sdt);
          if ($request->nip_email) {
              $this->USERMETA->insertUserMeta('_nip_email', $request->nip_email,$sdt);
          }


              return redirect()->route('hoSoSupply');
      }

      public function denominations($denominations,$requestPrice){
          if ($denominations == 'Đồng'){
              $price = $requestPrice;
          }elseif($denominations == 'Triệu'){
              $price = $requestPrice.'000000';
          }elseif($denominations == 'Tỷ'){
              $price = $requestPrice.'000000000';
          }
          return $price;
      }
}
