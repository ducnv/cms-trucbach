<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\Category;
use App\Models\News;
use App\Models\Tags;
use App\Models\User;
use App\Models\Province;
use App\Models\Roles;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Carbon\Carbon;


class NewsController extends Controller
{
    use DispatchesJobs;

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'Tin tức',
            ],
        ];

        $this->views['breadcumbMain'] = "Tin tức";
    }

    public function manage()
    {

        $this->views['title'] = 'Danh sách Tin tức';

        return view('news.index', $this->views);
    }

    public function getCreate()
    {
        $this->views['title'] = 'Tạo Tin tức';

        return view('news.create', $this->views);
    }

    public function getEdit($id)
    {
        $article = Articles::find($id);

        if (!$article) {
            return redirect('/news');
        }
        $this->views['breadcumbMain'] = "Chỉnh sửa";

        $this->views['article'] = $article;

        $this->views['title'] = $article->title;

        return view('news.edit', $this->views);

    }

    public function apiGetNews(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $news = Articles::select("*");

        if ($request->has('name') && $request->get('name') !== '' && $request->get('name') !== null) {
            $news->where('name', 'LIKE', "%{$request->get('name')}%");
        }

        if ($request->has('category_id') && $request->get('category_id') !== '' && $request->get('category_id') !== null) {
            $news->where('category_id', "{$request->get('category_id')}");
        }

        $news = $news->orderBy('created_at', 'DESC');

        return [
            'data' => $news->paginate($per_page),
        ];
    }

    public function postNews(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'title.required' => 'Bạn chưa nhập tiêu đề!',
            'description.required' => 'Bạn chưa nhập mô tả!',
            'category_id.required' => 'Bạn chưa chọn chuyên mục!',
        ];

        $okay = Validator::make($data, [
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        $news = new Articles();

        $news->user_id = auth()->user()->id;

        $news->title = $data['title'];

        $news->slug = str_slug($data['title']);

        $news->description = $data['description'];

        $news->thumb = $data['thumb'];

        $news->body = $data['body'];

        $news->category_id = $data['category_id'];

        $news->status = 'draft';

        $news->tags = $data['tags'];

        $news->save();

        try {


            return [
                'status' => 'success',
                'message' => 'Tạo Tin tức thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'error',
                'message' => 'Tạo Tin tức không thành công'
            ];
        }
    }

    public function putNews(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'title.required' => 'Bạn chưa nhập tiêu đề!',
            'description.required' => 'Bạn chưa nhập mô tả!',
            'category_id.required' => 'Bạn chưa chọn chuyên mục!',
        ];

        $okay = Validator::make($data, [
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        $news = Articles::find($data['id']);

        $news->user_id = auth()->user()->id;

        $news->title = $data['title'];

        $news->slug = str_slug($data['title']);

        $news->description = $data['description'];

        $news->thumb = $data['thumb'];

        $news->body = $data['body'];

        $news->category_id = $data['category_id'];

        $news->status = $data['status'];

        $news->tags = $data['tags'];

        try {
            $news->save();

            //tu khoa
            if (count($data['tags']) > 0) {
                $listId = [];
                foreach ($data['tags'] as $item) {
                    $tags = Tags::find($item['value']);
                    $tags->weight = $tags->weight + 1;
                    $tags->save();
                    array_push($listId, $item['value']);
                }
                //sync
                $news->listTags()->sync($listId);
            }
            return [
                'status' => 'success',
                'message' => 'Sửa Tin tức thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'error',
                'message' => 'Sửa Tin tức không thành công'
            ];
        }
    }

    public function apiGetCategory()
    {
        return [
            'data' => Category::select('id as value', 'name as text')->where('status', 'yes')->get(),
        ];
    }

    public function apiGetKeywords()
    {
        return [
            'data' => Tags::select('id as value', 'name as text')->get(),
        ];
    }

}
