<?php

namespace App\Http\Controllers;

use App\Models\KNTB_GT;
use App\Models\KNTB_USERMETA;
use App\Models\PROMO_WINNER;
use App\Models\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CtkmController extends Controller
{

    public function showCtkm()
    {
        $this->views['title'] = 'Chương trình khuyến mãi';
        return view('ctkm.ctkm', $this->views);
    }

    public function apiGetCtkm(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $contents = Promotion::orderBy('id','asc')->paginate($per_page);
        return [
            'data' => $contents,
        ];
    }
    public function allCtkm(Request $request)
    {
        $contents = Promotion::select('code as value', 'name as text')->orderBy('id','asc')->get();
        return [
            'data' => $contents,
        ];
    }
    public function apiAddCtkm(Request $request)
    {
        $code = Promotion::max('id') + 1;
        $end_date = implode(explode('-',Carbon::parse($request->end_date)->format('Y-m-d')));
        $start_date = implode(explode('-',Carbon::parse($request->end_date)->format('Y-m-d')));


        $promotion = new Promotion();
        $promotion->NAME = $request->detail['name'];
        $promotion->ID = $code;
        $promotion->CODE = $code < 10?'PROMO_0'.$code:'PROMO_'.$code;
        $promotion->DESCRIPTION = $request->detail['description'];
        $promotion->MT = $request->detail['mt'];
        $promotion->START_DATE = $start_date;
        $promotion->END_DATE =  $end_date;
        $promotion->CREATED_DATE =  Carbon::now();
        $promotion->save();

        $data = [
            'status' => 'success',
            'message' => 'Thêm mới thành công'
        ];
        return $data;
    }

    public function apiEditCtkm(Request $request)
    {
//        $code = Promotion::max('id') + 1;
        $end_date = implode(explode('-',Carbon::parse($request->end_date)->format('Y-m-d')));
        $start_date = implode(explode('-',Carbon::parse($request->end_date)->format('Y-m-d')));


        $promotion = Promotion::find($request->detail['id']);
        $promotion->NAME = $request->detail['name'];
//        $promotion->ID = $code;
//        $promotion->CODE = $code < 10?'PROMO_0'.$code:'PROMO_'.$code;
        $promotion->DESCRIPTION = $request->detail['description'];
        $promotion->MT = $request->detail['mt'];
        $promotion->START_DATE = $start_date;
        $promotion->END_DATE =  $end_date;
        $promotion->CREATED_DATE =  Carbon::now();
        $promotion->save();

        $data = [
            'status' => 'success',
            'message' => 'Sửa thành công'
        ];
        return $data;
    }

    public function apiDeleteCtkm(Request $request)
    {

        Promotion::where('id',$request->id)->delete();
        $data = [
            'status' => 'success',
            'message' => 'Xóa thành công'
        ];
        return $data;
    }

    public function TBDKCtkm()
    {
        $this->views['title'] = 'Thuê bao đăng ký';
        return view('ctkm.TBDKCtkm', $this->views);
    }

    public function TBTTCtkm()
    {
        $this->views['title'] = 'Thuê bao trúng thưởng';
        return view('ctkm.TBTTCtkm', $this->views);
    }

    public function TTTTCtkm()
    {
        $this->views['title'] = 'Tình trạng trao thưởng';
        return view('ctkm.TTTTCtkm', $this->views);
    }

    public function activeProfile(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 10;
        if ($request->has('pkg_code') && $request->pkg_code == 'PROMO_01'){
            $contents = KNTB_USERMETA::where('META_KEY','_nip_name')
                ->whereDate('CREATED_TIME', '>=', Carbon::parse($request->start_date)->format('Y-m-d'))
                ->whereDate('CREATED_TIME', '<=', Carbon::parse($request->end_date)->format('Y-m-d'));

            $isdn = $request->get('isdn');
            if ($request->has('isdn') && $isdn !== '' && $isdn !== null) {
                $contents->where('MSISDN', 'LIKE', "%{$isdn}%");
            }

            $contents = $contents->paginate($per_page);
            if ($contents->total() == 0){
                $data = [
                    'status' => 'error',
                    'message' => 'Không có dữ liệu phù hợp'
                ];
            }else{
                $data = [];
            }

            return [
                'data' => $contents,
                'status' => $data
            ];
        }elseif ($request->has('pkg_code') && $request->pkg_code == 'PROMO_02'){
//            $contents = KNTB_GT::whereDate('CREATED_DATE', '>=', Carbon::parse($request->start_date)->format('Y-m-d'))
//                ->whereDate('CREATED_DATE', '<=', Carbon::parse($request->end_date)->format('Y-m-d'))->get()->groupBy(function($d) {
//                return $d->msisdn_gt;
//            });
            $isdn = $request->get('isdn');
            if ($request->has('isdn') && $isdn !== '' && $isdn !== null) {
                $contents = KNTB_GT::where('MSISDN_GT', 'LIKE', "%{$isdn}%")->get()->groupBy(function($d) {
                    return $d->msisdn_gt;
                });
            }else{
                $contents = KNTB_GT::get()->groupBy(function($d) {
                    return $d->msisdn_gt;
                });
            }

            $a = [];
            foreach ($contents as $key => $value){
                if (count($value) >= 2){
                    $a[] = $value;

                }
            }

            if (!empty($a)){
                $data = [];

            }else{
                $data = [
                    'status' => 'error',
                    'message' => 'Không có dữ liệu phù hợp'
                ];
            }

            return [
                'data' => $a,
                'status' => $data
            ];
        }



    }

    public function winnerCTKM(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 10;
        $contents = PROMO_WINNER::whereDate('CREATED_TIME', '>=', Carbon::parse($request->start_date)->format('Y-m-d'))
            ->whereDate('CREATED_TIME', '<=', Carbon::parse($request->end_date)->format('Y-m-d'));

        $isdn = $request->get('isdn');
        if ($request->has('isdn') && $isdn !== '' && $isdn !== null) {
            $contents->where('MSISDN', 'LIKE', "%{$isdn}%");
        }

        $pkg_code = $request->get('pkg_code');
        if ($request->has('pkg_code') && $pkg_code !== '' && $pkg_code !== null) {
            $contents->where('promo_code', 'LIKE', "%{$pkg_code}%");
        }
        $contents = $contents->paginate($per_page);
        if ($contents->total() == 0){
            $data = [
                'status' => 'error',
                'message' => 'Không có dữ liệu phù hợp'
            ];
        }else{
            $data = [];
        }

        return [
            'data' => $contents,
            'status' => $data
        ];


    }

    public function apiReward(Request $request)
    {
        PROMO_WINNER::where('id',$request->id)->update(['status' => 1]);
        $data = [
            'status' => 'success',
            'message' => 'Trao thưởng thành công'
        ];
        return [
            'status' => $data
        ];
    }
}
