<?php

namespace App\Http\Controllers;

use App\Models\BlackList;
use App\Models\His;
use App\Models\MO;
use App\Models\MT;
use App\Models\Packages;
use App\Models\Post;
use App\Models\SanLoc;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use DB;
use Validator;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Fill;
use App\Jobs\LogSystemEvent;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Parser;
use Session;
use Sunra\PhpSimple\HtmlDomParser;
use Carbon\Carbon;

class SystemController extends Controller
{
    use DispatchesJobs;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'Tra cứu sử dụng dịch vụ'
            ],
        ];

        $this->views['breadcumbMain'] = "Tra cứu sử dụng dịch vụ";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @Access (permission = "log_sub: access")
     */
    public function searchSub(Request $request)
    {
        $this->views['title'] = 'Tra cứu thuê bao';


        $this->views['appends'] = $request->all();

        if ($request->has('msisdn') && $request->get('msisdn') !== '' && $request->get('msisdn') !== null) {
            Session::put('sdt', $request->get('msisdn'));
            $convertSdt = $request->get('msisdn');
            if (substr($convertSdt, 0, 1) == 0) {
                $convertSdt = '84' . substr($convertSdt, 1, strlen($convertSdt) - 1);
            }

            $this->views['sub'] = Subscriber::select("*")->where('msisdn', $convertSdt)->orderBy('create_time', 'DESC')->paginate(20);
        }

        return view('cmsNew.TraCuuThueBao', $this->views);
    }

    public function thoiGianGiuLoc(Request $request)
    {
        $this->views['title'] = 'Thời gian giữ lộc';

        $this->views['appends'] = $request->all();


        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {

            Session::put('sdt', $request->get('isdn'));

            $convertSdt = $request->get('isdn');

            if (substr($convertSdt, 0, 1) == 0) {
                $convertSdt = '84' . substr($convertSdt, 1, strlen($convertSdt) - 1);
            }

            $sub = Subscriber::where('msisdn', $convertSdt)->first();

            $reg_time = $sub->reg_time;

            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');

            $sanLoc = SanLoc::where('msisdn', $convertSdt)->where('created_at', '>', Carbon::parse($reg_time)->format('Y-m-d H:i:s'))
                ->where('created_at', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
                ->where('created_at', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'))
                ->orderBy('created_at', 'ASC')
                ->paginate(20);

            $dayCurrent = intval(date('d', time()));

            foreach ($sanLoc as $item) {
                if (intval($item->status) === 0) {
                    $dayItem = intval(Carbon::parse($item->created_at)->format('d'));
                    $datetime1 = strtotime(Carbon::parse($item->created_at)->format('Y-m-d H:i:s'));
                    if ($dayCurrent === $dayItem) {
                        $datetime2 = strtotime(Carbon::now());
                        $item->end_at = Carbon::now()->format('Y-m-d H:i:s');
                    } else {
                        $dayRenew = Carbon::parse($item->created_at)->format('d');
                        $dayRenew = intval($dayRenew) + 1;
//                        //kiem tra xem 10h hom sau co gia han thanh cong ko
//                        $check = His::where('msisdn', $convertSdt)->where('result', 0)->where('action', 'RENEW')
//                            ->where('created_at', '>=', Carbon::parse($start_date)->format('Y-m-' . $dayRenew . ' 00:00:00'))
//                            ->where('created_at', '<=', Carbon::parse($end_date)->format('Y-m-' . $dayRenew . ' 10:00:00'))->first();
//
                        $datetime2 = strtotime(Carbon::parse($item->created_at)->format('Y-m-' . $dayRenew . ' 10:00:00'));
                        $item->end_at = Carbon::parse($item->created_at)->format('Y-m-' . $dayRenew . ' 10:00:00');
                    }

                    $item->time_hold = $datetime2 - $datetime1;

                }
            }

            $this->views['data'] = $sanLoc;

        }


        return view('cmsNew.thoigiangiuloc', $this->views);
    }

    public function thueBaoGiuLoc(Request $request)
    {
        $this->views['title'] = 'Thuê bao giữ lộc';

        $this->views['appends'] = $request->all();

        $sanLoc = SanLoc::where('status', 0)->where('created_at', '>=', Carbon::now()->format('Y-m-d 00:00:00'))
            ->where('created_at', '<=', Carbon::now()->format('Y-m-d 23:59:59'))
            ->orderBy('created_at', 'ASC')
            ->paginate(20);

        $dayCurrent = intval(date('d', time()));

        foreach ($sanLoc as $item) {
            if (intval($item->status) === 0) {
                $dayItem = intval(Carbon::parse($item->created_at)->format('d'));
                $datetime1 = strtotime(Carbon::parse($item->created_at)->format('Y-m-d H:i:s'));
                if ($dayCurrent === $dayItem) {
                    $datetime2 = strtotime(Carbon::now());
                    $item->end_at = Carbon::now()->format('Y-m-d H:i:s');
                } else {
                    $dayRenew = Carbon::parse($item->created_at)->format('d');
                    $dayRenew = intval($dayRenew) + 1;
//                        //kiem tra xem 10h hom sau co gia han thanh cong ko
//                        $check = His::where('msisdn', $convertSdt)->where('result', 0)->where('action', 'RENEW')
//                            ->where('created_at', '>=', Carbon::parse($start_date)->format('Y-m-' . $dayRenew . ' 00:00:00'))
//                            ->where('created_at', '<=', Carbon::parse($end_date)->format('Y-m-' . $dayRenew . ' 10:00:00'))->first();
//
                    $datetime2 = strtotime(Carbon::parse($item->created_at)->format('Y-m-' . $dayRenew . ' 10:00:00'));
                    $item->end_at = Carbon::parse($item->created_at)->format('Y-m-' . $dayRenew . ' 10:00:00');
                }

                $item->time_hold = $datetime2 - $datetime1;

            }
        }


        $this->views['data'] = $sanLoc;

        return view('cmsNew.thuebaogiuloc', $this->views);

    }

    /**
     * @Access (permission = "log_register_un: access")
     */
    public function logRegisterUn(Request $request)
    {
        $this->views['title'] = 'Lịch sử đăng ký/hủy';

        $this->views['appends'] = $request->all();

        $package = Packages::select('code as value', 'name as text', 'reg_fee as price')->get();

        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {
            Session::put('sdt', $request->get('isdn'));
            $isdn = $request->get('isdn');
            if (substr($isdn, 0, 1) == 0) {
                $isdn = '84' . substr($isdn, 1, strlen($isdn) - 1);
            }

            $log = His::where('msisdn', $isdn)->whereIn('action', ['FIRST_REG', 'RE_REG', 'UNREG']);
            if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
                $log->where('pkg_code', $request->get('pkg_code'));
            }
            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');
            $log->where('created_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
                ->where('created_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'));
            $data = $log->orderBy('created_time', 'DESC')->paginate(20);

            return view('cmsNew.LichSuDangKiHuy', $this->views, compact('data', 'package'));
        }

        return view('cmsNew.LichSuDangKiHuy', $this->views, compact('package'));
    }


    /**
     * @Access (permission = "log_charge: access")
     */
    public function logCharge(Request $request)
    {
        $this->views['title'] = 'Lịch sử trừ cước';
        $this->views['appends'] = $request->all();
        $package = Packages::select('code as value', 'name as text', 'reg_fee as price')->get();
        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {
            Session::put('sdt', $request->get('isdn'));

            $isdn = $request->get('isdn');
            if (substr($isdn, 0, 1) == 0) {
                $isdn = '84' . substr($isdn, 1, strlen($isdn) - 1);
            }

            $log = His::where('msisdn', $isdn);

            $start_date = $request->get('start_date');

            $end_date = $request->get('end_date');

            $log->where('created_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
                ->where('created_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'));


            if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
                $log->where('pkg_code', $request->get('pkg_code'));
            }

            $log = $log->orderBy('created_time', 'DESC')->paginate(20);
            $sum = $log->sum('c_fee');
            return view('cmsNew.LichSuTruCuoc', $this->views, compact('log', 'sum', 'package'));
        }
        return view('cmsNew.LichSuTruCuoc', $this->views, compact('package'));
    }

    /**
     * @Access (permission = "log_mo: access")
     */
    public function logMo()
    {
        $this->views['title'] = 'Lịch sử MO ';

        return view('system.logmo', $this->views);
    }

    /**
     * @Access (permission = "log_mt: access")
     */
    public function logMt()
    {
        $this->views['title'] = 'Lịch sử MT';

        return view('system.logmt', $this->views);
    }

    public function logMoMt(Request $request)
    {
        $this->views['title'] = 'Lịch sử MO MT';
        $this->views['appends'] = $request->all();
        $package = Packages::select('code as value', 'name as text', 'reg_fee as price')->get();
        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {

            Session::put('sdt', $request->get('isdn'));

            $isdn = $request->get('isdn');
            if (substr($isdn, 0, 1) == 0) {
                $isdn = '84' . substr($isdn, 1, strlen($isdn) - 1);
            }
            $start_date = $request->get('start_date');

            $end_date = $request->get('end_date');

//            //MO
//            $logMo = MO::where('mo.msisdn', $isdn);
//
//            $logMo->where('mo.created_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
//                ->where('mo.created_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'));
//
//            $logMo = $logMo->leftJoin('his', function ($leftJoin) {
//                $leftJoin->on('mo.trans_id', '=', 'his.trans_id');
//            })->select('mo.created_time', 'mo.mo', 'his.result', 'his.c_fee', 'mo.channel', 'his.pkg_code','mo.mo_id');
//
//            if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
//                $logMo->where('his.pkg_code', $request->get('pkg_code'));
//            }
//
//            $logMo = $logMo->orderBy('mo.created_time', 'DESC')->paginate(20);
//            dd($logMo);

            $logMt = MT::where('mt.msisdn', $isdn);

            $logMt->where('mt.created_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
                ->where('mt.created_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'));

            $logMt = $logMt->leftJoin('his', function ($leftJoin) {
                $leftJoin->on('mt.trans_id', '=', 'his.trans_id');
            })->select('mt.created_time', 'mt.mt', 'mt.pkg_code', 'his.result', 'his.c_fee', 'mt.channel', 'mt.mo_id');

            if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
                $logMt->where('mt.pkg_code', $request->get('pkg_code'));
            }

            $logMt = $logMt->orderBy('mt.created_time', 'DESC')->paginate(20);

            return view('cmsNew.LichSuMoMt', $this->views, compact('logMt', 'package'));
        }
        return view('cmsNew.LichSuMoMt', $this->views, compact('package'));
    }

    public function logUseDeman(Request $request)
    {
        $this->views['title'] = 'Lịch sử sử dụng nguồn cầu';
        $this->views['appends'] = $request->all();
        $package = Packages::select('code as value', 'name as text', 'reg_fee as price')->where('pkg_type', 2)->get();
        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {

            Session::put('sdt', $request->get('isdn'));

            $isdn = $request->get('isdn');
            if (substr($isdn, 0, 1) == 0) {
                $isdn = '84' . substr($isdn, 1, strlen($isdn) - 1);
            }

            $log = Post::where('msisdn', $isdn)->where('sub_type', 2)->with(['subscriber' => function ($query) use ($isdn) {
                $query->select('msisdn', 'pkg_code', 'expired_time', 'service_state', 'sub_type')->where('msisdn', $isdn);
            }]);

//            $start_date = $request->get('start_date');
//
//            $end_date = $request->get('end_date');

//            $log->where('created_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
//                ->where('created_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'));


            if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
                $log->where('pkg_code', $request->get('pkg_code'));
            }

            $log = $log->orderBy('created_time', 'DESC')->paginate(20);

            foreach ($log as $key => $value) {
                if ($value->subscriber['service_state'] != 1 || $value->subscriber['expired_time'] < Carbon::now()) {
                    $value->post_status = 'close';
                }
            }

            return view('cmsNew.LichSuSuDungNguonCau', $this->views, compact('log', 'package'));

        }

        return view('cmsNew.LichSuSuDungNguonCau', $this->views, compact('package'));
    }

    public function logUseSupply(Request $request)
    {
        $this->views['title'] = 'Lịch sử sử dụng nguồn cung';
        $this->views['appends'] = $request->all();
        $package = Packages::select('code as value', 'name as text', 'reg_fee as price')->where('pkg_type', 1)->get();
        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {

            Session::put('sdt', $request->get('isdn'));

            $isdn = $request->get('isdn');
            if (substr($isdn, 0, 1) == 0) {
                $isdn = '84' . substr($isdn, 1, strlen($isdn) - 1);
            }

            $log = Post::where('msisdn', $isdn)->where('sub_type', 1)->with(['subscriber' => function ($query) use ($isdn) {
                $query->select('msisdn', 'pkg_code', 'expired_time', 'service_state', 'sub_type')->where('msisdn', $isdn);
            }]);

            $start_date = $request->get('start_date');

            $end_date = $request->get('end_date');

            $log->where('created_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
                ->where('created_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'));


            if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
                $log->where('pkg_code', $request->get('pkg_code'));
            }

            $log = $log->orderBy('created_time', 'DESC')->paginate(20);

            foreach ($log as $key => $value) {
                if ($value->expired_time < Carbon::now() || $value->subscriber['service_state'] != 1 || $value->subscriber['expired_time'] < Carbon::now()) {
                    $value->post_status = 'close';
                }
            }

            return view('cmsNew.LichSuSuDungNguonCung', $this->views, compact('log', 'package'));

        }

        return view('cmsNew.LichSuSuDungNguonCung', $this->views, compact('package'));
    }

    /**
     * @Access (permission = "black_list: access")
     */
    public function blackList()
    {
        $this->views['title'] = 'Quản trị blacklist';

        return view('system.blacklist', $this->views);
    }

    public function apiGetInfoSub(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $sub = Subscriber::select("*");

        $isdn = $request->get('isdn');
        if (substr($isdn, 0, 1) == 0) {
            $isdn = '84' . substr($isdn, 1, strlen($isdn) - 1);
        }

        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {
            $sub->where('msisdn', $isdn);
        }

        return [
            'data' => $sub->orderBy('create_time', 'DESC')->paginate($per_page)
        ];
    }


    public function apiGetLogMo(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $isdn = $request->get('isdn');

        $log = MO::where('mo.msisdn', $isdn);

        $start_date = $request->get('start_date');

        $end_date = $request->get('end_date');

        $log->where('mo.created_time', '>=', Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d 00:00:00'))
            ->where('mo.created_time', '<=', Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d 23:59:59'));

        $log = $log->leftJoin('his', function ($leftJoin) {
            $leftJoin->on('mo.trans_id', '=', 'his.trans_id');
        })->select('mo.created_time', 'mo.mo', 'his.result', 'his.c_fee', 'mo.channel', 'his.pkg_code');

        if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
            $log->where('his.pkg_code', $request->get('pkg_code'));
        }

        $log = $log->orderBy('mo.created_time', 'DESC')->paginate($per_page);

        return [
            'data' => $log
        ];
    }

    public function apiGetLogMt(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $isdn = $request->get('isdn');

        $log = MT::where('mt.msisdn', $isdn)->where('type', 1);

        $start_date = $request->get('start_date');

        $end_date = $request->get('end_date');

        $log->where('mt.created_time', '>=', Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d 00:00:00'))
            ->where('mt.created_time', '<=', Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d 23:59:59'));

        $log = $log->leftJoin('his', function ($leftJoin) {
            $leftJoin->on('mt.trans_id', '=', 'his.trans_id');
        })->select('mt.created_time', 'mt.mt', 'mt.pkg_code', 'his.result', 'his.c_fee', 'mt.channel');

        if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
            $log->where('mt.pkg_code', $request->get('pkg_code'));
        }

        $log = $log->orderBy('mt.created_time', 'DESC')->paginate($per_page);

        return [
            'data' => $log
        ];
    }

    public function apiGetLogCharge(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $isdn = $request->get('isdn');

        $log = His::where('msisdn', $isdn);

        $start_date = $request->get('start_date');

        $end_date = $request->get('end_date');

        $log->where('created_time', '>=', Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d 00:00:00'))
            ->where('created_time', '<=', Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d 23:59:59'));


        if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
            $log->where('pkg_code', $request->get('pkg_code'));
        }

        $log = $log->orderBy('created_time', 'DESC')->paginate($per_page);

        return [
            'data' => $log,
            'sum' => $log->sum('c_fee')
        ];
    }

    public function apiGetLogRegisterUn(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $isdn = $request->get('isdn');

        $log = His::where('msisdn', $isdn)->whereIn('action', ['FIRST_REG', 'RE_REG', 'UNREG']);

        if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
            $log->where('pkg_code', $request->get('pkg_code'));
        }

        $start_date = $request->get('start_date');

        $end_date = $request->get('end_date');

        $log->where('created_time', '>=', Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d 00:00:00'))
            ->where('created_time', '<=', Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d 23:59:59'));

        return [
            'data' => $log->orderBy('created_time', 'DESC')->paginate($per_page)
        ];
    }

    /**
     * @Access (permission = "black_list: access")
     */
    public function apiGetBlackList(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $data = BlackList::select('*');

        if ($request->has('msisdn') && $request->get('msisdn') !== '' && $request->get('msisdn') !== null) {
            $data->where('msisdn', $request->get('msisdn'));
        }

        if ($request->has('type') && $request->get('type') !== '' && $request->get('type') !== null) {
            $data->where('type', $request->get('type'));
        }

        return [
            'data' => $data->orderBy('created_time', 'DESC')->paginate($per_page)
        ];
    }

    /**
     * @Access (permission = "black_list_update: access")
     */
    public function apiPostBlackList(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'msisdn.required' => 'Bạn chưa nhập số điện thoại!',
            'type.required' => 'Bạn chưa chọn loại BlackList!'
        ];

        $okay = Validator::make($data, [
            'msisdn' => 'required',
            'type' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return array('errors' => $okay->errors()->first());
        }

        //insert

        //check xem tt sdt chua
        $check = BlackList::where('msisdn', $data['msisdn'])->first();

        if ($check) {
            return array('errors' => "Thuê bao đã được thêm!");
        }

        $blacklist = new BlackList();
        $blacklist->msisdn = $data['msisdn'];
        $blacklist->type = $data['type'];
        $blacklist->created_time = date('Y-m-d H:i:s', time());

        $blacklist->save();

        try {
            $blacklist->save();

            $sys_event = [
                'type' => 'create_blacklist',
                'description' => auth()->user()->name . " đã thêm số " . $data['msisdn'] . " vào blacklist",
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Add BlackList thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Add BlackList không thành công'
            ];
        }
    }

    /**
     * @Access (permission = "black_list_update: access")
     */
    public function apiPutBlackList(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'msisdn.required' => 'Bạn chưa nhập số điện thoại!',
            'type.required' => 'Bạn chưa chọn loại BlackList!'
        ];

        $okay = Validator::make($data, [
            'msisdn' => 'required',
            'type' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return array('errors' => $okay->errors()->first());
        }

        //insert

        $blacklist = BlackList::where('msisdn', $data['msisdn'])->first();
        $blacklist->type = $data['type'];

        try {
            $blacklist->save();

            $sys_event = [
                'type' => 'update_blacklist',
                'description' => auth()->user()->name . " đã cập nhật số " . $data['msisdn'] . " trong blacklist",
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Add BlackList thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Add BlackList không thành công'
            ];
        }
    }

    /**
     * @Access (permission = "black_list_update: access")
     */
    public function apiDeleteBlackList(Request $request)
    {
        $data = $request->all();

        $blacklist = BlackList::where('msisdn', $data['msisdn'])->first();

        if (!$blacklist) {
            return array('errors' => "Số điện thoại ko có trong BlackList! Vui lòng kiểm tra lại !");
        }

        try {

            DB::table('blacklist')->where('msisdn', $data['msisdn'])->delete();

            $sys_event = [
                'type' => 'delete_blacklist',
                'description' => auth()->user()->name . " đã xóa số " . $data['msisdn'] . " trong blacklist",
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Xóa BlackList thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Xóa BlackList không thành công'
            ];
        }
    }

    public function apiExportLogCharge(Request $request)
    {
        $isdn = $request->get('isdn');

        $logs = His::where('msisdn', $isdn)->orderBy('created_time', 'DESC')->get();

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Dữ liệu Charge');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'MSISDN');
        $excel->getActiveSheet()->setCellValue('C1', 'Action');
        $excel->getActiveSheet()->setCellValue('D1', 'Giá charge');
        $excel->getActiveSheet()->setCellValue('E1', 'Kết quả');
        $excel->getActiveSheet()->setCellValue('F1', 'Thời gian');
        $excel->getActiveSheet()->setCellValue('G1', 'Kênh thao tác');

        $i = 2;
        foreach ($logs as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->msisdn);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->action);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->c_fee);
            $excel->getActiveSheet()->setCellValue('E' . $i, intval($row->result) === 0 ? 'Thành công' : 'Thất bại');
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->created_time);
            $excel->getActiveSheet()->setCellValue('G' . $i, $row->channel);

            $i++;
        }

        $path = $this->mkdirDir();
        $link_show = '/storage/' . $path;
        $link = storage_path("app/public/" . $path);

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];

    }

    public function apiExportLogMo(Request $request)
    {
        $isdn = $request->get('isdn');

        $logs = MO::where('msisdn', $isdn)->orderBy('created_time', 'DESC')->get();

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Dữ liệu MO');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'MSISDN');
        $excel->getActiveSheet()->setCellValue('C1', 'Action');
        $excel->getActiveSheet()->setCellValue('D1', 'Nội dung MO');
        $excel->getActiveSheet()->setCellValue('E1', 'Thời gian');
        $excel->getActiveSheet()->setCellValue('F1', 'Kênh thao tác');

        $i = 2;
        foreach ($logs as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->msisdn);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->action);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->mo);
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->created_time);
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->channel);

            $i++;
        }

        $path = $this->mkdirDir();
        $link_show = '/storage/' . $path;
        $link = storage_path("app/public/" . $path);

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];

    }

    public function apiExportLogMt(Request $request)
    {
        $isdn = $request->get('isdn');

        $logs = MT::where('msisdn', $isdn)->where('type', 1)->orderBy('created_time', 'DESC')->get();

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Dữ liệu MT');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'MSISDN');
        $excel->getActiveSheet()->setCellValue('C1', 'Action');
        $excel->getActiveSheet()->setCellValue('D1', 'Nội dung MT');
        $excel->getActiveSheet()->setCellValue('E1', 'Thời gian');
        $excel->getActiveSheet()->setCellValue('F1', 'Kênh thao tác');

        $i = 2;
        foreach ($logs as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->msisdn);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->action);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->mt);
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->created_time);
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->channel);

            $i++;
        }

        $path = $this->mkdirDir();
        $link_show = '/storage/' . $path;
        $link = storage_path("app/public/" . $path);

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];

    }

    public function apiExportLogRegister(Request $request)
    {
        $isdn = $request->get('isdn');

        $logs = His::where('msisdn', $isdn)->whereIn('action', ['FIRST_REG', 'RE_REG', 'RENEW'])->orderBy('created_time', 'DESC')->get();

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Dữ liệu Register');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'MSISDN');
        $excel->getActiveSheet()->setCellValue('C1', 'Action');
        $excel->getActiveSheet()->setCellValue('D1', 'Giá charge');
        $excel->getActiveSheet()->setCellValue('E1', 'Kết quả');
        $excel->getActiveSheet()->setCellValue('F1', 'Thời gian');
        $excel->getActiveSheet()->setCellValue('G1', 'Kênh thao tác');

        $i = 2;
        foreach ($logs as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->msisdn);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->action);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->c_fee);
            $excel->getActiveSheet()->setCellValue('E' . $i, intval($row->result) === 0 ? 'Thành công' : 'Thất bại');
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->created_time);
            $excel->getActiveSheet()->setCellValue('G' . $i, $row->channel);

            $i++;
        }

        $path = $this->mkdirDir();
        $link_show = '/storage/' . $path;
        $link = storage_path("app/public/" . $path);

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function apiExportLogUnRegister(Request $request)
    {
        $isdn = $request->get('isdn');

        $logs = His::where('msisdn', $isdn)->where('action', 'UNREG')->orderBy('created_time', 'DESC')->paginate($per_page);

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Dữ liệu UnRegister');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'MSISDN');
        $excel->getActiveSheet()->setCellValue('C1', 'Action');
        $excel->getActiveSheet()->setCellValue('D1', 'Giá charge');
        $excel->getActiveSheet()->setCellValue('E1', 'Kết quả');
        $excel->getActiveSheet()->setCellValue('F1', 'Thời gian');
        $excel->getActiveSheet()->setCellValue('G1', 'Kênh thao tác');

        $i = 2;
        foreach ($logs as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->msisdn);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->action);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->c_fee);
            $excel->getActiveSheet()->setCellValue('E' . $i, intval($row->result) === 0 ? 'Thành công' : 'Thất bại');
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->created_time);
            $excel->getActiveSheet()->setCellValue('G' . $i, $row->channel);

            $i++;
        }

        $path = $this->mkdirDir();
        $link_show = '/storage/' . $path;
        $link = storage_path("app/public/" . $path);

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function apiExportLogUssd(Request $request)
    {
        $isdn = $request->get('isdn');

        $logs = MT::where('msisdn', $isdn)->where('type', 2)->orderBy('created_time', 'DESC')->get();

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Dữ liệu Ussd');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'MSISDN');
        $excel->getActiveSheet()->setCellValue('C1', 'Action');
        $excel->getActiveSheet()->setCellValue('D1', 'Nội dung MT');
        $excel->getActiveSheet()->setCellValue('E1', 'Thời gian');
        $excel->getActiveSheet()->setCellValue('F1', 'Kênh thao tác');

        $i = 2;
        foreach ($logs as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->msisdn);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->action);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->mt);
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->created_time);
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->channel);

            $i++;
        }

        $path = $this->mkdirDir();
        $link_show = '/storage/' . $path;
        $link = storage_path("app/public/" . $path);

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function mkdirDir()
    {
        $dir = 'export' . '/' . date('Y/m/d') . '/';
        $file_name = sprintf("%s-%s-%s.%s", 'log', date('YmdHis'), str_random(5), 'xlsx');
        $link = storage_path("app/public/" . $dir . $file_name);

        if (!is_writable(dirname($link)) || !is_executable(dirname($link))) {
            mkdir(dirname($link), 0755, true);
        }

        return $dir . $file_name;
    }

    public function configInvite()
    {
        $this->views['title'] = 'Cấu hình mới USSD';

        return view('system.configinvite', $this->views);
    }

    public function apiPostConfigInvite(Request $request)
    {

        $data = $request->all();

        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', 'localhost:9090/handle',
            [
                \GuzzleHttp\RequestOptions::JSON => [
                    'action' => 'config',
                    'cmd' => 'update',
                    'data' => [
                        (object)array(
                            "key" => "tapping|patterm",
                            "value" => $data['invite']
                        ),
                        (object)array(
                            "key" => "tapping|balance",
                            "value" => $data['balance']
                        )
                    ]
                ]
            ]
        );

        return $response->getBody();
    }

    public function apiGetConfigInvite(Request $request)
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', "localhost:9090/handle?action=config&cmd=list-backup");

        $content = $response->getBody()->getContents();

        $content = str_replace("0|", "", $content);

        $data = [];

        foreach (json_decode($content) as $item) {
            $response = $client->request('GET', "localhost:9090/handle?action=config&cmd=view&fileName=" . $item);

            $content = $response->getBody()->getContents();

            $xml = simplexml_load_string(utf8_encode($content));

            $config = (string)$xml->tapping->patterm;

            $config = str_replace(["^[0-9]{10,11}(", ")$"], "", $config);

            $config = str_replace("|", ",", $config);

            array_push($data, [
                'fileName' => $item,
                'patterm' => $config,
                'balance' => (string)$xml->tapping->balance,
                'ip' => (string)$xml->ip
            ]);
        }

        return [
            'data' => $data
        ];

    }

    public function apiViewFileConfigInvite(Request $request)
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', "localhost:9090/handle?action=config&cmd=view");

        $content = $response->getBody()->getContents();

        $xml = simplexml_load_string(utf8_encode($content));

        $config = (string)$xml->tapping->patterm;

        $config = str_replace(["^[0-9]{10,11}(", ")$"], "", $config);

        $config = str_replace("|", ",", $config);

        return [
            'data' => $config
        ];

    }

    public function apiRollBackConfig(Request $request)
    {
        $data = $request->all();

        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', 'localhost:9090/handle',
            [
                \GuzzleHttp\RequestOptions::JSON => [
                    'action' => 'config',
                    'cmd' => 'rollback',
                    'data' => [
                        "name" => $data['idx']
                    ]
                ]
            ]
        );

        return $response->getBody();
    }

    public function informationServices()
    {
        return view('cmsNew.ThongTinDichVu');
    }
}
