<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Tags;
use App\Models\User;
use App\Models\Province;
use App\Models\Roles;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Carbon\Carbon;


class CategoryController extends Controller
{
    use DispatchesJobs;

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'chuyên mục',
            ],
        ];

        $this->views['breadcumbMain'] = "chuyên mục";
    }

    public function manage()
    {

        $this->views['title'] = 'Danh sách chuyên mục';

        return view('category.index', $this->views);
    }

    public function apiGetCategory(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $category = Category::select("*");

        if ($request->has('name') && $request->get('name') !== '' && $request->get('name') !== null) {
            $category->where('name', 'LIKE', "%{$request->get('name')}%");
        }

        $category = $category->orderBy('created_at', 'DESC');

        return [
            'data' => $category->paginate($per_page),
        ];
    }

    public function postCategory(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'name.required' => 'Bạn chưa nhập tên chuyên mục!',
            'description.required' => 'Bạn chưa nhập mô tả!'
        ];

        $okay = Validator::make($data, [
            'name' => 'required',
            'description' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        $category = new Category();

        $category->name = $data['name'];

        $category->slug = str_slug($data['name']);

        $category->status = $data['status'];

        $category->description = $data['description'];

        $category->seo_title = $data['seo_title'];

        $category->seo_description = $data['seo_description'];

        $category->seo_keywords = $data['seo_keywords'];

        $category->save();

        try {
            $category->save();
            return [
                'status' => 'success',
                'message' => 'Tạo chuyên mục thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'error',
                'message' => 'Tạo chuyên mục không thành công'
            ];
        }
    }

    public function putCategory(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'name.required' => 'Bạn chưa nhập tên chuyên mục!',
            'description.required' => 'Bạn chưa nhập mô tả!'
        ];

        $okay = Validator::make($data, [
            'name' => 'required',
            'description' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        $category = Category::find($data['id']);

        $category->name = $data['name'];

        $category->slug = str_slug($data['slug']);

        $category->status = $data['status'];

        $category->description = $data['description'];

        $category->seo_title = $data['seo_title'];

        $category->seo_description = $data['seo_description'];

        $category->seo_keywords = $data['seo_keywords'];

        $category->save();

        try {
            $category->save();
            return [
                'status' => 'success',
                'message' => 'Cập nhật chuyên mục thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'error',
                'message' => 'Cập nhật chuyên mục không thành công'
            ];
        }
    }


    public function keywords()
    {
        $this->views['title'] = 'Danh sách từ khóa';

        return view('category.keywords', $this->views);
    }

    public function apiGetKeywords(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $keywords = Tags::select("*");

        if ($request->has('name') && $request->get('name') !== '' && $request->get('name') !== null) {
            $keywords->where('name', 'LIKE', "%{$request->get('name')}%");
        }

        $keywords = $keywords->orderBy('created_at', 'DESC');

        return [
            'data' => $keywords->paginate($per_page),
        ];
    }

    public function postKeywords(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'name.required' => 'Bạn chưa nhập tên từ khóa!',
        ];

        $okay = Validator::make($data, [
            'name' => 'required',
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        $keyword = new Tags();

        $keyword->name = $data['name'];

        $keyword->slug = str_slug($data['name']);

        $keyword->weight = 0;

        try {
            $keyword->save();

            return [
                'status' => 'success',
                'message' => 'Tạo từ khóa thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'error',
                'message' => 'Tạo từ khóa không thành công'
            ];
        }
    }

    public function putKeywords(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'name.required' => 'Bạn chưa nhập tên từ khóa!',
        ];

        $okay = Validator::make($data, [
            'name' => 'required',
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        $keyword = Tags::find($data['id']);

        $keyword->name = $data['name'];

        $keyword->slug = $data['slug'];

        try {
            $keyword->save();

            return [
                'status' => 'success',
                'message' => 'Cập nhật từ khóa thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'error',
                'message' => 'Cập nhật từ khóa không thành công'
            ];
        }
    }

}
