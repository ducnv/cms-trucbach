<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'Cấu hình',
            ],
        ];

        $this->views['breadcumbMain'] = "Cấu hình";
    }

    public function manage()
    {
        $this->views['title'] = 'Cấu hình trang';

        return view('setting.index');
    }

    public function apiGetSetting()
    {
        $settings = [
        ];
        foreach (Setting::where('autoload', 1)->get(['key', 'value']) as $key) {
            $settings[$key->key] = $key->value;
        }
        return [
            'data' => $settings
        ];
    }

    public function apiUpdateSetting(Request $request)
    {
        $data = $request->all();

        try {
            foreach ($data as $key => $value) {
                if ($value != '' && $value != null) {
                    $setting = Setting::where('key', $key)->first();
                    $setting->value = $value;
                    $setting->save();
                }
            }
            return [
                'status' => 'success',
                'message' => 'Cấu hình thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'error',
                'message' => 'Cấu hình không thành công'
            ];
        }


    }

}
