<?php

namespace App\Http\Controllers;

use App\Models\Pages;
use App\Models\Category;
use App\Models\User;
use App\Models\Province;
use App\Models\Roles;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Carbon\Carbon;


class PagesController extends Controller
{
    use DispatchesJobs;

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'Trang',
            ],
        ];

        $this->views['breadcumbMain'] = "Trang";
    }

    public function manage()
    {

        $this->views['title'] = 'Danh sách Trang';

        return view('pages.index', $this->views);
    }

    public function getCreate()
    {
        $this->views['title'] = 'Tạo Trang';

        return view('pages.create', $this->views);
    }

    public function getEdit($id)
    {
        $pages = Pages::find($id);

        if (!$pages) {
            return redirect('/pages');
        }
        $this->views['breadcumbMain'] = "Chỉnh sửa";

        $this->views['pages'] = $pages;

        $this->views['title'] = $pages->name;

        return view('pages.edit', $this->views);

    }

    public function apiGetpages(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $pages = Pages::select("*");

        if ($request->has('name') && $request->get('name') !== '' && $request->get('name') !== null) {
            $pages->where('name', 'LIKE', "%{$request->get('name')}%");
        }

        $pages = $pages->orderBy('created_at', 'DESC');

        return [
            'data' => $pages->paginate($per_page),
        ];
    }

    public function postpages(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'name.required' => 'Bạn chưa nhập tên trang!',
        ];

        $okay = Validator::make($data, [
            'name' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        $pages = new Pages();

        $pages->user_id = auth()->user()->id;

        $pages->name = $data['name'];

        $pages->slug = str_slug($data['name']);

        $pages->description = str_replace("../", "", $data['description']);

        $pages->thumb = $data['thumb'];

        $pages->seo_title = $data['seo_title'];

        $pages->seo_keywords = $data['seo_keywords'];

        $pages->seo_description = $data['seo_description'];

        $pages->status = 'yes';

        $pages->save();

        try {
            $pages->save();
            return [
                'status' => 'success',
                'message' => 'Tạo Trang thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'error',
                'message' => 'Tạo Trang không thành công'
            ];
        }
    }

    public function putpages(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'name.required' => 'Bạn chưa nhập tên trang!',
        ];

        $okay = Validator::make($data, [
            'name' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        $pages = Pages::find($data['id']);

        $pages->user_id = auth()->user()->id;

        $pages->name = $data['name'];

        $pages->slug = str_slug($data['name']);

        $pages->description = str_replace("../", "", $data['description']);

        $pages->thumb = $data['thumb'];

        $pages->seo_title = $data['seo_title'];

        $pages->seo_keywords = $data['seo_keywords'];

        $pages->seo_description = $data['seo_description'];

        $pages->status = 'yes';

        $pages->save();

        try {
            $pages->save();
            return [
                'status' => 'success',
                'message' => 'Sửa Trang thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'error',
                'message' => 'Sửa Trang không thành công'
            ];
        }
    }

    public function apiGetCategory()
    {
        return [
            'data' => Category::select('id as value', 'name as text')->get(),
        ];
    }

}
