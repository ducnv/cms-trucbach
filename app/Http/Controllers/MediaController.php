<?php

namespace App\Http\Controllers;

use App\Models\Media;
use Illuminate\Http\Request;
use App\Jobs\ResizeImage;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Intervention\Image\Facades\Image;

class MediaController extends Controller
{
    use DispatchesJobs;

    protected $media_limit = 12;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->views['title'] = 'Thư viện';

        return view('media.index', $this->views);
    }

    public function getMedia($mediaId = null, Request $request)
    {

        $media = Media::select('*');

        if ($mediaId === null) {
            if ($request->has('name') && $request->get('name') !== null) {
                $media->where('name', 'LIKE', '%' . $request->get('name') . '%');
            }
            if ($request->has('date')) {
                list($start, $end) = explode('|', $request->get('date'));
                $media->where('created_at', '>',
                    Carbon::createFromFormat('Y-m-d H:i:s', $start . ' 00:00:00')
                );
                $media->where('created_at', '<',
                    Carbon::createFromFormat('Y-m-d H:i:s', $end . ' 23:59:59')
                );
            }
            if ($request->has('media_type') && $request->get('media_type') !== null) {
                $media->where('media_type', $request->get('media_type'));
            }
        } else {
            $media->where('id', $mediaId);
        }

        return [
            'data' => $media->orderBy('created_at', 'DESC')->paginate($this->media_limit)
        ];
    }

    public function createMedia(Request $request)
    {

//        $category_id = session('media_category_id');

        if (!$request->hasFile('file')) {
            abort(404, 'Không tồn tại file upload');
        }
        $extension = strtolower($request->file->getClientOriginalExtension());
        $media_type = $this->parseMediaType($extension);
        $dir = $media_type . '/' . date('Y/m/d');
        $file_name = sprintf("%s-%s-%s.%s", $media_type, date('YmdHis'), str_random(5), $extension);
        try {
            $path = $request->file->storeAs($dir, $file_name, 'public');

            $media = Media::create([
                'user_id' => auth()->user()->id,
//                'category_id' => intval($category_id),
                'uuid' => str_random(15),
                'media_type' => $media_type,
                'name' => $file_name,
                'extension' => $extension,
                'mime' => mime_content_type(storage_path('app/public/' . $path)),
                'file_name' => $file_name,
                'file_path' => $path,
                'file_url' => $this->getUrlForDisplay($path),
            ]);
        } catch (\Exception $ex) {
            Storage::delete('public/' . $dir . $file_name);
            return [
                'error' => 404,
                'message' => $ex->getMessage()
            ];
        }
        // TODO: generate job process image/video
        $err = $this->parseInfo($media);


        if ($err === true) {
            return [
                'data' => $media
            ];
        } else {
            return [
                'error' => 403,
                'message' => $err
            ];
        }
    }

    public function parseMediaType($extension)
    {
        if (in_array($extension, ['png', 'jpg', 'jpeg', 'gif'])) {
            return 'image';
        } elseif (in_array($extension, ['mp4', 'avi', 'mpeg'])) {
            return 'video';
        } else {
            return 'docs';
        }
    }

    public function parseUuid($file_name)
    {
        $tmp = explode('.', $file_name);
        return reset($tmp);
    }

    public function generateThumb($path)
    {
    }

    public function parseMimeType($path)
    {
        try {
            return File::mimeType(storage_path('app/public/' . $path));
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function parseInfo($media)
    {
        $media = Media::find($media->id);

        $thumb_path = $this->getThumbPath($media->file_path);

        $absolute_thumb_path = storage_path('app/public/' . $this->getThumbPath($media->file_path));

        if ($media->media_type == 'image') {
            // get height, width, exif, orientation
            $img = Image::make(storage_path('app/public/' . $media->file_path));
            $mime = $img->mime();
            $height = $img->height();
            $width = $img->width();
            $file_size = $img->filesize();
            $orientation = $height > $width ? 'portrait' : 'landscape';

            if (!is_writable(dirname($absolute_thumb_path)) || !is_executable(dirname($absolute_thumb_path))) {
                mkdir(dirname($absolute_thumb_path), 0755, true);
            }

            $thumb_image = $this->makeThumbFromImage(
                $media->file_path,
                $orientation
            );

            // update info
            $media->thumbnail = $thumb_image;
            $media->mime = $mime;
            $media->height = $height;
            $media->width = $width;
            $media->orientation = $orientation;
            $media->file_size = $file_size;
            $media->version = ['640x480', '640x360', '200x200'];

            try {
                $media->save();
                return true;
            } catch (\Exception $ex) {
                return $ex->getMessage();
            }
        } elseif ($media->media_type == 'video') {
            // make thumbnail from video
            try {
                $thumbnail = $this->makeThumbnailForVideo($media);
                $info = getVideoAttributes($media->file_path);

                $media->thumbnail = $thumbnail;
                $media->width = $info['width'];
                $media->height = $info['height'];
                $media->duration = $info['hours'] . ':' . $info['mins'] . ':' . $info['secs'] . ':' . $info['ms'];
                $media->file_size = filesize(storage_path('app/public/' . $media->file_path));

                $media->save();
                return true;
            } catch (\Exception $ex) {
                return $ex->getMessage();
            }
        }
    }

    public function makeThumbFromImage($path, $orientation)
    {
        list($file_name, $ext) = explode('.', $path);
        $link = sprintf("%s-%s.%s", $file_name, '200x200', 'png');

        try {
            $job = (new ResizeImage($path, $orientation))->delay(1);

            $this->dispatch($job);

        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }
        return $this->getUrlForDisplay($link);
    }

    public function makeThumbnailForVideo($media)
    {
        $thumb_path = dirname($media->file_path) . '/';
        $command = 'ffmpeg -i ' . storage_path('app/public/' . $media->file_path) . ' -ss 00:00:01.000 -vframes 1 ' . storage_path('app/public/' . $thumb_path . basename($media->file_path, '.' . $media->extension) . '.png');

        shell_exec($command);

        $thumbnail = $this->makeThumbFromImage(
            $thumb_path . basename($media->file_path, '.' . $media->extension) . '.png',
            'landscape'
        );
        return $thumbnail;
    }

    public function getThumbPath($img_path)
    {
        return dirname($img_path) . '/' . basename($img_path);
    }

    public function getUrlForDisplay($path)
    {
        return '/storage/' . $path;
    }

    /**
     * PUT /api/media
     *
     * @param Request $request
     */
    public function updateMedia($id, Request $request)
    {
        $media = Media::find($id);
        if (!$media) {
            abort(404, __('media.not_found'));
        }
        $data = $request->all();

        foreach ($data['data'] as $key => $value) {
            $media->$key = $value;
        }
        try {
            $media->save();
            return [
                'data' => $media
            ];
        } catch (\Exception $ex) {
            abort(400, $ex->getMessage());
        }
    }

    /**
     * @Access (permission = "media: delete")
     */
    public function deleteMedia(Request $request)
    {
        $data = $request->get('data')['selectedImage'];

        foreach ($data as $media) {
            //xoa file goc
            unlink(storage_path('app/public/' . $media['file_path']));
            if ($media['media_type'] == 'image' || $media['media_type'] == 'video') {
                // xoa file resize
                $link = str_replace(['.png', '.jpg', '.jpeg', '.mp4'], '', $media['file_path']);
                if ($media['media_type'] == 'video') {
                    unlink(storage_path('app/public/' . $link . '.png'));
                }
                unlink(storage_path('app/public/' . $link . '-200x200.png'));
                unlink(storage_path('app/public/' . $link . '-640x480.png'));
                unlink(storage_path('app/public/' . $link . '-640x360.png'));
            }

            $medias = Media::find($media['id']);
            $name = $medias->name;
            $medias->delete();

            $sys_event = [
                'type' => 'delete_media',
                'description' => auth()->user()->name . " đã xóa file : " . $name,
                'from' => 'User',
                'user_id' => auth()->user()->id
            ];
            $this->dispatch((new LogSystemEvent($sys_event))->delay(30));
        }

        return [
            'data' => 'sussces'
        ];
    }
}
