<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
//        if (Auth::attempt($credentials, $request->has('remember'))) {
//
//            return array("success" => 'Đăng nhập thành công', "url" => '/nguoi-tim-viec/quan-ly-ho-so');
//        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        return view('auth.dangxuat');
    }

    function callAPI($url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ]);
// Send the request & save response to $resp
        $resp = curl_exec($curl);
// Close request to clear up some resources
        curl_close($curl);
    }

}
