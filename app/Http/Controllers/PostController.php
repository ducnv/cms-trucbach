<?php

namespace App\Http\Controllers;

use App\Models\KNTB_POSTMETA;
use App\Models\KNTB_TERM_RELATIONSHIPS;
use App\Models\KNTB_TERMS;
use App\Models\KNTB_USERMETA;
use App\Models\Packages;
use App\Models\Post;
use App\Models\Subscriber;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Config;

class PostController extends Controller
{
    protected $TERMS = '';
    protected $POST = '';
    protected $POSTMETA = '';
    protected $PACKAGES = '';
    protected $KNTB_TERM_RELATIONSHIPS = '';
    protected $SUBSCRIBER = '';
    protected $USERMETA = '';
    public function __construct(Request $request, KNTB_USERMETA $USERMETA,KNTB_TERMS $TERMS, Post $POST, KNTB_POSTMETA $POSTMETA, Packages $PACKAGES, KNTB_TERM_RELATIONSHIPS $KNTB_TERM_RELATIONSHIPS, SUBSCRIBER $SUBSCRIBER)
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'Quản trị tin đăng'
            ],
        ];

        $this->views['breadcumbMain'] = "Quản trị tin đăng";

        $this->TERMS = $TERMS;
        $this->POST = $POST;
        $this->POSTMETA = $POSTMETA;
        $this->PACKAGES = $PACKAGES;
        $this->KNTB_TERM_RELATIONSHIPS = $KNTB_TERM_RELATIONSHIPS;
        $this->SUBSCRIBER = $SUBSCRIBER;
        $this->USERMETA = $USERMETA;
    }

    /**
     * @Access (permission = "post: access")
     */
    public function changeStatusSupply()
    {
        $this->views['title'] = 'Trạng thái tin nguồn cung';

        return view('post.postSupply', $this->views);
    }

    /**
     * @Access (permission = "post: access")
     */
    public function hoSoDemandsources()
    {
        $this->views['title'] = 'Hồ sơ nguồn cầu';

        return view('post.hoSoDemandSources', $this->views);
    }
    /**
     * @Access (permission = "post: access")
     */
    public function hoSoSupply()
    {
        $this->views['title'] = 'Hồ sơ nguồn cung';

        return view('post.hoSoSupply', $this->views);
    }

    /**
     * @Access (permission = "post: access")
     */
    public function apiGetPostSupply(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $contents = Post::where('sub_type',1);

        $name = $request->get('name');
        if ($request->has('name') && $name !== '' && $name !== null) {
            $contents->where('post_title', 'LIKE', "%{$name}%")->orWhere('post_code','LIKE',"%{$name}%");
        }

        $packageName = $request->get('packageName');
        if ($request->has('name') && $packageName !== '' && $packageName !== null) {
            $contents->where('pkg_code', 'LIKE', "%{$packageName}%");
        }

        $status = $request->get('status');
        if ($request->has('name') && $status !== '' && $status !== null) {
            $contents->where('post_status', 'LIKE', "%{$status}%");
        }

//        $expiredTime = $request->get('expiredTime');
//        if ($request->has('name') && $expiredTime !== '' && $expiredTime !== null) {
//            if ($expiredTime == 'limited'){
//                $contents->where('expired_time', '>=', Carbon::now());
//            }elseif ($expiredTime == 'expired'){
//                $contents->where('expired_time', '<', Carbon::now());
//            }
//        }

        $contents = $contents->orderBy('created_time', 'DESC')->paginate($per_page);
        foreach ($contents as $key => $value){
            $subscriber = Subscriber::where('msisdn',$value->msisdn)->where('pkg_code',$value->pkg_code)->first();
            if($value->expired_time < Carbon::now() || $subscriber->service_state != 1 || $subscriber->expired_time < Carbon::now()){
                $value->post_status = 'close';
            }
        }
        return [
            'data' => $contents,
        ];
    }

    /**
     * @Access (permission = "post: access")
     */
    public function apiGetHoSoDemandsources(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $contents = Post::where('sub_type',2);

        $name = $request->get('name');
        if ($request->has('name') && $name !== '' && $name !== null) {
            $contents->where('post_title', 'LIKE', "%{$name}%")->orWhere('post_code','LIKE',"%{$name}%");
        }

        $packageName = $request->get('packageName');
        if ($request->has('name') && $packageName !== '' && $packageName !== null) {
            $contents->where('pkg_code', 'LIKE', "%{$packageName}%");
        }

        $status = $request->get('status');
        if ($request->has('name') && $status !== '' && $status !== null) {
            $contents->where('post_status', 'LIKE', "%{$status}%");
        }

        $contents = $contents->orderBy('created_time', 'DESC')->paginate($per_page);

//        foreach ($contents as $key => $value){
//            $subscriber = Subscriber::where('msisdn',$value->msisdn)->where('pkg_code',$value->pkg_code)->first();
//
//            if($subscriber->service_state != 1 || $subscriber->expired_time < Carbon::now()){
//                $value->post_status = 'close';
//            }
//        }


        return [
            'data' => $contents,
        ];
    }

    /**
     * @Access (permission = "post: access")
     */
    public function apiGetHoSoSupply(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $contents = $this->USERMETA;

        $name = $request->get('name');
        if ($request->has('name') && $name !== '' && $name !== null) {
            $contents->where('msisdn', 'LIKE', "%{$name}%");
        }
//
//        $packageName = $request->get('packageName');
//        if ($request->has('name') && $packageName !== '' && $packageName !== null) {
//            $contents->where('pkg_code', 'LIKE', "%{$packageName}%");
//        }
//
//        $status = $request->get('status');
//        if ($request->has('name') && $status !== '' && $status !== null) {
//            $contents->where('post_status', 'LIKE', "%{$status}%");
//        }
//
//        $contents = $contents->orderBy('created_time', 'DESC')->paginate($per_page);
//        foreach ($contents as $key => $value){
//            $subscriber = Subscriber::where('msisdn',$value->msisdn)->where('pkg_code',$value->pkg_code)->first();
//
//            if($subscriber->service_state != 1 || $subscriber->expired_time < Carbon::now()){
//                $value->post_status = 'close';
//            }
//        }

        $contents = $contents->orderBy('id', 'DESC')->where([ 'PKG_CODE' => null])->paginate($per_page);
        return [
            'data' => $contents,
        ];



    }

    public function apiChangeStatus(Request $request){
        $contents = DB::table('KNTB_POST')->whereIn('id',$request->listSelected)->get();

        foreach ($contents as $key => $value){
            $subscriber = Subscriber::where('msisdn',$value->msisdn)->where('pkg_code',$value->pkg_code)->first();

            if($value->expired_time == null){

                if( $subscriber->service_state != 1 || $subscriber->expired_time < Carbon::now()){
                    $flag = 1;
                }
            }else{
                if($value->expired_time < Carbon::now() || $subscriber->service_state != 1 || $subscriber->expired_time < Carbon::now()){
                    $flag = 1;
                }
            }

            if ($request->statusName == 'published' && $value->post_status == 'published'){
                $flag = 2;
            }

            if ($request->statusName == 'close' && $value->post_status == 'close'){
                $flag = 3;
            }

        }
        if (!empty($flag) && $flag == 1){

            $data = [
                'status' => 'error',
                'message' => 'Thay đổi trạng thái thất bại'
            ];
        }elseif (!empty($flag) && $flag == 2){
            $data = [
                'status' => 'error',
                'message' => 'Tin đang ở trạng thái Published'
            ];
        }elseif (!empty($flag) && $flag == 3){
            $data = [
                'status' => 'error',
                'message' => 'Tin đang ở trạng thái Close'
            ];
        }else{
            $data = [
                'status' => 'success',
                'message' => 'Thay đổi trạng thái thành công'
            ];
            DB::table('KNTB_POST')->whereIn('id',$request->listSelected)->update(['post_status' => $request->statusName]);
            foreach ($contents as $key => $value){
                if ($request->statusName == 'published'){
                    if ($value->modified_time == null ){
                        $this->sendApiPostAdd($value->post_code);
                    }else{
                        $this->sendApiPostEdit($value->post_code);
                    }
                }elseif ($request->statusName == 'close'){
                    $this->sendApiPostDelete($value->pkg_code,$value->msisdn,$value->sub_type);
                }

            }
        }

        return $data;
    }

    public function apiChangeStatusDeman(Request $request){
        $contents = DB::table('KNTB_POST')->whereIn('id',$request->listSelected)->get();

        foreach ($contents as $key => $value){

//            $subscriber = Subscriber::where('msisdn',$value->msisdn)->where('pkg_code',$value->pkg_code)->first();
//            if($value->expired_time == null){
//
//                if( $subscriber->service_state != 1 || $subscriber->expired_time < Carbon::now()){
//                    $flag = 1;
//                }
//            }else{
//                if($value->expired_time < Carbon::now() || $subscriber->service_state != 1 || $subscriber->expired_time < Carbon::now()){
//                    $flag = 1;
//                }
//            }

            if ($request->statusName == 'published' && $value->post_status == 'published'){
                $flag = 2;
            }

            if ($request->statusName == 'close' && $value->post_status == 'close'){
                $flag = 3;
            }

        }
//        if (!empty($flag) && $flag == 1){
//
//            $data = [
//                'status' => 'error',
//                'message' => 'Thay đổi trạng thái thất bại'
//            ];
//        }else
        if (!empty($flag) && $flag == 2){
            $data = [
                'status' => 'error',
                'message' => 'Tin đang ở trạng thái Published'
            ];
        }elseif (!empty($flag) && $flag == 3){
            $data = [
                'status' => 'error',
                'message' => 'Tin đang ở trạng thái Close'
            ];
        }else{
            $data = [
                'status' => 'success',
                'message' => 'Thay đổi trạng thái thành công'
            ];
            DB::table('KNTB_POST')->whereIn('id',$request->listSelected)->update(['post_status' => $request->statusName]);
            foreach ($contents as $key => $value){
                if ($request->statusName == 'published'){
                    if ($value->modified_time == null ){
                        $this->sendApiPostAdd($value->post_code);
                    }else{
                        $this->sendApiPostEdit($value->post_code);
                    }
                }elseif ($request->statusName == 'close'){
                    $this->sendApiPostDelete($value->pkg_code,$value->msisdn,$value->sub_type);
                }

            }

        }

        return $data;
    }

    public function sendApiPostAdd($data){

        try {
            $client = new Client();
            $client->request('GET', 'http://localhost:9006/kntb/api/match/submit/post', [
                'query' => ['postCode' => $data]
            ]);
        }catch(\Exception $e) {
            echo 'Send API Error ! ' .$e->getMessage();
        }
    }

    public function sendApiPostEdit($data){
        try {
            $client = new Client();
            $client->request('GET', 'http://localhost:9006/kntb/api/match/edit/post', [
                'query' => ['postCode' => $data]
            ]);
        }catch(\Exception $e) {
            echo 'Send API Error ! ' .$e->getMessage();
        }
    }

    public function sendApiPostDelete($data,$msisdn,$type){
        try {
            $client = new Client();
            $client->request('GET', 'http://localhost:9006/kntb/api/match/delete/post', [
                'query' => ['packageCode' => $data,'msisdn' => $msisdn,'type' => $type]
            ]);
        }catch(\Exception $e) {
            echo 'Send API Error ! ' .$e->getMessage();
        }
    }


    //nhac ben web kntb
    public function getAddPost()
    {

        $package = $this->PACKAGES->get();
//        dd($package);
        $wage = $this->TERMS->where('TAXONOMY', 'nip_wage')->get();
        $province = $this->TERMS->where('TAXONOMY', 'nip_province')->get();
        $data = [
            'package' => $package,
            'wage' => $wage,
            'province' => $province,
        ];
        return view('post.addPostSupply', $data);
    }

    public function ckeckSDTSupply(Request $request){
        $sdt = $this->SUBSCRIBER->with('packages')->where([
                'MSISDN' => $request->sdt,
                'SUB_TYPE' => 1,
                'SERVICE_STATE' => 1,
            ])->get();

//        $package = $this->SUBSCRIBER->with('packages')
//            ->where([
//                'MSISDN' => Config::get('package.numbumerPhone'),
//                'SUB_TYPE' => 1,
//                'SERVICE_STATE' => 1,
//            ])->where('EXPIRED_TIME','>=', Carbon::now())->get();
        if (!empty($sdt) && count($sdt)>0){
            $data = ['sdt' => $sdt];

        }else{
            $data = ['error' => true];

        }
        return response($data);
    }

    public function ckeckHSSDTDeman(Request $request,$PKG_Code){
        $sdt = $this->SUBSCRIBER->with('packages')->where([
            'MSISDN' => $request->sdt,
            'SUB_TYPE' => 2,
            'PKG_CODE' => $PKG_Code,
            'SERVICE_STATE'=> 1

        ])->get();
        $checkInPost = $this->USERMETA->where([
            'MSISDN' => $request->sdt,
            'pkg_code' => $PKG_Code,
        ])->get();

//        $package = $this->SUBSCRIBER->with('packages')
//            ->where([
//                'MSISDN' => Config::get('package.numbumerPhone'),
//                'SUB_TYPE' => 1,
//                'SERVICE_STATE' => 1,
//            ])->where('EXPIRED_TIME','>=', Carbon::now())->get();
        if (!empty($sdt) && count($sdt)>0 && count($checkInPost) == 0){
            $data = ['sdt' => $sdt];

        }else{
            $data = ['error' => true];

        }
        return response($data);
    }

    public function ckeckHSSDTSupply(Request $request){
        $sdt = $this->SUBSCRIBER->with('packages')->where([
            'MSISDN' => $request->sdt,
            'SUB_TYPE' => 2,
        ])->get();
        $checkInPost = $this->USERMETA->where([
            'MSISDN' => $request->sdt,
        ])->get();
//        $package = $this->SUBSCRIBER->with('packages')
//            ->where([
//                'MSISDN' => Config::get('package.numbumerPhone'),
//                'SUB_TYPE' => 1,
//                'SERVICE_STATE' => 1,
//            ])->where('EXPIRED_TIME','>=', Carbon::now())->get();
        if (!empty($sdt) && count($sdt)>0 && count($checkInPost) == 0){
            $data = ['sdt' => $sdt];

        }else{
            $data = ['error' => true];

        }
        return response($data);
    }

    public function ckeckProvince(Request $request)
    {
        $province = $request->province;
        return response($this->TERMS->showProvince($province));
    }

    public function postAddPost(Request $request)
    {

        $data = $request->except('_token');
//        dd($data);
        $data['timeOut'] = date_format(Carbon::createFromFormat('Y-m-d', $data['timeOut'])->addDays(1), 'Y-m-d');
        if ($request->benefitPackage == Config::get('package.namePackage.job_supply') && !empty($request->wage)) {
            $post = $this->POST->insertPost($data,$request->sdt);
            $postId = $post;
            $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->wage);
        } elseif ($request->benefitPackage != Config::get('package.namePackage.job_supply') && !empty($request->price)) {
            $post = $this->POST->insertPost($data,$request->sdt);
            $postId = $post;
            $price = $this->denominations($request->denominations, $request->price);
            $this->POSTMETA->insertPostMeta('nip_price', $price, $postId);
        } else {
            return redirect()->back()->with(['alert' => 'success', 'message' => 'ERROR']);
        }

        $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->exigency);
        $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->classify);
        $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->nip_province);
        $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->nip_district);
        if (!empty($request->nip_address)) {
            $this->POSTMETA->insertPostMeta('nip_post_address', $request->nip_address, $postId);

        }
        return redirect()->back()->with(['alert' => 'success', 'message' => 'Đăng tin thành công']);
    }

    public function denominations($denominations,$requestPrice){
        if ($denominations == 'Đồng'){
            $price = $requestPrice;
        }elseif($denominations == 'Triệu'){
            $price = $requestPrice.'000000';
        }elseif($denominations == 'Tỷ'){
            $price = $requestPrice.'000000000';
        }
        return $price;
    }

    public function getEditPost($id)
    {
//        $id = $this->POST->convertSidlug($slug);
        $post = $this->POST->where('id', $id)->with('postMeta')->with(['termRelationships' => function ($query) {
            $query->with('terms');
        }])->first();
        //check value edit post
        $valueRelationships = $post['termRelationships']->pluck('terms')->pluck('id', 'taxonomy');

        $postMeta = $post->postMeta->pluck('meta_value', 'meta_key');

        $package = $this->SUBSCRIBER->with('packages')
            ->where([
                'MSISDN' => $post->msisdn,
                'SUB_TYPE' => 1,
            ])->get();
//        $package = $this->PACKAGES->where('pkg_type', 1)->get();

        $showMeta = $this->TERMS->showPackage($post->pkg_code);

        $wage = $this->TERMS->where('TAXONOMY', 'nip_wage')->get();
        $price = $this->POSTMETA->where([
            'POST_ID' => $id,
            'META_KEY' => 'nip_price',
        ])->value('meta_value');

        $province = $this->TERMS->where('TAXONOMY', 'nip_province')->get();

        $district = $this->TERMS->where([
            'TAXONOMY' => 'nip_district',
            'parent' => $valueRelationships['nip_province'],
        ])->get();

        $data = [
            'post' => $post,
            'postMeta' => $postMeta,
            'package' => $package,
            'showMeta' => $showMeta,
            'wage' => $wage,
            'price' => $price,
            'province' => $province,
            'district' => $district,
            'valueRelationships' => $valueRelationships,
        ];
//        dd($data);

        return view('post.editPostSupply', $data);
    }

    public function postEditPost(Request $request, $id)
    {
//        $id = $this->POST->convertSlug($slug);id
        $data = $request->except('_token');
//        dd($data);

        if ($request->benefitPackage == Config::get('package.namePackage.job_supply') && !empty($request->wage)) {
            $post = $this->POST->insertPost($data,$data['sdt'], $id);
            $postId = $post;

            $checkPrice = $this->POSTMETA->where([
                'POST_ID' => $postId,
                'META_KEY' => 'nip_price',
            ])->first();
            if (!empty($checkPrice)) {
                $this->POSTMETA->where(['POST_ID' => $postId, 'META_KEY' => 'nip_price'])->delete();
            }

            $this->KNTB_TERM_RELATIONSHIPS->where('post_id', $postId)->delete();
            $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->wage);
        } elseif ($request->benefitPackage != Config::get('package.namePackage.job_supply') && !empty($request->price)) {
            $post = $this->POST->insertPost($data,$data['sdt'], $id);
            $postId = $post;

            $this->KNTB_TERM_RELATIONSHIPS->where('post_id', $postId)->delete();

            $postMetaId = $this->POSTMETA->where(['POST_ID' => $postId, 'META_KEY' => 'nip_price'])->value('id');
//            $price = implode(explode('.',$request->price));
            $price = $this->denominations($request->denominations,$request->price);
            $this->POSTMETA->insertPostMeta('nip_price', $price, $postId,$postMetaId);
        } else {
            return redirect()->back()->with(['alert' => 'error', 'message' => 'ERROR']);
        }

        $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->exigency);
        $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->classify);

        $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->nip_province);
        $this->KNTB_TERM_RELATIONSHIPS->insertTermRelationships($postId, $request->nip_district);

        $nip_post_address = $this->POSTMETA->idPostMeta($id, 'nip_post_address');

        if (!empty($request->nip_address)){
            $this->POSTMETA->insertPostMeta('nip_post_address', $request->nip_address, $postId, $nip_post_address);
        }
//        $postCode = $this->POST->where('id',$postId)->value('post_code');
//        $this->POST->sendApiPostEdit($postCode);

        return redirect()->route('changeStatusSupply')->with(['alert' => 'success', 'message' => 'Cập nhật thành công. Tin đăng ở trạng thái chờ duyệt']);
    }
}
