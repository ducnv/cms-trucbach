<?php

namespace App\Http\Controllers;

use App\Models\SystemEvents;
use App\Models\User;
use App\Models\Roles;
use Illuminate\Http\Request;
use App\Http\Requests\CreateNewUser;
use Session;
use Carbon\Carbon;
use App\Jobs\LogSystemEvent;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    use DispatchesJobs;

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'Người dùng'
            ],
        ];

        $this->views['breadcumbMain'] = "Người dùng";
    }

    /**
     * @Access (permission = "user: access")
     */
    public function manageUser()
    {
        $this->views['title'] = 'Danh sách người dùng';

        return view('user.manage_user', $this->views);
    }

    /**
     * @Access (permission = "user: access")
     */
    public function apiGetUsers(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 10;

        return ['data' => User::paginate($per_page)];
    }

    public function apiGetRoles(Request $request)
    {
        return [
            'data' => Roles::all()
        ];

    }


    /**
     * @Access (permission = "user: create")
     */
    public function apiCreateUser(CreateNewUser $request)
    {
        $user = new User();
        $user->name = $request->get('name');
        $user->username = str_slug($request->get('name'));
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->role_id = $request->get('role_id');
        $user->api_token = str_random(64);

        $user->session_id = Session::getId();
        $user->last_login = Carbon::now();
        $user->last_login_ip = \Request::ip();

        try {
            $user->save();

            $sys_event = [
                'type' => 'create_user',
                'description' => auth()->user()->name . " đã tạo người dùng mới " . $user->name,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));


            return [
                'status' => 'success',
                'message' => 'Tạo tài khoản thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Tạo tài khoản không thành công'
            ];
        }
    }

    public function apiUpdateUser($userID, Request $request)
    {
        $user = User::findOrFail($userID);
        if ($request->has('status')) {
            $user->status = $request->get('status');
        }
        if ($request->has('name')) {
            $user->name = $request->get('name');
        }
        if ($request->has('email')) {
            $user->email = $request->get('email');
        }
        if ($request->has('role_id')) {
            $user->role_id = $request->get('role_id');
        }
        if ($request->has('password')) {
            $user->password = bcrypt($request->get('password'));
        }
        try {
            $user->save();

            $sys_event = [
                'type' => 'update_user',
                'description' => auth()->user()->name . " đã cập nhật người dùng " . $user->name,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));


            return [
                'status' => 'success',
                'message' => 'Cập nhật tài khoản thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Cập nhật tài khoản không thành công'
            ];
        }
    }

    /**
     * @Access (permission = "user: permission")
     */
    public function manageRoles()
    {
        $this->views['title'] = 'Quản lý quyền người dùng';

        return view('user.managepermission', $this->views);
    }

    public function getPermissions()
    {
        return [
            'data' => parse_package_config('permission')
        ];
    }

    /**
     * @Access (permission = "user: permission")
     */
    public function roleUpdatePermissions(Request $request)
    {
        $data = $request->get('roles');

        try {
            foreach ($data as $item) {
                $role = Roles::find($item['id']);
                $role->permissions = $item['permissions'];
                $role->save();
            }

            $sys_event = [
                'type' => 'permission_user',
                'description' => auth()->user()->name . " đã cập nhật quyền hạn",
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Cập nhật quyền hạn thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Cập nhật quyền hạn không thành công'
            ];
        }
    }

    public function profileUser()
    {
        $this->views['title'] = 'Trang cá nhân';

        return view('user.profile', $this->views);
    }

    public function apiProfileUpdate(Request $request)
    {
        $data = $request->all();

        $user = auth()->user();

        $user->name = $data['name'];

        try {

            $user->save();

            return [
                'status' => 'success',
                'message' => 'Cập nhật thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Cập nhật không thành công'
            ];
        }

    }

    public function apiChangePass(Request $request)
    {
        $data = $request->all();

        $messages = [
            'current.required' => 'Yêu cầu nhập mật khẩu cũ',
            'new_pass.required' => 'Yêu cầu nhập mật khẩu mới',
            'confirm_new.required' => 'Yêu cầu nhập lại mật khẩu mới',
            'confirm_new.same' => 'Mật khẩu không giống nhau',
            'new_pass.min' => 'Mật khẩu mới tối thiểu 6 ký tự!'
        ];

        $okay = Validator::make($request->all(), [
            'current' => 'required',
            'new_pass' => 'required|min:6',
            'confirm_new' => 'required|same:new_pass'
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        if (!(Hash::check($request->get('current'), Auth::user()->password))) {
            return [
                'status' => 'fail',
                'message' => "Mật khẩu cũ không đúng"
            ];
        }

        if (strcmp($request->get('current'), $request->get('new_pass')) == 0) {
            return [
                'status' => 'fail',
                'message' => "Mật khẩu mới giống mật khẩu cũ"
            ];
        }

        $user = auth()->user();

        $user->password = bcrypt($data['new_pass']);

        try {

            $user->save();

            return [
                'status' => 'success',
                'message' => 'Đổi mật khẩu thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Đổi mật khẩu không thành công'
            ];
        }
    }

    public function apiGetHistory(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $history = SystemEvents::select("*")->where('user_id', auth()->user()->id);

        $history = $history->orderBy('created_at', 'DESC');

        return [
            'data' => $history->paginate($per_page),
        ];
    }


}
