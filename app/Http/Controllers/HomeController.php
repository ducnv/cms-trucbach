<?php

namespace App\Http\Controllers;

use App\Models\StatsPending;
use App\Models\StatsUnreg;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //neu la cskh thi chuyen sang trang cham soc khac hang luon
        if (intval(auth()->user()->role_id) === 5) {
            return redirect('/cskh/tra-cuu-thue-bao');
        }

        return view('home');
    }


    /**
     * @Access (permission = "sanluong: access")
     */
    public function apiChart(Request $request)
    {
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        return [
            'chartsub' => $this->chartSub($startDate, $endDate),
            'chartdk' => $this->chartDK($startDate, $endDate),
            'charthuydk' => $this->chartHuyDK($startDate, $endDate),
            'chartdt' => $this->chartDT($startDate, $endDate),
            'chartpiedt' => $this->chartPieDT($startDate, $endDate),
            'chartdkhourly' => $this->chartDKHourly(),
            'chartdthourly' => $this->chartDTHourly(),
        ];
    }

    public function chartDTHourly()
    {
        $str = "";
        $presub = "";
        $predt = "";
        $datasub = "";
        $datadt = "";
        $totalSub = 0;
        $totaldt = 0;

        $dateCompare = date("d/m/Y", strtotime("-1 day"));
        $preDay = date('d', strtotime("-1 day"));

        for ($i = 0; $i < 24; $i++) {
            $hour = $i;
            if ($i < 10) {
                $hour = "0" . $hour;
            }

            $result = DB::connection('mysql')
                ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(decode(ITEM, 'RENEW', ACTUAL_REVENUE, 0)) as dtgiahan,
                sum(ACTUAL_REVENUE) as tongdt
                from \"STATS_GENERAL\"
                WHERE STATS_DATE BETWEEN TO_DATE ('$dateCompare', 'dd/mm/yyyy') AND TO_DATE ('$dateCompare', 'dd/mm/yyyy')
                AND STATS_HOUR = '$hour' AND GROUP# = '3'
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by \"STATS_DATE\" asc"));

            if ($result && $result[0]) {
                $totalSub = $totalSub + $this->showPoint($result[0]->dtgiahan);
                $totaldt = $totaldt + $this->showPoint($result[0]->tongdt);
            }
            $hour = $hour . "h";
            $presub .= "[\"$hour\"," . $totalSub . "],";
            $predt .= "[\"$hour\"," . $totaldt . "],";
        }
        $dateCompare = date("d/m/Y");
        $currentHour = date('H');
        $currentDay = date('d');
        $totaldt = 0;
        $totalSub = 0;

        for ($i = 0; $i < 24; $i++) {
            if ($i > $currentHour) {
                break;
            }
            $hour = $i;
            if ($i < 10) {
                $hour = "0" . $hour;
            }
            $result = DB::connection('mysql')
                ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(decode(ITEM, 'RENEW', ACTUAL_REVENUE, 0)) as dtgiahan,
                sum(ACTUAL_REVENUE) as tongdt
                from \"STATS_GENERAL\"
                WHERE STATS_DATE BETWEEN TO_DATE ('$dateCompare', 'dd/mm/yyyy') AND TO_DATE ('$dateCompare', 'dd/mm/yyyy')
                AND STATS_HOUR = '$hour' AND GROUP# = '3'
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by \"STATS_DATE\" asc"));

            if ($result && $result[0]) {
                $totalSub = $totalSub + $this->showPoint($result[0]->dtgiahan);
                $totaldt = $totaldt + $this->showPoint($result[0]->tongdt);
            }
            $hour = $hour . "h";
            $datasub .= "[\"$hour\"," . $totalSub . "],";
            $datadt .= "[\"$hour\"," . $totaldt . "],";
        }

        $str .= "{\"name\": \"Total gia hạn Ngày $preDay\", \"data\": [" . substr($presub, 0, -1) . "]},";
        $str .= "{\"name\": \"Total Ngày $preDay\", \"data\": [" . substr($predt, 0, -1) . "]},";
        $str .= "{\"name\": \"Total gia hạn Ngày $currentDay\", \"data\": [" . substr($datasub, 0, -1) . "]},";
        $str .= "{\"name\": \"Total Ngày $currentDay\", \"data\": [" . substr($datadt, 0, -1) . "]}";
        $str = "[" . $str . "]";

        return $str;
    }

    public function chartDKHourly()
    {
        $str = "";
        $predk = "";
        $precancel = "";
        $datadk = "";
        $datacancel = "";
        $totaldk = 0;
        $totalCancel = 0;

        $dateCompare = date("d/m/Y", strtotime("-1 day"));
        $preDay = date('d', strtotime("-1 day"));

        for ($i = 0; $i < 24; $i++) {
            $hour = $i;
            if ($i < 10) {
                $hour = "0" . $hour;
            }
            $result = DB::connection('mysql')
                ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(decode(item, 'REGISTER', subs, 0)) as tongdk,
                sum(decode(item, 'UNREG', subs, 0)) as totalcanunreg
                from \"STATS_GENERAL\"
                WHERE STATS_DATE BETWEEN TO_DATE ('$dateCompare', 'dd/mm/yyyy') AND TO_DATE ('$dateCompare', 'dd/mm/yyyy')
                AND STATS_HOUR = '$hour' AND GROUP# = '3'
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by \"STATS_DATE\" asc"));

            if ($result && $result[0]) {
                $totaldk = $totaldk + $this->showPoint($result[0]->tongdk);
                $totalCancel = $totalCancel + $this->showPoint($result[0]->totalcanunreg);
            }
            $hour = $hour . "h";
            $predk .= "[\"$hour\"," . $totaldk . "],";
            $precancel .= "[\"$hour\"," . $totalCancel . "],";
        }
        $dateCompare = date("d/m/Y");
        $currentHour = date('H');
        $currentDay = date('d');
        $totaldk = 0;
        $totalCancel = 0;

        for ($i = 0; $i < 24; $i++) {
            if ($i > $currentHour) {
                break;
            }
            $hour = $i;
            if ($i < 10) {
                $hour = "0" . $hour;
            }
            $result = DB::connection('mysql')
                ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(decode(item, 'REGISTER', subs, 0)) as tongdk,
                sum(decode(item, 'UNREG', subs, 0)) as totalcanunreg
                from \"STATS_GENERAL\"
                WHERE STATS_DATE BETWEEN TO_DATE ('$dateCompare', 'dd/mm/yyyy') AND TO_DATE ('$dateCompare', 'dd/mm/yyyy')
                AND STATS_HOUR = '$hour' AND GROUP# = '3'
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by \"STATS_DATE\" asc"));
            if ($result && $result[0]) {
                $totaldk = $totaldk + $this->showPoint($result[0]->tongdk);
                $totalCancel = $totalCancel + $this->showPoint($result[0]->totalcanunreg);
            }
            $hour = $hour . "h";
            $datadk .= "[\"$hour\"," . $totaldk . "],";
            $datacancel .= "[\"$hour\"," . $totalCancel . "],";
        }

        $str .= "{\"name\": \"REGISTER Ngày $preDay\", \"data\": [" . substr($predk, 0, -1) . "]},";
        $str .= "{\"name\": \"CANCEL Ngày $preDay\", \"data\": [" . substr($precancel, 0, -1) . "]},";
        $str .= "{\"name\": \"REGISTER Ngày $currentDay\", \"data\": [" . substr($datadk, 0, -1) . "]},";
        $str .= "{\"name\": \"CANCEL Ngày $currentDay\", \"data\": [" . substr($datacancel, 0, -1) . "]}";
        $str = "[" . $str . "]";

        return $str;
    }

    public function chartSub($startDate, $endDate)
    {
        $str = "";
        $dataActive = "";
        $dataPending = "";
        $dataCancelUnreg = "";
        $dateFirst = date("Y-m-d", strtotime($startDate));
        $dateLast = date("Y-m-d", strtotime($endDate));

        $dateFirst = date("Y-m-d", strtotime($startDate));
        $dateLast = date("Y-m-d", strtotime($endDate));
        while ($dateFirst <= $dateLast) {
            $dateCompare = date("d/m/Y", strtotime($dateFirst));
            $result = DB::connection('mysql')
                ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(decode(item, '1', ACC_SUBS, 0)) as totalactive,
               sum(decode(item, '2', ACC_SUBS, 0)) as totalpending,
               sum(decode(item, '3', SUBS, 0)) as totalcanunreg
                from \"STATS_GENERAL\" 
                 WHERE group# = 1 AND STATS_DATE BETWEEN TO_DATE ('$dateCompare', 'dd/mm/yyyy') AND TO_DATE ('$dateCompare', 'dd/mm/yyyy')
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by \"STATS_DATE\" asc"));

            if ($result) {
                $dataActive .= "[\"$dateFirst\"," . $this->showPoint($result[0]->totalactive) . "],";
                $dataPending .= "[\"$dateFirst\"," . $this->showPoint($result[0]->totalpending) . "],";
                $dataCancelUnreg .= "[\"$dateFirst\"," . $this->showPoint($result[0]->totalcanunreg) . "],";
            } else {
                $dataActive .= "[\"$dateFirst\",0],";
                $dataPending .= "[\"$dateFirst\",0],";
                $dataCancelUnreg .= "[\"$dateFirst\",0],";
            }
            $dateFirst = date("Y-m-d", strtotime("+1 day", strtotime($dateFirst)));
        }
        $str .= "{\"name\": \"ACTIVE\", \"data\": [" . substr($dataActive, 0, -1) . "]},";
        $str .= "{\"name\": \"PENDING\", \"data\": [" . substr($dataPending, 0, -1) . "]},";
        $str .= "{\"name\": \"CANCEL/UNREG\", \"data\": [" . substr($dataCancelUnreg, 0, -1) . "]}";
        $str = "[" . $str . "]";

        return $str;
    }

    public function chartDK($startDate, $endDate)
    {
        $str = "";
        $data101 = "";
        $datasms = "";
        $dataussd = "";
        $total101 = 0;
        $totalsms = 0;
        $totalussd = 0;
        $dateFirst = date("Y-m-d", strtotime($startDate));
        $dateLast = date("Y-m-d", strtotime($endDate));

        while ($dateFirst <= $dateLast) {
            $dateCompare = date("d/m/Y", strtotime($dateFirst));
            $result = DB::connection('mysql')
                ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(case
             when CP = 'INVITE' then
              SUBS
               end) as dk101,
           sum(case
                 when CHANNEL = 'SMS' then
                  SUBS
               end) as dksms,
           sum(case
                 when CHANNEL = 'USSD' and CP is null then
                  SUBS
               end) as dkussd
                from \"STATS_GENERAL\" 
                WHERE GROUP# = '1' and item='1' AND STATS_DATE BETWEEN TO_DATE ('$dateCompare', 'dd/mm/yyyy') AND TO_DATE ('$dateCompare', 'dd/mm/yyyy')
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by \"STATS_DATE\" asc"));

            if ($result) {
                $data101 .= "[\"$dateFirst\"," . $this->showPoint($result[0]->dk101) . "],";
                $datasms .= "[\"$dateFirst\"," . $this->showPoint($result[0]->dksms) . "],";
                $dataussd .= "[\"$dateFirst\"," . $this->showPoint($result[0]->dkussd) . "],";

                $total101 += $this->showPoint($result[0]->dk101);
                $totalsms += $this->showPoint($result[0]->dksms);
                $totalussd += $this->showPoint($result[0]->dkussd);
            } else {
                $data101 .= "[\"$dateFirst\",0],";
                $datasms .= "[\"$dateFirst\",0],";
                $dataussd .= "[\"$dateFirst\",0],";
            }
            $dateFirst = date("Y-m-d", strtotime("+1 day", strtotime($dateFirst)));
        }
        //$str .= "{\"name\": \"DK 101\", \"data\": [".substr($data101, 0, -1)."]},";
        //$str .= "{\"name\": \"DK SMS\", \"data\": [".substr($datasms, 0, -1)."]},";
        //$str .= "{\"name\": \"DK USSD\", \"data\": [".substr($dataussd, 0, -1)."]}";
        $str .= "[[\"DK 101\", $total101],[\"DK SMS\", $totalsms],[\"DK USSD\", $totalussd]]";
        //$str = "[".$str."]";

        return $str;
    }

    public function chartHuyDK($startDate, $endDate)
    {
        $str = "";
        $dataht = "";
        $datasms = "";
        $dataussd = "";
        $dateFirst = date("Y-m-d", strtotime($startDate));
        $dateLast = date("Y-m-d", strtotime($endDate));

        while ($dateFirst <= $dateLast) {
            $dateCompare = date("d/m/Y", strtotime($dateFirst));
            $result = DB::connection('mysql')
                ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(case when CHANNEL = 'SMS' and ITEM = '3' and GROUP# = '1' then ACC_SUBS end) as huysms,
                sum(case when CHANNEL = 'USSD' and ITEM = '3' and GROUP# = '1' then ACC_SUBS end) as huyussd,
                sum(case when ITEM = '3' and GROUP# = '1' then ACC_SUBS end) as huyboiht
                from \"STATS_GENERAL\" 
                WHERE STATS_DATE BETWEEN TO_DATE ('$dateCompare', 'dd/mm/yyyy') AND TO_DATE ('$dateCompare', 'dd/mm/yyyy')
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by \"STATS_DATE\" asc"));

            if ($result) {
                $dataht .= "[\"$dateFirst\"," . $this->showPoint($result[0]->huyboiht) . "],";
                $datasms .= "[\"$dateFirst\"," . $this->showPoint($result[0]->huysms) . "],";
                $dataussd .= "[\"$dateFirst\"," . $this->showPoint($result[0]->huyussd) . "],";
            } else {
                $dataht .= "[\"$dateFirst\",0],";
                $datasms .= "[\"$dateFirst\",0],";
                $dataussd .= "[\"$dateFirst\",0],";
            }
            $dateFirst = date("Y-m-d", strtotime("+1 day", strtotime($dateFirst)));
        }
        $str .= "{\"name\": \"Huy bởi HT\", \"data\": [" . substr($dataht, 0, -1) . "]},";
        $str .= "{\"name\": \"Huy SMS\", \"data\": [" . substr($datasms, 0, -1) . "]},";
        $str .= "{\"name\": \"Huy USSD\", \"data\": [" . substr($dataussd, 0, -1) . "]}";
        $str = "[" . $str . "]";

        return $str;
    }

    public function chartDT($startDate, $endDate)
    {
        $str = "";
        $datadt = "";
        $dateFirst = date("Y-m-d", strtotime($startDate));
        $dateLast = date("Y-m-d", strtotime($endDate));

        while ($dateFirst <= $dateLast) {
            $dateCompare = date("d/m/Y", strtotime($dateFirst));
            $result = DB::connection('mysql')
                ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(ACTUAL_REVENUE) as tongdt
                from \"STATS_GENERAL\" 
                WHERE group# = '2' AND STATS_DATE BETWEEN TO_DATE ('$dateCompare', 'dd/mm/yyyy') AND TO_DATE ('$dateCompare', 'dd/mm/yyyy')
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by \"STATS_DATE\" asc"));

            if ($result) {
                $datadt .= "[\"$dateFirst\"," . $this->showPoint($result[0]->tongdt) . "],";
            } else {
                $datadt .= "[\"$dateFirst\",0],";
            }
            $dateFirst = date("Y-m-d", strtotime("+1 day", strtotime($dateFirst)));
        }
        $str .= "{\"name\": \"Doanh thu\", \"data\": [" . substr($datadt, 0, -1) . "]}";
        $str = "[" . $str . "]";

        return $str;
    }

    public function chartPieDT($startDate, $endDate)
    {
        $str = "";
        $datadtdk = 0;
        $datadtgiahan = 0;
        $datadtcauhoi = 0;
        $dateFirst = date("Y-m-d", strtotime($startDate));
        $dateLast = date("Y-m-d", strtotime($endDate));

        while ($dateFirst <= $dateLast) {
            $dateCompare = date("d/m/Y", strtotime($dateFirst));
            $result = DB::connection('mysql')
                ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(decode(ITEM, 'REGISTER', ACTUAL_REVENUE, 0)) as dtdk,
               sum(decode(ITEM, 'RENEW', ACTUAL_REVENUE, 0)) as dtgiahan,
               sum(decode(ITEM, 'ANSWER', ACTUAL_REVENUE, 0)) as dtcauhoi
                from \"STATS_GENERAL\"
                WHERE group# = 2 AND STATS_DATE BETWEEN TO_DATE ('$dateCompare', 'dd/mm/yyyy') AND TO_DATE ('$dateCompare', 'dd/mm/yyyy')
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by \"STATS_DATE\" asc"));

            if ($result) {
                $datadtdk += $this->showPoint($result[0]->dtdk);
                $datadtgiahan += $this->showPoint($result[0]->dtgiahan);
                $datadtcauhoi += $this->showPoint($result[0]->dtcauhoi);
            }
            $dateFirst = date("Y-m-d", strtotime("+1 day", strtotime($dateFirst)));
        }
        $str .= "[[\"Đăng Ký\", $datadtdk],[\"Gia Hạn\", $datadtgiahan],[\"Câu Hỏi\", $datadtcauhoi]]";

        return $str;
    }

    public function chartInvite(Request $request)
    {
        $startDate = $request->get('invite_start_date');
        $endDate = $request->get('invite_end_date');
        $results = DB::connection('mysql')
            ->select(DB::raw('
                    select TO_CHAR(STATS_DATE,\'dd/mm/yyyy\') as "STATS_DATE",
                    subs_invite, 
                    subs_resp_1, 
                    subs_resp_2,
                    subs_un_resp
                    from "Stats_Invite" 
                    WHERE STATS_DATE BETWEEN TO_DATE (\'$startDate\', \'dd/mm/yyyy\') AND TO_DATE (\'$endDate\', \'dd/mm/yyyy\')
                    group by TO_CHAR(STATS_DATE,\'dd/mm/yyyy\') order by "STATS_DATE" asc'));

        $str = "";
        $dataInvite = "";
        if ($startDate == $endDate) {

        } else {
            while (strtotime($startDate) <= strtotime($endDate)) {
                $date = date("dd/mm/yyyy", strtotime("+1 day", strtotime($startDate)));
                $result = $results->where('STATS_DATE', '=', $date);
                if ($result) {
                    $dataInvite .= "['$date'," . $this->showPoint($result["subs_invite"]) . "],";
                } else {
                    $dataInvite .= "['$date',0],";
                }
            }
        }
        $str .= "{name: 'Tổng Invite USSD', data: [" . substr($dataInvite, 0, -1) . "]}";
        $str = "[" . $str . "]";

        return [
            'data' => $str,
        ];
    }

    private function showPoint($value)
    {
        if (!$value) {
            return 0;
        }
        return $value;
    }
}
