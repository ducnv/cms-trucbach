<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\DailyNews;
use App\Models\His;
use App\Models\Message;
use App\Models\Packages;
use App\Models\PointHis;
use App\Models\Question;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use DB;
use Validator;
use Carbon\Carbon;
use App\Jobs\LogSystemEvent;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ContentController extends Controller
{
    use DispatchesJobs;

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'Quản trị nội dung'
            ],
        ];

        $this->views['breadcumbMain'] = "Quản trị nội dung";
    }

    /**
     * @Access (permission = "content: access")
     */
    public function manageContent()
    {
        $this->views['title'] = 'MT nội dung';

        return view('content.mtnoidung', $this->views);
    }

    /**
     * @Access (permission = "content: access")
     */
    public function manageMessages()
    {
        $this->views['title'] = 'Tin nhắn thuê bao';

        return view('content.messages', $this->views);
    }

    /**
     * @Access (permission = "content: access")
     */
    public function thongKeCauHoi()
    {
        $this->views['title'] = 'Thống kê câu hỏi';

        $data = DB::connection('mysql')
            ->select(DB::raw('select count(*) as total, q_level, timeout from question group by q_level,timeout order by q_level asc'));

        $this->views['data'] = $data;

        return view('content.thongkecauhoi', $this->views);
    }

    /**
     * @Access (permission = "content: access")
     */
    public function manageCategory()
    {
        $this->views['title'] = 'Quản lý chuyên mục';

        return view('content.category', $this->views);
    }

    /**
     * @Access (permission = "content: access")
     */
    public function manageQuestion()
    {
        $this->views['title'] = 'Quản lý câu hỏi';

        $data = DB::connection('mysql')
            ->select(DB::raw('select count(*) as total, q_level, timeout from question group by q_level,timeout order by q_level asc'));

        $this->views['level'] = $data;

        return view('content.question', $this->views);
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiGetContent(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $contents = DailyNews::select("*");

        if ($request->has('name') && $request->get('name') !== '' && $request->get('name') !== null) {
            $contents->where('job_title', 'LIKE', "%{$request->get('name')}%");
        }

        if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
            $contents->where('pkg_code', "{$request->get('pkg_code')}");
        }

        $contents = $contents->orderBy('created_time', 'DESC');

        return [
            'data' => $contents->paginate($per_page),
        ];
    }


    public function apiGetPackages(Request $request)
    {
        return [
            'data' => Packages::select('code as value', 'name as text', 'reg_fee as price')->get(),
        ];
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiGetCatAll(Request $request)
    {
        return [
            'data' => Category::select('id as value', 'name as text')->get(),
        ];
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiGetMessage(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $messages = Message::select("*");

        if ($request->has('name') && $request->get('name') !== '' && $request->get('name') !== null) {
            $messages->where('content', 'LIKE', "%{$request->get('name')}%");
        }

        $messages = $messages->orderBy('id', 'DESC');

        return [
            'data' => $messages->paginate($per_page),
        ];
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiGetCategory(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $category = Category::select("*");

        if ($request->has('name') && $request->get('name') !== '' && $request->get('name') !== null) {
            $category->where('name', 'LIKE', "%{$request->get('name')}%");
        }

        $category = $category->orderBy('id', 'DESC');

        return [
            'data' => $category->paginate($per_page),
        ];
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiGetQuestion(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $questions = Question::select("*");

        if ($request->has('name') && $request->get('name') !== '' && $request->get('name') !== null) {
            $questions->where('content', 'LIKE', "%{$request->get('name')}%");
        }

        if ($request->has('q_level') && $request->get('q_level') !== '' && $request->get('q_level') !== null) {
            $questions->where('q_level', $request->get('q_level'));
        }

        if ($request->has('cat_id') && $request->get('cat_id') !== '' && $request->get('cat_id') !== null) {
            $questions->where('cat_id', $request->get('cat_id'));
        }

        $questions = $questions->orderBy('id', 'DESC');

        return [
            'data' => $questions->paginate($per_page),
        ];
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiPostContent(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'content.required' => 'Bạn chưa nhập nội dung MT!',
            'content.max' => 'Nội dung MT vượt quá 450 ký tự !',
            'send_time.required' => 'Bạn chưa chọn thời gian bắt đầu gửi !'
        ];

        $okay = Validator::make($data, [
            'content' => 'required|max:437',
            'send_time' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        // ket noi db va lưu thog tin MT lại

        $dailynews = new DailyNews();
        $dailynews->content = "[KNTB] " . $this->convert_vi_to_en($data['content']);
        $dailynews->pkg_code = $data['pkg_code'];
        $dailynews->created_time = date('Y-m-d H:i:s', time());
        $dailynews->send_time = $data['send_time'];
        $dailynews->send_date = Carbon::createFromFormat('Y-m-d H:i:s', $data['send_time'])->format('Ymd');
        $dailynews->state = 0;
        $dailynews->send_hour = Carbon::createFromFormat('Y-m-d H:i:s', $data['send_time'])->format('YmdH');

        try {
            $dailynews->save();

            $sys_event = [
                'type' => 'create_news',
                'description' => auth()->user()->name . " đã tạo DailyNews " . $dailynews->id,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Tạo MT thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Tạo MT không thành công'
            ];
        }
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiPutContent(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'content.required' => 'Bạn chưa nhập nội dung MT!',
            'content.max' => 'Nội dung MT vượt quá 450 ký tự !',
            'send_time.required' => 'Bạn chưa chọn thời gian bắt đầu gửi !'
        ];

        $okay = Validator::make($data, [
            'content' => 'required|max:437',
            'send_time' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        // ket noi db va lưu thog tin MT lại

        $dailynews = DailyNews::where('id', $data['id'])->first();
        $dailynews->content = $this->convert_vi_to_en($data['content']);
        $dailynews->pkg_code = $data['pkg_code'];
        $dailynews->send_time = $data['send_time'];
        $dailynews->send_date = Carbon::createFromFormat('Y-m-d H:i:s', $data['send_time'])->format('Ymd');
        $dailynews->send_hour = Carbon::createFromFormat('Y-m-d H:i:s', $data['send_time'])->format('YmdH');

        try {

            $dailynews->save();

            $sys_event = [
                'type' => 'update_news',
                'description' => auth()->user()->name . " đã cập nhật DailyNews " . $dailynews->id,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Cập nhật MT thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Cập nhật MT không thành công'
            ];
        }
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiPostMessage(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'action.required' => 'Bạn chưa chọn loại tin nhắn!',
            'code.required' => 'Bạn chưa nhập code mã lỗi!',
            'type.required' => 'Bạn chưa chọn kiểu gửi!',
            'pkg_code.required' => 'Bạn chưa chọn gói cước!',
            'channel.required' => 'Bạn chưa chọn kênh thao tác!',
            'content.required' => 'Bạn chưa nhập nội dung MT!',
        ];

        $okay = Validator::make($data, [
            'action' => 'required',
            'code' => 'required',
            'type' => 'required',
            'pkg_code' => 'required',
            'channel' => 'required',
            'content' => 'required',
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        // ket noi db va lưu thog tin MT lại
        $messages = new Message();
        $messages->action = $data['action'];
        $messages->code = $data['code'];
        $messages->type = $data['type'];
        $messages->pkg_code = $data['pkg_code'];
        $messages->order_sent = 0;
        $messages->channel = $data['channel'];
        $messages->delay = $data['delay'];
        $messages->shortcode = $data['shortcode'];
        $messages->content = $data['content'];
        $messages->send_channel = $data['send_channel'];

        try {
            $messages->save();

            $sys_event = [
                'type' => 'create_message',
                'description' => auth()->user()->name . " đã tạo message " . $messages->id,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Tạo tin nhắn thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Tạo tin nhắn không thành công'
            ];
        }

    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiPutMessage(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'action.required' => 'Bạn chưa chọn loại tin nhắn!',
            'code.required' => 'Bạn chưa nhập code mã lỗi!',
            'type.required' => 'Bạn chưa chọn kiểu gửi!',
            'pkg_code.required' => 'Bạn chưa chọn gói cước!',
            'channel.required' => 'Bạn chưa chọn kênh thao tác!',
            'content.required' => 'Bạn chưa nhập nội dung MT!',
        ];

        $okay = Validator::make($data, [
            'action' => 'required',
            'code' => 'required',
            'type' => 'required',
            'pkg_code' => 'required',
            'channel' => 'required',
            'content' => 'required',
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }


        $messages = new Message();
        $messages->action = $data['action'];
        $messages->code = $data['code'];
        $messages->type = $data['type'];
        $messages->pkg_code = $data['pkg_code'];
        $messages->order_sent = 0;
        $messages->channel = $data['channel'];
        $messages->delay = $data['delay'];
        $messages->shortcode = $data['shortcode'];
        $messages->content = $data['content'];
        $messages->send_channel = $data['send_channel'];

        try {
            $messages->save();

            $sys_event = [
                'type' => 'update_message',
                'description' => auth()->user()->name . " đã cập nhật message " . $messages->id,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Cập nhật tin nhắn thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Cập nhật tin nhắn không thành công'
            ];
        }

    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiPostCategory(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'name.required' => 'Bạn chưa nhập tên chuyên mục!',
            'descriptions.required' => 'Bạn chưa nhập mô tả chuyên mục!'
        ];

        $okay = Validator::make($data, [
            'name' => 'required',
            'descriptions' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        // ket noi db va lưu thog tin MT lại
        $category = new Category();
        $category->name = $data['name'];
        $category->descriptions = $data['descriptions'];


        try {

            $category->save();

            $sys_event = [
                'type' => 'create_category',
                'description' => auth()->user()->name . " đã tạo chuyên mục " . $category->id,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Tạo chuyên mục thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Tạo chuyên mục không thành công'
            ];
        }
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiPutCategory(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'name.required' => 'Bạn chưa nhập tên chuyên mục!',
            'descriptions.required' => 'Bạn chưa nhập mô tả chuyên mục!'
        ];

        $okay = Validator::make($data, [
            'name' => 'required',
            'descriptions' => 'required'
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        $category = Category::where('id', $data['id'])->first();
        $category->name = $data['name'];
        $category->descriptions = $data['descriptions'];

        // ket noi db va lưu thog tin MT lại

        try {

            $category->save();

            $sys_event = [
                'type' => 'update_category',
                'description' => auth()->user()->name . " đã cập nhật chuyên mục " . $category->id,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Cập nhật chuyên mục thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Cập nhật chuyên mục không thành công'
            ];
        }
    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiPostQuestion(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'q_level.required' => 'Bạn chưa chọn cấp độ khó cho câu hỏi!',
            'cat_id.required' => 'Bạn chưa chọn chuyên mục cho câu hỏi!',
            'content.required' => 'Bạn chưa nhập nội dung cho câu hỏi!',
            'label1.required' => 'Bạn chưa nhập lựa chọn 1 cho câu hỏi!',
            'label2.required' => 'Bạn chưa nhập lựa chọn 2 cho câu hỏi!',
            'correct_label.required' => 'Bạn chưa nhập đáp án đúng cho câu hỏi!',
        ];

        $okay = Validator::make($data, [
            'q_level' => 'required',
            'cat_id' => 'required',
            'content' => 'required',
            'label1' => 'required',
            'label2' => 'required',
            'correct_label' => 'required',
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        // ket noi db va lưu thog tin cau hoi  lại
        $question = new Question();
        $question->cat_id = $data['cat_id'];
        $question->content = $data['content'] . "\n1." . $data['label1'] . "\n2." . $data['label2'] . ($data['label3'] != '' ? "\n3." . $data['label3'] : "") . ($data['label4'] != '' ? "\n4." . $data['label4'] : "");
        $question->correct_label = $data['correct_label'];
        $question->q_level = $data['q_level'];
        $question->fee = '500';
        $question->timeout = $data['timeout'];
        $question->created_time = date('Y-m-d H:i:s', time());
        $question->modified_time = date('Y-m-d H:i:s', time());
        $question->header_content = "Moi QK bam 1,2" . ($data['label3'] != '' ? ",3" : "") . ($data['label4'] != '' ? ",4" : "") . " tuong ung voi DA";

        try {

            $question->save();

            $sys_event = [
                'type' => 'create_question',
                'description' => auth()->user()->name . " đã tạo câu hỏi" . $question->id,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Tạo câu hỏi thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Tạo câu hỏi không thành công'
            ];
        }

    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiPutQuestion(Request $request)
    {
        $data = $request->all();

        //validate data
        $messages = [
            'q_level.required' => 'Bạn chưa chọn cấp độ khó cho câu hỏi!',
            'cat_id.required' => 'Bạn chưa chọn chuyên mục cho câu hỏi!',
            'content.required' => 'Bạn chưa nhập nội dung cho câu hỏi!',
            'correct_label.required' => 'Bạn chưa nhập đáp án đúng cho câu hỏi!',
        ];

        $okay = Validator::make($data, [
            'q_level' => 'required',
            'cat_id' => 'required',
            'content' => 'required',
            'correct_label' => 'required',
        ], $messages);

        if ($okay->fails()) {
            return [
                'status' => 'fail',
                'message' => $okay->errors()->first()
            ];
        }

        // ket noi db va lưu thog tin cau hoi lại

        $question = Question::where('id', $data['id'])->first();
        $question->cat_id = $data['cat_id'];
        $question->content = $data['content'];
        $question->correct_label = $data['correct_label'];
        $question->q_level = $data['q_level'];
        $question->timeout = $data['timeout'];
        $question->modified_time = date('Y-m-d H:i:s', time());

        try {

            $question->save();

            $sys_event = [
                'type' => 'update_question',
                'description' => auth()->user()->name . " đã cập nhật câu hỏi" . $question->id,
                'user_id' => auth()->user()->id
            ];

            $this->dispatch((new LogSystemEvent($sys_event))->delay(5));

            return [
                'status' => 'success',
                'message' => 'Cập nhật câu hỏi thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Cập nhật câu hỏi không thành công'
            ];
        }

    }

    /**
     * @Access (permission = "content: access")
     */
    public function apiPutTimeoutQuestion(Request $request)
    {
        $data = $request->all();

        try {
            DB::update('update question set timeout = ' . $data['timeout'] . ' where q_level = ' . $data['q_level']);

            return [
                'status' => 'success',
                'message' => 'Cập nhật thời gian trả lời câu hỏi thành công'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'fail',
                'message' => 'Cập nhật thời gian trả lời câu hỏi không thành công'
            ];
        }

    }

    public function convert_vi_to_en($str)
    {
        $str = preg_replace(" / (à | á | ạ | ả | ã | â | ầ | ấ | ậ | ẩ | ẫ | ă | ằ | ắ | ặ | ẳ | ẵ) / ", 'a', $str);
        $str = preg_replace(" / (è | é | ẹ | ẻ | ẽ | ê | ề | ế | ệ | ể | ễ) / ", 'e', $str);
        $str = preg_replace(" / (ì | í | ị | ỉ | ĩ) / ", 'i', $str);
        $str = preg_replace(" / (ò | ó | ọ | ỏ | õ | ô | ồ | ố | ộ | ổ | ỗ | ơ | ờ | ớ | ợ | ở | ỡ) / ", 'o', $str);
        $str = preg_replace(" / (ù | ú | ụ | ủ | ũ | ư | ừ | ứ | ự | ử | ữ) / ", 'u', $str);
        $str = preg_replace(" / (ỳ | ý | ỵ | ỷ | ỹ) / ", 'y', $str);
        $str = preg_replace(" / (đ) / ", 'd', $str);
        $str = preg_replace(" / (À | Á | Ạ | Ả | Ã | Â | Ầ | Ấ | Ậ | Ẩ | Ẫ | Ă | Ằ | Ắ | Ặ | Ẳ | Ẵ) / ", 'A', $str);
        $str = preg_replace(" / (È | É | Ẹ | Ẻ | Ẽ | Ê | Ề | Ế | Ệ | Ể | Ễ) / ", 'E', $str);
        $str = preg_replace(" / (Ì | Í | Ị | Ỉ | Ĩ) / ", 'I', $str);
        $str = preg_replace(" / (Ò | Ó | Ọ | Ỏ | Õ | Ô | Ồ | Ố | Ộ | Ổ | Ỗ | Ơ | Ờ | Ớ | Ợ | Ở | Ỡ) / ", 'O', $str);
        $str = preg_replace(" / (Ù | Ú | Ụ | Ủ | Ũ | Ư | Ừ | Ứ | Ự | Ử | Ữ) / ", 'U', $str);
        $str = preg_replace(" / (Ỳ | Ý | Ỵ | Ỷ | Ỹ) / ", 'Y', $str);
        $str = preg_replace(" / (Đ) / ", 'D', $str);
        //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
        return $str;
    }
}
