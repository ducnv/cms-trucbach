<?php

namespace App\Http\Controllers;

use App\Models\PointHis;
use App\Models\StatsPoint;
use App\Models\Subscriber;
use App\Models\Winner;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Fill;

class ManageBonusController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'Quản trị sản lượng'
            ],
        ];

        $this->views['breadcumbMain'] = "Quản trị sản lượng";
    }

    /**
     * @Access (permission = "trungthuong: access")
     */
    public function manageSubBonus()
    {
        $this->views['title'] = 'Thuê bao trúng thưởng';

        return view('bonus.managesubbonus', $this->views);
    }

    /**
     * @Access (permission = "trungthuong: access")
     */
    public function manageSubBonusCurrent()
    {
        $this->views['title'] = 'Điểm số hiện tại';

        return view('bonus.managesubbonuscurrent', $this->views);
    }

    /**
     * @Access (permission = "trungthuong: access")
     */
    public function manageReportPoint()
    {
        $this->views['title'] = 'Báo cáo điểm số';

        return view('bonus.reportpoint', $this->views);
    }

    /**
     * @Access (permission = "trungthuong: access")
     */
    public function managePointFree()
    {
        $this->views['title'] = 'Báo cáo điểm số của thuê bao đăng ký miễn phí hôm trước';

        return view('bonus.reportpointfree', $this->views);
    }

    public function searchPoint()
    {

        $this->views['title'] = 'Tra cứu điểm số';

        return view('bonus.searchpoint', $this->views);
    }

    public function manageSubBonusDay()
    {
        $this->views['title'] = 'Thuê bao trúng thưởng ngày';

        return view('bonus.day', $this->views);
    }

    public function manageSubBonusMonth()
    {
        $this->views['title'] = 'Thuê bao trúng thưởng tháng';

        return view('bonus.month', $this->views);
    }

    public function manageNhapHS()
    {
        $this->views['title'] = 'Thuê bao trúng thưởng nhập hồ sơ';

        return view('bonus.nhapHS', $this->views);
    }

    public function managePending()
    {
        $this->views['title'] = 'Thuê bao trúng thưởng tập pending';

        return view('bonus.pending', $this->views);
    }

    public function manage3Day()
    {
        $this->views['title'] = 'Thuê bao trúng thưởng gia hạn 3 ngày';

        return view('bonus.3Day', $this->views);
    }

    public function moneyNow()
    {
        $this->views['title'] = 'Ket noi nhanh tay- Tien ve tui ngay';

        return view('bonus.moneynow', $this->views);
    }

    public function apiManageNhapHS(Request $request)
    {
        $trackDate = implode(explode('-', Carbon::parse($request->get('track_date'))->format('Y-m-d')));

        // Danh sách top 50 thuê bao dẫn đầu điểm số
        $data = DB::connection('mysql')
            ->select(DB::raw("select 
            s.msisdn ,s.pkg_code, reg_time, unreg_time, 
            case when service_state =1 then 'active' else 'unreg' end tinh_trang,win_date, 
            case when status =1 then 'đã trao thưởng' else 'chưa trao thưởng' end status,
            p.awarded_time  
        from subscriber s 
        join promo_winner p on s.msisdn = p.msisdn and s.pkg_code = p.pkg_code
        where p.win_date =$trackDate and p.promo_code ='PROMO_01'"));


        return [
            'data' => $data,//$result->paginate($per_page),
        ];
    }

    public function apiManagePending(Request $request)
    {
        $start_date = implode(explode('-', Carbon::parse($request->get('start_date'))->format('Y-m-d')));
        $end_date = implode(explode('-', Carbon::parse($request->get('end_date'))->format('Y-m-d')));

        // Danh sách top 50 thuê bao dẫn đầu điểm số
        $data = DB::connection('mysql')
            ->select(DB::raw("select 
            s.msisdn ,s.pkg_code, reg_time, unreg_time, 
            case when service_state =1 then 'active' else 'unreg' end tinh_trang,win_date, 
            sent_time ngay_bao_trung_thuong,
            confirm confirm_ok,
            case when status =1 then 'đã trao thưởng' else 'chưa trao thưởng' end status,
            p.awarded_time 
        from subscriber s 
        join promo_winner p on s.msisdn = p.msisdn and s.pkg_code = p.pkg_code
        where p.win_date between $start_date and $end_date and p.promo_code like 'PROMO_PENDING%'"));


        return [
            'data' => $data,//$result->paginate($per_page),
        ];
    }

    public function apiManage3Day(Request $request)
    {
        $trackDate = implode(explode('-', Carbon::parse($request->get('track_date'))->format('Y-m-d')));

        // Danh sách top 50 thuê bao dẫn đầu điểm số
        $data = DB::connection('mysql')
            ->select(DB::raw("select 
    s.msisdn ,s.pkg_code, reg_time, unreg_time, 
    case when service_state =1 then 'active' else 'unreg' end tinh_trang,win_date, 
    case when status =1 then 'đã trao thưởng' else 'chưa trao thưởng' end status,
    p.awarded_time  
from subscriber s 
join promo_winner p on s.msisdn = p.msisdn and s.pkg_code = p.pkg_code
where p.win_date =$trackDate and p.promo_code ='PROMO_03'"));


        return [
            'data' => $data,//$result->paginate($per_page),
        ];
    }

    public function apiGetBonusMonth(Request $request)
    {
        $start_date = $request->get('start_date');

        $result = DB::connection('mysql')
            ->select(DB::raw("select * from (select MSISDN, pkg_code,total, RANK() OVER (PARTITION By msisdn ORDER BY total desc) as rank from (
select msisdn,pkg_code , sum(point) as total from point_his where AVAIABLE_date like '$start_date%' group by msisdn,pkg_code )) where rank =1 order by total desc 
"));

        foreach ($result as $item) {
            //check xem ngay n+1 co renew ko
            $renew = PointHis::where('msisdn', $item->msisdn)->where('action', 'RENEW')->where('avaiable_date', 'LIKE', "{$start_date}%")->get();

            $item->renew = count($renew);

            $isdn = Subscriber::where('msisdn', $item->msisdn)->first();
            $item->sub = $isdn;
        }

        return [
            'data' => $result
        ];
    }

    public function apiGetBonusDay(Request $request)
    {
        $start_date = $request->get('start_date');

        $result = DB::connection('mysql')
            ->select(DB::raw("select * from (select MSISDN, pkg_code,total, RANK() OVER (PARTITION By msisdn ORDER BY total desc) as rank from (
select msisdn,pkg_code , sum(point) as total from point_his where avaiable_date = '$start_date' group by msisdn,pkg_code  HAVING (sum(point) > 10) 
)) where rank =1 order by total desc"));

        //check xem ngay n + 1 thue bao con hoat dong ko.
        $dateTomorrow = date('Ymd', strtotime("+1 day", strtotime($start_date)));

        foreach ($result as $item) {
            //check xem ngay n+1 co renew ko
            $renew = PointHis::where('msisdn', $item->msisdn)->where('action', 'RENEW')->where('avaiable_date', $dateTomorrow)->first();
            if ($renew) {
                $item->renew = 1;
            } else {
                $item->renew = 0;
            }
            $isdn = Subscriber::where('msisdn', $item->msisdn)->first();
            $item->sub = $isdn;
        }

        return [
            'data' => $result
        ];
    }

    public function apiGetSearchPoint(Request $request)
    {

        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $start_date = $request->get('start_date');

        $end_date = $request->get('end_date');

        $point = DB::table('point_his')
            ->where('avaiable_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
            ->where('avaiable_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'));

        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {
            $point->where('point_his.msisdn', $request->get('isdn'));
        }

        if ($request->has('pkg_code') && $request->get('pkg_code') !== '' && $request->get('pkg_code') !== null) {
            $point->where('point_his.pkg_code', $request->get('pkg_code'));
        }

        $point = $point->orderBy('avaiable_time', 'DESC')->get();

        foreach ($point as $item) {
            $isdn = Subscriber::where('msisdn', $item->msisdn)->first();
            $item->sub = $isdn;
        }

        return [
            'data' => $point,
            'total' => $point->sum('point')
        ];
    }

    public function apiGetSubBonus(Request $request)
    {

        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $winners = Winner::select("*")->where('point', '<>', -1);

        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {
            $winners->where('msisdn', $request->get('isdn'));
        }

        if ($request->has('prize_type') && $request->get('prize_type') !== '' && $request->get('prize_type') !== null) {
            $winners->where('prize_type', $request->get('prize_type'));
        }

        $winners->whereRaw("WIN_DATE BETWEEN TO_DATE ('$start_date', 'dd/mm/yyyy') AND TO_DATE ('$end_date', 'dd/mm/yyyy')");

        $winners = $winners->orderBy('ranking', 'ASC');

        return [
            'data' => $winners->paginate($per_page),
        ];
    }

    public function apiGetReportPoint(Request $request)
    {
        $start_date = $request->get('start_date');

        $end_date = $request->get('end_date');


        //so luong 10 diem
        $p_10 = DB::table('point_his')->select(DB::raw("count(msisdn) as count, avaiable_date"))
            ->where('avaiable_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
            ->where('avaiable_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'))
            ->where('point', 10)
            ->groupBy('avaiable_date')->orderBy('avaiable_date', 'desc')
            ->get()->mapWithKeys(function ($item) {
                return [$item->avaiable_date => $item->count];
            })->toArray();

        $p_20_70 = DB::table('point_his')->select(DB::raw("count(msisdn) as count, avaiable_date"))
            ->where('avaiable_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
            ->where('avaiable_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'))
            ->where('point', '>=', 20)->where('point', '<=', 70)
            ->groupBy('avaiable_date')->orderBy('avaiable_date', 'desc')
            ->get()->mapWithKeys(function ($item) {
                return [$item->avaiable_date => $item->count];
            })->toArray();

        $p_80 = DB::table('point_his')->select(DB::raw("count(msisdn) as count, avaiable_date"))
            ->where('avaiable_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
            ->where('avaiable_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'))
            ->where('point', 80)
            ->groupBy('avaiable_date')->orderBy('avaiable_date', 'desc')
            ->get()->mapWithKeys(function ($item) {
                return [$item->avaiable_date => $item->count];
            })->toArray();

        $p_90 = DB::table('point_his')->select(DB::raw("count(msisdn) as count, avaiable_date"))
            ->where('avaiable_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
            ->where('avaiable_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'))
            ->where('point', 90)
            ->groupBy('avaiable_date')->orderBy('avaiable_date', 'desc')
            ->get()->mapWithKeys(function ($item) {
                return [$item->avaiable_date => $item->count];
            })->toArray();

        $p_100 = DB::table('point_his')->select(DB::raw("count(msisdn) as count, avaiable_date"))
            ->where('avaiable_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
            ->where('avaiable_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'))
            ->where('point', 100)
            ->groupBy('avaiable_date')->orderBy('avaiable_date', 'desc')
            ->get()->mapWithKeys(function ($item) {
                return [$item->avaiable_date => $item->count];
            })->toArray();

        $p_110_200 = DB::table('point_his')->select(DB::raw("count(msisdn) as count, avaiable_date"))
            ->where('avaiable_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
            ->where('avaiable_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'))
            ->where('point', '>=', 110)->where('point', '<=', 200)
            ->groupBy('avaiable_date')->orderBy('avaiable_date', 'desc')
            ->get()->mapWithKeys(function ($item) {
                return [$item->avaiable_date => $item->count];
            })->toArray();

        $p_200 = DB::table('point_his')->select(DB::raw("count(msisdn) as count, avaiable_date"))
            ->where('avaiable_time', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'))
            ->where('avaiable_time', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'))
            ->where('point', '>=', 200)
            ->groupBy('avaiable_date')->orderBy('avaiable_date', 'desc')
            ->get()->mapWithKeys(function ($item) {
                return [$item->avaiable_date => $item->count];
            })->toArray();

        $date1 = date_create($start_date);
        $date2 = date_create($end_date);
        $diff = date_diff($date1, $date2);
        $days = $diff->format("%a");

        $arrTime = [date('Ymd', strtotime($start_date))];
        for ($i = 1; $i < $days; $i++) {
            array_push($arrTime, date('Ymd', strtotime("+{$i} day", strtotime($start_date))));
        }

        return [
            'data' => [
                'p_10' => $p_10,
                'p_20_70' => $p_20_70,
                'p_80' => $p_80,
                'p_90' => $p_90,
                'p_100' => $p_100,
                'p_110_200' => $p_110_200,
                'p_200' => $p_200,
                'time' => $arrTime
            ]
        ];
    }

    public function apiGetSubBonusCurrent(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $start_date = $request->get('start_date');

        $point = DB::table('point_his')->select(DB::raw('sum(point) as sum, point_his.msisdn'))
            ->where('point_his.avaiable_date', $start_date)->groupBy("point_his.msisdn")->orderBy("sum", 'DESC')
            ->join('subscriber', function ($leftJoin) {
                $leftJoin->on('subscriber.msisdn', '=', 'point_his.msisdn');
            });

        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {
            $point->where('point_his.msisdn', $request->get('isdn'));
        }

        $point = $point->take(100)->get();

        foreach ($point as $item) {
            $isdn = Subscriber::where('msisdn', $item->msisdn)->first();
            $item->sub = $isdn;
        }

        return [
            'data' => $point
        ];
    }

    public function apiGetSubBonusFree(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

        $start_date = $request->get('start_date');

        $date_yesterday = date('Ymd', strtotime('-1 day', strtotime($start_date)));

        $point = DB::table('point_his')->select(DB::raw('sum(point) as sum, point_his.msisdn'))
            ->where('point_his.avaiable_date', $start_date)
            ->where('point_his.created_date', $date_yesterday)
            ->groupBy("point_his.msisdn")->orderBy("sum", 'DESC')
            ->join('subscriber', function ($leftJoin) {
                $leftJoin->on('subscriber.msisdn', '=', 'point_his.msisdn');
            });

        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {
            $point->where('point_his.msisdn', $request->get('isdn'));
        }

        $point = $point->take(100)->get();

        foreach ($point as $item) {
            $isdn = Subscriber::where('msisdn', $item->msisdn)->first();
            $item->sub = $isdn;
        }

        return [
            'data' => $point
        ];
    }

    public function apiMoneyNow(Request $request)
    {

        $trackDate = implode(explode('-', Carbon::parse($request->get('track_date'))->format('Ymd')));

        // Danh sách top 50 thuê bao dẫn đầu điểm số
        $data = DB::connection('mysql')
            ->select(DB::raw("SELECT msisdn, case when service_state = 3 then 'HUY' else 'ACTIVE' end tinh_trang, reg_time from subscriber  where pkg_code ='VIECLAMCAU' and to_char(reg_time,'YYYYMMDD') between (select start_date from promotion where code ='PROMO_KETNOI' ) and (select  end_date from promotion where code ='PROMO_KETNOI') order by reg_time"));

        return [
            'data' => $data,//$result->paginate($per_page),
        ];

    }

    public function apiExportSubBonus(Request $request)
    {

        $winners = Winner::select("*");

        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {
            $winners->where('msisdn', $request->get('isdn'));
        }

        $winners->whereRaw("WIN_DATE BETWEEN TO_DATE ('$start_date', 'dd/mm/yyyy') AND TO_DATE ('$end_date', 'dd/mm/yyyy')");

        $winners = $winners->orderBy('ranking', 'ASC')->get();

        $dir = 'export' . '/' . date('Y/m/d') . '/';
        $file_name = sprintf("%s-%s-%s.%s", 'file', date('YmdHis'), str_random(5), 'xlsx');
        $link_show = '/storage/' . $dir . $file_name;
        $link = storage_path("app/public/" . $dir . $file_name);

        if (!is_writable(dirname($link)) || !is_executable(dirname($link))) {
            mkdir(dirname($link), 0755, true);
        }

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Dữ liệu thống kê');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'Ngày');
        $excel->getActiveSheet()->setCellValue('C1', 'TB trúng thưởng');
        $excel->getActiveSheet()->setCellValue('D1', 'Trạng thái hiện tại');
        $excel->getActiveSheet()->setCellValue('E1', 'Ngày đăng ký');
        $excel->getActiveSheet()->setCellValue('F1', 'Ngày hủy');
        $excel->getActiveSheet()->setCellValue('G1', 'Nạp tiền');
        $excel->getActiveSheet()->setCellValue('H1', 'Mệnh giá nạp');


        $i = 2;
        foreach ($winners as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $row->ranking);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->win_date);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->msisdn);
            $excel->getActiveSheet()->setCellValue('D' . $i, $this->getState(intval($row->service_state)));
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->reg_time);
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->unreg_time);
            $excel->getActiveSheet()->setCellValue('G' . $i, $this->showTopup(intval($row->topup)));
            $excel->getActiveSheet()->setCellValue('H' . $i, $row->face_value);
            $i++;
        }

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function apiExportSubBonusCurrent(Request $request)
    {

        $start_date = $request->get('start_date');

        $point = DB::table('point_his')->select(DB::raw('sum(point) as sum, point_his.msisdn'))
            ->where('point_his.CREATED_DATE', $start_date)->groupBy("point_his.msisdn")->orderBy("sum", 'DESC')
            ->join('subscriber', function ($leftJoin) {
                $leftJoin->on('subscriber.msisdn', '=', 'point_his.msisdn');
            });

        if ($request->has('isdn') && $request->get('isdn') !== '' && $request->get('isdn') !== null) {
            $point->where('point_his.msisdn', $request->get('isdn'));
        }

        $point = $point->take(100)->get();

        foreach ($point as $item) {
            $isdn = Subscriber::where('msisdn', $item->msisdn)->first();
            $item->sub = $isdn;
        }

        $dir = 'export' . '/' . date('Y/m/d') . '/';
        $file_name = sprintf("%s-%s-%s.%s", 'file', date('YmdHis'), str_random(5), 'xlsx');
        $link_show = '/storage/' . $dir . $file_name;
        $link = storage_path("app/public/" . $dir . $file_name);

        if (!is_writable(dirname($link)) || !is_executable(dirname($link))) {
            mkdir(dirname($link), 0755, true);
        }

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Dữ liệu thống kê');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'Ngày');
        $excel->getActiveSheet()->setCellValue('C1', 'TB trúng thưởng');
        $excel->getActiveSheet()->setCellValue('D1', 'Điểm số trúng thưởng');
        $excel->getActiveSheet()->setCellValue('E1', 'Trạng thái hiện tại');
        $excel->getActiveSheet()->setCellValue('F1', 'Ngày đăng ký');
        $excel->getActiveSheet()->setCellValue('G1', 'Ngày hủy');


        $i = 2;
        foreach ($point as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $start_date);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->msisdn);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->sum);
            $excel->getActiveSheet()->setCellValue('E' . $i, $this->getState(intval($row->sub->service_state)));
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->sub->reg_time);
            $excel->getActiveSheet()->setCellValue('G' . $i, $row->sub->unreg_time);

            $i++;
        }

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function getState($value)
    {
        if ($value === 1) {
            return "Active";
        } elseif ($value === 3) {
            return 'Unregister';
        }
        return "Pending";
    }

    public function showTopup($value)
    {
        if ($value === 1) {
            return "Đã nạp tiền";
        }
        return 'Chưa nạp tiền';

    }

}
