<?php

namespace App\Http\Controllers;

use App\Models\DoiSoat;
use App\Models\StatsGeneral;
use App\Models\StatsInvite;
use App\Models\StatsPending;
use App\Models\StatsQuestion;
use App\Models\StatsUnreg;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Fill;

class QuantityController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->views['breadcumb'] = [
            [
                'url' => '#',
                'label' => 'Quản trị sản lượng'
            ],
        ];

        $this->views['breadcumbMain'] = "Quản trị sản lượng";
    }

    public function thongKeDichVu()
    {
        $this->views['title'] = 'Thống kê dịch vụ';
//dd($this->views);
        return view('quantity.thongkedichvu', $this->views);
    }

    public function thongKeCharge()
    {
        $this->views['title'] = 'Thống kê Charge';
//dd($this->views);
        return view('quantity.thongkecharge', $this->views);
    }

    public function theoDoiMedia()
    {
        $this->views['title'] = 'Theo dõi sản lượng truyền thông';
//dd($this->views);
        return view('quantity.theodoimedia', $this->views);
    }

    public function inviteUssd()
    {
        $this->views['title'] = 'Báo cáo invite USSD';

        return view('quantity.inviteussd', $this->views);
    }

    public function cauHoiUssd()
    {
        $this->views['title'] = 'Báo cáo câu hỏi USSD';

        return view('quantity.cauhoiussd', $this->views);
    }

    public function thueBaoHuy()
    {
        $this->views['title'] = 'Báo cáo thuê bao hủy';

        return view('quantity.thuebaohuy', $this->views);
    }

    public function thueBaoPending()
    {
        $this->views['title'] = 'Báo cáo thuê bao pending';

        return view('quantity.thuebaopending', $this->views);
    }

    public function doiSoatSanLuong()
    {
        $this->views['title'] = 'Đối soát sản lượng';

        return view('quantity.doisoatsanluong', $this->views);
    }

    public function apiExportStatistic(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $result = DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(case
             when cp = 'INVITE' and item = '1' then
              SUBS
           end) as dk101,
       sum(case
             when CHANNEL = 'SMS' and item = '1' then
              SUBS
           end) as dksms,
       sum(case
             when CHANNEL = 'USSD' and CP is null and item = '1' then
              SUBS
           end) as dkussd,
       sum(case
             when item = '1' then
              SUBS
           end) as tongdk,
       sum(decode(item, '1', ACC_SUBS, '2', ACC_SUBS, 0)) as tongtb,
       sum(case
             when CHANNEL = 'SMS' and ITEM = '3' then
              SUBS
           end) as huysms,
       sum(case
             when CHANNEL = 'USSD' and ITEM = '3' then
              SUBS
           end) as huyussd,
       sum(case
             when CHANNEL not in ('SMS', 'USSD') and ITEM = '3' then
              SUBS
           end) as huyboiht,
       sum(case
             when ITEM = '3' then
              SUBS
           end) as tonghuy,
       sum(decode(ITEM, 'REGISTER', ACTUAL_REVENUE, 0)) as dtdk,
       sum(decode(ITEM, 'RENEW', ACTUAL_REVENUE, 0)) as dtgiahan,
       sum(decode(ITEM, 'ANSWER', ACTUAL_REVENUE, 0)) as dtcauhoi,
       sum(ACTUAL_REVENUE) as tongdt
                from \"STATS_GENERAL\" 
                WHERE group# in ('1', '2') AND STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        $dir = 'export' . '/' . date('Y/m/d') . '/';
        $file_name = sprintf("%s-%s-%s.%s", 'file', date('YmdHis'), str_random(5), 'xlsx');
        $link_show = '/storage/' . $dir . $file_name;
        $link = storage_path("app/public/" . $dir . $file_name);

        if (!is_writable(dirname($link)) || !is_executable(dirname($link))) {
            mkdir(dirname($link), 0755, true);
        }

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Thống kê dịch vụ');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'Ngày');
        $excel->getActiveSheet()->setCellValue('C1', 'DK 101');
        $excel->getActiveSheet()->setCellValue('D1', 'DK SMS');
        $excel->getActiveSheet()->setCellValue('E1', 'DK USSD');
        $excel->getActiveSheet()->setCellValue('F1', 'Tổng DK');
        $excel->getActiveSheet()->setCellValue('G1', 'Tổng TB');
        $excel->getActiveSheet()->setCellValue('H1', 'Hủy SMS');
        $excel->getActiveSheet()->setCellValue('I1', 'Hủy USSD');
        $excel->getActiveSheet()->setCellValue('J1', 'Hủy bởi HT');
        $excel->getActiveSheet()->setCellValue('K1', 'Tổng hủy');
        $excel->getActiveSheet()->setCellValue('L1', 'DT DK');
        $excel->getActiveSheet()->setCellValue('M1', 'DT gia hạn');
        $excel->getActiveSheet()->setCellValue('N1', 'DT câu hỏi');
        $excel->getActiveSheet()->setCellValue('O1', 'Tổng DT');

        $i = 2;
        foreach ($result as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->stats_date);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->dk101);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->dksms);
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->dkussd);
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->tongdk);
            $excel->getActiveSheet()->setCellValue('G' . $i, $row->tongtb);
            $excel->getActiveSheet()->setCellValue('H' . $i, $row->huysms);
            $excel->getActiveSheet()->setCellValue('I' . $i, $row->huyussd);
            $excel->getActiveSheet()->setCellValue('J' . $i, $row->huyboiht);
            $excel->getActiveSheet()->setCellValue('K' . $i, $row->tonghuy);
            $excel->getActiveSheet()->setCellValue('L' . $i, $row->dtdk);
            $excel->getActiveSheet()->setCellValue('M' . $i, $row->dtgiahan);
            $excel->getActiveSheet()->setCellValue('N' . $i, $row->dtcauhoi);
            $excel->getActiveSheet()->setCellValue('O' . $i, $row->tongdt);

            $i++;
        }

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function apiStatistic(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $result = DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as \"STATS_DATE\", 
                sum(case
             when cp = 'INVITE' and item = '1' then
              SUBS
           end) as dk101,
       sum(case
             when CHANNEL = 'SMS' and item = '1' then
              SUBS
           end) as dksms,
       sum(case
             when CHANNEL = 'USSD' and CP is null and item = '1' then
              SUBS
           end) as dkussd,
       sum(case
             when item = '1' then
              SUBS
           end) as tongdk,
       sum(decode(item, '1', ACC_SUBS, '2', ACC_SUBS, 0)) as tongtb,
       sum(case
             when CHANNEL = 'SMS' and ITEM = '3' then
              SUBS
           end) as huysms,
       sum(case
             when CHANNEL = 'USSD' and ITEM = '3' then
              SUBS
           end) as huyussd,
       sum(case
             when CHANNEL not in ('SMS', 'USSD') and ITEM = '3' then
              SUBS
           end) as huyboiht,
       sum(case
             when ITEM = '3' then
              SUBS
           end) as tonghuy,
       sum(decode(ITEM, 'REGISTER', ACTUAL_REVENUE, 0)) as dtdk,
       sum(decode(ITEM, 'RENEW', ACTUAL_REVENUE, 0)) as dtgiahan,
       sum(decode(ITEM, 'ANSWER', ACTUAL_REVENUE, 0)) as dtcauhoi,
       sum(ACTUAL_REVENUE) as tongdt
                from \"STATS_GENERAL\" 
                WHERE group# in ('1', '2') AND STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                group by TO_CHAR(STATS_DATE,'dd/mm/yyyy') order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        return [
            'data' => $result,//$result->paginate($per_page),
        ];
    }

    public function apiExportStatisticPendingUssd(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        //$result = StatsPending::select("*")
        //    ->whereRaw("STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')")
        //    ->orderBy('STATS_DATE', 'ASC')
        //    ->get();

        $result= DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as STATS_DATE,
                total_pending,
                pending_d3, 
                pending_u3, 
                pending_u11,
                pending_u16,
                pending_gr30,
                pending_gr45
                from STATS_PENDING 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        $dir = 'export' . '/' . date('Y/m/d') . '/';
        $file_name = sprintf("%s-%s-%s.%s", 'file', date('YmdHis'), str_random(5), 'xlsx');
        $link_show = '/storage/' . $dir . $file_name;
        $link = storage_path("app/public/" . $dir . $file_name);

        if (!is_writable(dirname($link)) || !is_executable(dirname($link))) {
            mkdir(dirname($link), 0755, true);
        }

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Thống kê dịch vụ');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'Ngày');
        $excel->getActiveSheet()->setCellValue('C1', 'Tổng Pending');
        $excel->getActiveSheet()->setCellValue('D1', 'Số TB Pending 3 ngày liên tiếp');
        $excel->getActiveSheet()->setCellValue('E1', 'Số TB Pending < 3 ngày liên tiếp');
        $excel->getActiveSheet()->setCellValue('F1', 'Số TB Pending 5- 15 ngày');
        $excel->getActiveSheet()->setCellValue('G1', 'Số TB Pending > 10 ngày');
        $excel->getActiveSheet()->setCellValue('H1', 'Số TB Pending > 30 ngày');
        $excel->getActiveSheet()->setCellValue('I1', 'Số TB Pending > 45 ngày');

        $i = 2;
        foreach ($result as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->stats_date);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->total_pending);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->pending_d3);
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->pending_u3);
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->pending_u11);
            $excel->getActiveSheet()->setCellValue('G' . $i, $row->pending_u16);
            $excel->getActiveSheet()->setCellValue('H' . $i, $row->pending_gr30);
            $excel->getActiveSheet()->setCellValue('I' . $i, $row->pending_gr45);
            $i++;
        }

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function apiStatisticPendingUssd(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        /*$result = StatsPending::select("*")
            ->whereRaw("STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')")
            ->orderBy('STATS_DATE', 'ASC')
            ->get();*/
        $result= DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as STATS_DATE,
                total_pending,
                pending_d3, 
                pending_u3, 
                pending_u11,
                pending_u16,
                pending_gr30,
                pending_gr45
                from STATS_PENDING 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        return [
            'data' => $result,//->paginate($per_page),
        ];
    }

    public function apiExportStatisticCancelUssd(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        /*$result = StatsUnreg::select("*")
            ->whereRaw("STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')")
            ->orderBy('STATS_DATE', 'ASC')
            ->get();*/

        $result= DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as STATS_DATE,
                total_unreg,
                unreg_in_5min, 
                unreg_in_day, 
                unreg_in_week,
                unreg_gr_week
                from STATS_UNREG 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        $dir = 'export' . '/' . date('Y/m/d') . '/';
        $file_name = sprintf("%s-%s-%s.%s", 'file', date('YmdHis'), str_random(5), 'xlsx');
        $link_show = '/storage/' . $dir . $file_name;
        $link = storage_path("app/public/" . $dir . $file_name);

        if (!is_writable(dirname($link)) || !is_executable(dirname($link))) {
            mkdir(dirname($link), 0755, true);
        }

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Thống kê dịch vụ');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'Ngày');
        $excel->getActiveSheet()->setCellValue('C1', 'Tổng Hủy');
        $excel->getActiveSheet()->setCellValue('D1', 'Hủy ngay sau khi DK (<5 phút)');
        $excel->getActiveSheet()->setCellValue('E1', 'Hủy trong ngày DK');
        $excel->getActiveSheet()->setCellValue('F1', 'Hủy sau 1 tuần DK');
        $excel->getActiveSheet()->setCellValue('G1', 'Hủy > 1 tuần DK');

        $i = 2;
        foreach ($result as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->stats_date);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->total_unreg);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->unreg_in_5min);
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->unreg_in_day);
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->unreg_in_week);
            $excel->getActiveSheet()->setCellValue('G' . $i, $row->unreg_gr_week);

            $i++;
        }

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function apiStatisticCancelUssd(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

       /* $result = StatsUnreg::select("*")
            ->whereRaw("STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')")
            ->orderBy('STATS_DATE', 'ASC')
            ->get();*/

        $result= DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as STATS_DATE,
                total_unreg,
                unreg_in_5min, 
                unreg_in_day, 
                unreg_in_week,
                unreg_gr_week
                from STATS_UNREG 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        return [
            'data' => $result,//->paginate($per_page),
        ];
    }

    public function apiExportStatisticControl(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $result= DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'MM/YYYY') as \"STATS_DATE\", 
                sum(decode(PKG_VALUE, 500, count#)) as total500,
       sum(decode(PKG_VALUE, 500, REVENUE)) as dt_500,
       sum(decode(PKG_VALUE, 1000, count#)) as total1000,
       sum(decode(PKG_VALUE, 1000, REVENUE)) as dt_1000,
       sum(decode(PKG_VALUE, 2000, count#)) as total2000,
       sum(decode(PKG_VALUE, 2000, REVENUE)) as dt_2000
                from \"DOI_SOAT\" 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'MM/YYYY') AND TO_DATE ('$endDate', 'MM/YYYY') 
                group by TO_CHAR(STATS_DATE,'MM/YYYY') order by \"STATS_DATE\" asc"));
        $dir = 'export' . '/' . date('Y/m/d') . '/';
        $file_name = sprintf("%s-%s-%s.%s", 'file', date('YmdHis'), str_random(5), 'xlsx');
        $link_show = '/storage/' . $dir . $file_name;
        $link = storage_path("app/public/" . $dir . $file_name);

        if (!is_writable(dirname($link)) || !is_executable(dirname($link))) {
            mkdir(dirname($link), 0755, true);
        }

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Đối soát sản lượng');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'Tháng');
        $excel->getActiveSheet()->setCellValue('C1', 'Lượt tải tính cước gói 2000');
        $excel->getActiveSheet()->setCellValue('D1', 'Lượt tải tính cước gói 1000');
        $excel->getActiveSheet()->setCellValue('E1', 'Lượt tải tính cước gói 500');

        $i = 2;
        foreach ($result as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->stats_date);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->total2000);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->total1000);
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->total500);

            $i++;
        }

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function apiStatisticControl(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        //$result = DoiSoat::select("*")
            //->whereBetween('STATS_DATE', [$startDate, $endDate])
        //    ->select(
        //        DB::raw("TO_CHAR(STATS_DATE,'MM/YYYY') as \"STATS_DATE\""),
        //        DB::raw('count(case when PKG_VALUE = 500 then \'x\' end) as total500'),
        //        DB::raw('count(case when PKG_VALUE = 1000 then \'x\' end) as total1000'),
        //        DB::raw('count(case when PKG_VALUE = 2000 then \'x\' end) as total2000')
        //    )
        //    ->groupBy('TO_CHAR(STATS_DATE,\'MM/YYYY\')')
        //    ->orderBy('STATS_DATE', 'ASC');

        $data = DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'MM/YYYY') as \"STATS_DATE\", 
                sum(decode(PKG_VALUE, 500, count#)) as total500,
               sum(decode(PKG_VALUE, 500, REVENUE)) as dt_500,
               sum(decode(PKG_VALUE, 1000, count#)) as total1000,
               sum(decode(PKG_VALUE, 1000, REVENUE)) as dt_1000,
               sum(decode(PKG_VALUE, 2000, count#)) as total2000,
               sum(decode(PKG_VALUE, 2000, REVENUE)) as dt_2000
                from \"DOI_SOAT\" 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'MM/YYYY') AND TO_DATE ('$endDate', 'MM/YYYY') 
                group by TO_CHAR(STATS_DATE,'MM/YYYY') order by \"STATS_DATE\" asc"));

        return [
            'data' => $data,//$result->paginate($per_page),
        ];
    }

    public function apiStatisticCharge(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

//        $startDate = implode(explode('-',Carbon::parse($request->get('start_date'))->format('Y-m-d')));
        $endDate = implode(explode('-',Carbon::parse($request->get('end_date'))->format('Y-m-d')));
        $trackDate = implode(explode('-',Carbon::parse($request->get('track_date'))->format('Y-m-d')));

        $data = DB::connection('mysql')
            ->select(DB::raw("select 
        $trackDate as ngay_theo_doi,
        (select count(1) from subscriber where TO_CHAR(reg_time,'YYYYMMDD') = $trackDate) as tong_dang_ki,
        created_date as ngay_charge,
        thue_bao_charge,
        ti_le,
        thue_bao_huy,
        (select count(1) from subscriber where service_state=1) as tong_active
    from
    (
        select created_date,
            sum(case when action = 'RENEW' then 1 else 0 END) thue_bao_charge,
            sum(case when action = 'RENEW' then 1 else 0 END)/(select count(1) from subscriber where TO_CHAR(reg_time,'YYYYMMDD') = $trackDate) ti_le,
            sum(case when action = 'UNREG' then 1 else 0 END) thue_bao_huy 
        from 
            (
               select * from his h join subscriber s on h.msisdn = s.msisdn and h.pkg_code = s.pkg_code
                where  h.action in ('RENEW','UNREG')
                   --  and msisdn in (select msisdn from subscriber where TO_CHAR(reg_time,'YYYYMMDD') = 20190715)
                  and TO_CHAR(s.reg_time,'YYYYMMDD') = $trackDate 
                  and  (h.created_date between $trackDate and $endDate)
                  and h.result = 0
            )
        group by created_date
    )
    order by created_date"));
        $dataConvert = [];
        foreach($data as $key => $value){
            $dataConvert[$key]['value'] = $value;
            if ($key == 0){
                $dataConvert[$key]['active'] = $value->tong_dang_ki - $value->thue_bao_huy ;
                $dataConvert[$key]['tile'] = 0;
            }else{
                $dataConvert[$key]['active'] = $dataConvert[$key - 1]['active'] - $value->thue_bao_huy;
                $dataConvert[$key]['tile'] = round(($value->thue_bao_charge/ $dataConvert[$key - 1]['active'])*100, 2);
            }
        }
        return [
            'data' => $dataConvert,//$result->paginate($per_page),
        ];
    }


    public function apiStatisticMedia(Request $request){
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $trackDate = implode(explode('-',Carbon::parse($request->get('track_date'))->format('Y-m-d')));

        $data = DB::connection('mysql')
            ->select(DB::raw("select 
        TO_CHAR(created_time,'HH24') hour,
        sum(1) tong,
        sum(case when result = 0 then 1 else 0 end) thanh_cong,
        sum(case when result = 0 then 0 else 1 end) that_bai
    from mt_crosssale where created_date =$trackDate
    group by TO_CHAR(created_time,'HH24')
    order by hour"));

        return [
            'data' => $data,//$result->paginate($per_page),
        ];
    }

    public function apiExportStatisticInviteUssd(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        /*$result = StatsInvite::select("*")
            ->whereRaw("STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')")
            ->orderBy('STATS_DATE', 'ASC')->get();*/

        $result= DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as STATS_DATE,
                invite_subs,
                subs_resp_1, 
                subs_resp_2, 
                subs_un_resp
                from STATS_INVITE 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        $dir = 'export' . '/' . date('Y/m/d') . '/';
        $file_name = sprintf("%s-%s-%s.%s", 'file', date('YmdHis'), str_random(5), 'xlsx');
        $link_show = '/storage/' . $dir . $file_name;
        $link = storage_path("app/public/" . $dir . $file_name);

        if (!is_writable(dirname($link)) || !is_executable(dirname($link))) {
            mkdir(dirname($link), 0755, true);
        }

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Báo cáo Invite USSD');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'Ngày');
        $excel->getActiveSheet()->setCellValue('C1', 'Tổng invite USSD');
        $excel->getActiveSheet()->setCellValue('D1', 'Phản hồi 1');
        $excel->getActiveSheet()->setCellValue('E1', 'Phản hồi 2');
        $excel->getActiveSheet()->setCellValue('F1', 'Không phản hồi');

        $i = 2;
        foreach ($result as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->stats_date);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->invite_subs);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->subs_resp_1);
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->subs_resp_2);
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->subs_un_resp);

            $i++;
        }

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function apiStatisticInviteUssd(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

       /* $result = StatsInvite::select("*")
        ->whereRaw("STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')")
        ->orderBy('STATS_DATE', 'ASC')->get();*/

        $result= DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as STATS_DATE,
                invite_subs,
                subs_resp_1, 
                subs_resp_2, 
                subs_un_resp
                from STATS_INVITE 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        return [
            'data' => $result,//->paginate($per_page),
        ];
    }

    public function apiExportStatisticQuestion(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        /*$result = StatsQuestion::select("*")
            ->whereRaw("STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')")
            ->orderBy('STATS_DATE', 'ASC')
            ->get();*/

        $result= DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as STATS_DATE,
                total_question,
                free_question, 
                charge_question, 
                SUBS_RESPONSE,
                SUBS_UNRESPONSE,
                subs_u3,
                subs_u11,
                subs_u21,
                subs_u31
                from STATS_QUESTIONS 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        $dir = 'export' . '/' . date('Y/m/d') . '/';
        $file_name = sprintf("%s-%s-%s.%s", 'file', date('YmdHis'), str_random(5), 'xlsx');
        $link_show = '/storage/' . $dir . $file_name;
        $link = storage_path("app/public/" . $dir . $file_name);

        if (!is_writable(dirname($link)) || !is_executable(dirname($link))) {
            mkdir(dirname($link), 0755, true);
        }

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Báo cáo câu hỏi USSD');
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $excel->getDefaultStyle()->applyFromArray($style);

        $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

        $excel->getActiveSheet()->setCellValue('A1', 'STT');
        $excel->getActiveSheet()->setCellValue('B1', 'Ngày');
        $excel->getActiveSheet()->setCellValue('C1', 'Tổng số câu hỏi đã push');
        $excel->getActiveSheet()->setCellValue('D1', 'Câu hỏi miễn phí');
        $excel->getActiveSheet()->setCellValue('E1', 'Mất phí');
        $excel->getActiveSheet()->setCellValue('F1', 'Số TB có trả lời');
        $excel->getActiveSheet()->setCellValue('G1', 'Số TB không trả lời');
        $excel->getActiveSheet()->setCellValue('H1', 'Số TB =< 2 câu');
        $excel->getActiveSheet()->setCellValue('I1', 'Số TB 3 =< 10 câu');
        $excel->getActiveSheet()->setCellValue('J1', 'Số TB 11 =< 20 câu');
        $excel->getActiveSheet()->setCellValue('K1', 'Số TB 12 =< 30 câu');

        $i = 2;
        foreach ($result as $key => $row) {
            $excel->getActiveSheet()->setCellValue('A' . $i, $key + 1);
            $excel->getActiveSheet()->setCellValue('B' . $i, $row->stats_date);
            $excel->getActiveSheet()->setCellValue('C' . $i, $row->total_question);
            $excel->getActiveSheet()->setCellValue('D' . $i, $row->free_question);
            $excel->getActiveSheet()->setCellValue('E' . $i, $row->charge_question);
            $excel->getActiveSheet()->setCellValue('F' . $i, $row->SUBS_RESPONSE);
            $excel->getActiveSheet()->setCellValue('G' . $i, $row->SUBS_UNRESPONSE);
            $excel->getActiveSheet()->setCellValue('H' . $i, $row->subs_u3);
            $excel->getActiveSheet()->setCellValue('I' . $i, $row->subs_u11);
            $excel->getActiveSheet()->setCellValue('J' . $i, $row->subs_u21);
            $excel->getActiveSheet()->setCellValue('K' . $i, $row->subs_u31);

            $i++;
        }

        PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save($link);

        return [
            'data' => $link_show
        ];
    }

    public function apiStatisticQuestion(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        /*$result = StatsQuestion::select("*")
            ->whereRaw("STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')")
            ->orderBy('STATS_DATE', 'ASC')
            ->get();*/

        $result= DB::connection('mysql')
            ->select(DB::raw("
                select TO_CHAR(STATS_DATE,'dd/mm/yyyy') as STATS_DATE,
                total_question,
                free_question, 
                charge_question, 
                SUBS_RESPONSE,
                SUBS_UNRESPONSE,
                subs_u3,
                subs_u11,
                subs_u21,
                subs_u31
                from STATS_QUESTIONS 
                WHERE STATS_DATE BETWEEN TO_DATE ('$startDate', 'dd/mm/yyyy') AND TO_DATE ('$endDate', 'dd/mm/yyyy')
                order by TO_DATE(STATS_DATE, 'dd/mm/yyyy') asc"));

        return [
            'data' => $result,//->paginate($per_page),
        ];
    }

}
