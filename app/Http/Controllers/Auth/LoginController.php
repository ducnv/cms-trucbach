<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Support\Facades\Cache;
use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {

        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    protected function authenticated(Request $request, $user)
    {
        try {
            /*
            * Save new session_id, last_login time and last_login_ip
            */
            $user->last_login = Carbon::now();
            $user->last_login_ip = $request->ip();
            $user->session_id = $request->session()->getId();
            $user->save();

            Session::put('user_id', $user->id);
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }
        return false;
    }
}
