<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ToolController extends Controller
{
    public function thongKeGuiLoiMoi()
    {
        $this->views['title'] = 'Thống kê gửi lời mời';

        return view('tool.thongkeguiloimoi', $this->views);
    }

    public function baoCaoTongQuanThang()
    {
        $this->views['title'] = 'Báo cáo tổng quan tháng';

        return view('tool.baocaotongquanthang', $this->views);
    }

    public function baoCaoTongQuanNgay()
    {
        $this->views['title'] = 'Báo cáo tổng quan ngày';

        return view('tool.baocaotongquanngay', $this->views);
    }

    public function baoCaoGuiLoiMoi()
    {
        $this->views['title'] = 'Báo cáo gửi lời mời';

        return view('tool.baocaoguiloimoi', $this->views);
    }

    public function thongKeThueBaoPending()
    {
        $this->views['title'] = 'Thống kê thuê bao pending';

        return view('tool.thongkethuebaopending', $this->views);
    }

    public function doiSoatTheoThang()
    {
        $this->views['title'] = 'Đối soát theo tháng';

        return view('tool.doisoattheothang', $this->views);
    }

    public function tiLeChargeDichVu()
    {
        $this->views['title'] = 'Tỉ lệ charge dịch vụ';

        return view('tool.tilechargedichvu', $this->views);
    }

    public function theoDoiSanluongDangKi()
    {
        $this->views['title'] = 'Theo dõi sản lượng đăng kí theo ngày';

        return view('tool.theodoisanluongdangki', $this->views);
    }

    public function apiBaoCaoGuiLoiMoi(Request $request)
    {
        $trackDate = implode(explode('-',Carbon::parse($request->get('track_date'))->format('Y-m-d')));

        //Báo cáo doanh thu
        $data = DB::connection('mysql')
            ->select(DB::raw("select hour,sum(invited) invited,sum(tc) tc,sum(accept) accepted from ( 
                select to_number(to_char(created_time,'HH24')) hour,count(*) invited, 0 tc,0 accept from mt where created_date = $trackDate  and action = 'TAPPING' and result = 0 group by to_number(to_char(created_time,'HH24')) 
                union
                select to_number(to_char(created_time,'HH24')) hour,0 invited,sum(case action when 'tc' then 1 else 0 end) tc, sum(case when action = 'Register' and mo = '1' then 1 else 0 end) accept from mo where created_date = $trackDate and action in ('Register','tc') group by to_number(to_char(created_time,'HH24'))) group by hour order by hour"));

        $dataTotal = [];
        $invited = 0;
        $accepted = 0;
        $tc = 0;
        foreach($data as $key => $value){
            $dataTotal['invited'] = $invited += $value->invited;
            $dataTotal['accepted'] = $accepted += $value->accepted;
            $dataTotal['tc'] = $tc += $value->tc;
        }

        $dataConvert = $dataTotal;
        foreach($data as $key => $value){
            $dataConvert['data2'][$key]['value'] = $value;
        }
        return [
            'data' => $dataConvert,//$result->paginate($per_page),
        ];
    }

    public function apiThongKeGuiLoiMoi(Request $request)
    {
//        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

//        $startDate = implode(explode('-',Carbon::parse($request->get('start_date'))->format('Y-m-d')));
        $trackDate1 = implode(explode('-',Carbon::parse($request->get('track_date1'))->format('Y-m-d')));
        $trackDate2 = implode(explode('-',Carbon::parse($request->get('track_date2'))->format('Y-m-d')));

        $data = DB::connection('mysql')
            ->select(DB::raw("select to_char(created_time,'HH24') hour, sum(1) invited,
                 sum(case when result = 0 then 0 else 1 end) tc,
                 sum(case when result = 0 then 1 else 0 end) accepted ,
                 (sum(case when result = 0 then 1 else 0 end)/sum(1)) ti_le,
                 sum(case when result = 104 then 1 else 0 end) tps, 
                 sum(case when result = 105 then 1 else 0 end) packages,
                 sum(case when result in (107,106) then 1 else 0 end) gpc, 
                 sum(case when result = 108 then 1 else 0 end) blacklist,
                sum(case when result not in (108,107,105,106,104,0) then 1 else 0 end) timeout 
             from mt_cross_sell where created_date between  $trackDate1 and  $trackDate2 group by to_char(created_time,'HH24') order by hour"));

//        $dataConvert = [];
        $dataTotal = [];
        $invited = 0;
        $accepted = 0;
        $tc = 0;
        $ti_le = 0;
        $tps = 0;
        $packages = 0;
        $gpc = 0;
        $blacklist = 0;
        $timeout = 0;
        $count = count($data);
        foreach($data as $key => $value){
            $dataTotal['invited'] = $invited += $value->invited;
            $dataTotal['accepted'] = $accepted += $value->accepted;
            $dataTotal['tc'] = $tc += $value->tc;
            $dataTotal['ti_le'] =  	round(($dataTotal['accepted']/ $dataTotal['invited'])*100, 2);
            $dataTotal['tps'] = $tps += $value->tps;
            $dataTotal['packages'] = $packages += $value->packages;
            $dataTotal['gpc'] = $gpc += $value->gpc;
            $dataTotal['blacklist'] = $blacklist += $value->blacklist;
            $dataTotal['timeout'] = $timeout += $value->timeout;
        }

        $dataConvert = $dataTotal;
        foreach($data as $key => $value){
            $dataConvert['data2'][$key]['value'] = $value;
            $dataConvert['data2'][$key]['tile'] = round(($value->ti_le)*100, 2);
        }
        return [
            'data' => $dataConvert,//$result->paginate($per_page),
        ];
    }

    public function apiBaoCaoTongQuanThang(Request $request)
    {
        //        $startDate = implode(explode('-',Carbon::parse($request->get('start_date'))->format('Y-m-d')));
//        $trackDate = implode(explode('-', Carbon::createFromDate($year, $month, 1)->endOfMonth()->format('Y-m-d')));
        $trackDate = explode('-',$request->get('track_date'));
        $month = $trackDate[0];
        $year = $trackDate[1];
        $startDate = implode(explode('-',Carbon::createFromDate($year, $month, 1)->format('Y-m-d')));
        $endDate = implode(explode('-', Carbon::createFromDate($year, $month, 1)->endOfMonth()->format('Y-m-d')));


        //Báo cáo doanh thu
        $data = DB::connection('mysql')
            ->select(DB::raw(" select created_date day
                ,sum(case when action in ('FIRST_REG','RE_REG') then c_fee else 0 end) reg_rev
                ,sum(case when action in ('RENEW') then c_fee else 0 end) renew_rev
                ,sum(case when action in ('ANSWER') then c_fee else 0 end) question_rev
                ,sum(case c_fee when 500 then c_fee else 0 end) xx_5t
                ,sum(case c_fee when 2000 then c_fee else 0 end) xx_2k
                ,sum(case c_fee when 500 then 1 else 0 end) cc_5t
                ,sum(case c_fee when 2000 then 1 else 0 end) cc_2k
                from his
                where created_date >= $startDate and created_date <= $endDate and action in ('FIRST_REG','RE_REG','RENEW', 'ANSWER') and result = 0
                group by created_date
                order by created_date"));

        $dataTotal = [];
        $reg_rev = 0;
        $renew_rev = 0;
        $question_rev = 0;
        $tongDoanhThu = 0;
        $doanhThuLuyKe = 0;
        $xx_5t = 0;
        $xx_2k = 0;
        $cc_2k = 0;
        $cc_5t = 0;
        $count = count($data);
        foreach($data as $key => $value){
            $dataTotal['reg_rev'] = $reg_rev += $value->reg_rev;
            $dataTotal['renew_rev'] = $renew_rev += $value->renew_rev;
            $dataTotal['question_rev'] = $question_rev += $value->question_rev;
            $dataTotal['tongDoanhThu'] =  $dataTotal['reg_rev'] + $dataTotal['renew_rev'];
//            $dataTotal['doanhThuLuyKe'] = $doanhThuLuyKe += $value->doanhThuLuyKe;
            $dataTotal['xx_5t'] = $xx_5t += $value->xx_5t;
            $dataTotal['xx_2k'] = $xx_2k += $value->xx_2k;
            $dataTotal['cc_2k'] = $cc_2k += $value->cc_2k;
            $dataTotal['cc_5t'] = $cc_5t += $value->cc_5t;
        }
        $dataConvert = $dataTotal;

        foreach($data as $key => $value){
            $dataConvert['data2'][$key]['value'] = $value;
            $dataConvert['data2'][$key]['tongDoanhThu'] = $value->reg_rev + $value->renew_rev;
            if ($key == 0 ){
                $dataConvert['data2'][$key]['doanhThuLuyKe'] = $dataConvert['data2'][$key]['tongDoanhThu'] ;
            }else{
                $dataConvert['data2'][$key]['doanhThuLuyKe'] = $dataConvert['data2'][$key]['tongDoanhThu'] + $dataConvert['data2'][$key - 1]['doanhThuLuyKe'];
            }
        }
        //Báo cáo phát triển thuê bao
        $dataPhatTrienTB = DB::connection('mysql')
            ->select(DB::raw("select created_date day
                ,sum(case when action in ('FIRST_REG','RE_REG') then 1 else 0 end) total_reg 
                ,sum(case when action = 'UNREG' then 1 else 0 end) total_unreg 
                ,sum(case when action in ('FIRST_REG','RE_REG') and channel = 'SMS' then 1 else 0 end) total_reg_sms 
                ,sum(case when action in ('FIRST_REG','RE_REG') and channel = 'USSD' and cp = 'INVITE' then 1 else 0 end) total_reg_invite 
                ,sum(case when action in ('FIRST_REG','RE_REG') and channel = 'USSD' and cp != 'INVITE' then 1 else 0 end) total_reg_ussd 
                ,sum(case when action in ('FIRST_REG','RE_REG') and channel not in ('SMS','USSD') then 1 else 0 end) total_reg_other 
                ,sum(case when action = 'UNREG' and channel = 'USSD' then 1 else 0 end) total_unreg_ussd 
                ,sum(case when action = 'UNREG' and channel = 'SMS' then 1 else 0 end) total_unreg_sms 
                ,sum(case when action = 'UNREG' and channel not in ('SMS','USSD') then 1 else 0 end) total_unreg_other 
                from his 
                where created_date >= $startDate and created_date <= $endDate and action in ('FIRST_REG','RE_REG','UNREG') and result = 0
                group by created_date
                 order by created_date"));


        $dataTotal2 = [];
        $total_reg = 0;
        $total_unreg = 0;
        $total_reg_sms = 0;
        $total_reg_invite = 0;
        $total_reg_ussd = 0;
        $total_reg_other = 0;
        $total_unreg_sms = 0;
        $total_unreg_ussd = 0;
        $total_unreg_other = 0;
        foreach($dataPhatTrienTB as $key => $value){
            $dataTotal2['total_reg'] = $total_reg += $value->total_reg;
            $dataTotal2['total_unreg'] = $total_unreg += $value->total_unreg;
            $dataTotal2['TBPT'] =$dataTotal2['total_reg'] - $dataTotal2['total_unreg'];
            $dataTotal2['total_reg_sms'] = $total_reg_sms += $value->total_reg_sms;
            $dataTotal2['total_reg_invite'] = $total_reg_invite += $value->total_reg_invite;

            $dataTotal2['total_reg_ussd'] = $total_reg_ussd += $value->total_reg_ussd;
            $dataTotal2['total_reg_other'] = $total_reg_other += $value->total_reg_other;
            $dataTotal2['total_unreg_sms'] = $total_unreg_sms += $value->total_unreg_sms;
            $dataTotal2['total_unreg_ussd'] = $total_unreg_ussd += $value->total_unreg_ussd;
            $dataTotal2['total_unreg_other'] = $total_unreg_other += $value->total_unreg_other;
        }
        $dataConvert2 = $dataTotal2;
        foreach($dataPhatTrienTB as $key => $value){
            $dataConvert2['data2'][$key]['value'] = $value;
        }

        $listData = [$dataConvert,$dataConvert2];

        return [
            'data' => $listData,//$result->paginate($per_page),
        ];
    }

    public function apiBaoCaoTongQuanNgay(Request $request)
    {
        $trackDate = implode(explode('-',Carbon::parse($request->get('track_date'))->format('Y-m-d')));


        //Báo cáo doanh thu
        $data = DB::connection('mysql')
            ->select(DB::raw("select to_number(to_char(created_time,'HH24')) hour
                ,sum(case when action in ('FIRST_REG','RE_REG') then c_fee else 0 end) reg_rev 
                ,sum(case when action in ('RENEW') then c_fee else 0 end) renew_rev 
                ,sum(case when action in ('ANSWER') then c_fee else 0 end) question_rev 
                from his 
                where created_date = $trackDate and action in ('FIRST_REG','RE_REG','RENEW', 'ANSWER') and result = 0 
                group by to_number(to_char(created_time,'HH24'))
                order by to_number(to_char(created_time,'HH24'))"));

        $dataTotal = [];
        $reg_rev = 0;
        $renew_rev = 0;
        $question_rev = 0;
        foreach($data as $key => $value){
            $dataTotal['reg_rev'] = $reg_rev += $value->reg_rev;
            $dataTotal['renew_rev'] = $renew_rev += $value->renew_rev;
            $dataTotal['question_rev'] = $question_rev += $value->question_rev;
            $dataTotal['tongDoanhThu'] =  $dataTotal['reg_rev'] + $dataTotal['renew_rev'];
//            $dataTotal['doanhThuLuyKe'] = $doanhThuLuyKe += $value->doanhThuLuyKe;
        }
        $dataConvert = $dataTotal;

        foreach($data as $key => $value){
            $dataConvert['data2'][$key]['value'] = $value;
            $dataConvert['data2'][$key]['tongDoanhThu'] = $value->reg_rev + $value->renew_rev;
            if ($key == 0 ){
                $dataConvert['data2'][$key]['doanhThuLuyKe'] = $dataConvert['data2'][$key]['tongDoanhThu'] ;
            }else{
                $dataConvert['data2'][$key]['doanhThuLuyKe'] = $dataConvert['data2'][$key]['tongDoanhThu'] + $dataConvert['data2'][$key - 1]['doanhThuLuyKe'];
            }
        }
//        //Báo cáo phát triển thuê bao
        $dataPhatTrienTB = DB::connection('mysql')
            ->select(DB::raw("select a.hour, total_reg, total_unreg, total_reg_sms, total_reg_invite, total_reg_ussd, total_reg_other,
        total_unreg_ussd, total_unreg_sms, total_unreg_other, case when (accepted = 0 or accepted is null) then 0 else trunc(total_reg/accepted*100,3) end ti_le_dk,
        case when (accepted = 0 or accepted is null) then 0 else accepted end accepted
     from
        ( select to_number(to_char(created_time,'HH24')) hour 
                ,sum(case when action in ('FIRST_REG','RE_REG') then 1 else 0 end) total_reg 
                ,sum(case when action = 'UNREG' then 1 else 0 end) total_unreg 
                ,sum(case when action in ('FIRST_REG','RE_REG') and channel = 'SMS' then 1 else 0 end) total_reg_sms 
                ,sum(case when action in ('FIRST_REG','RE_REG') and channel = 'USSD' and cp = 'VASCLOUD' and syntax = 'DK3G' then 1 else 0 end) total_reg_invite 
                ,sum(case when action in ('FIRST_REG','RE_REG') and channel = 'USSD' and (syntax != 'DK3G' or cp is NULL) then 1 else 0 end) total_reg_ussd 
                ,sum(case when action in ('FIRST_REG','RE_REG') and channel not in ('SMS','USSD') then 1 else 0 end) total_reg_other 
                ,sum(case when action = 'UNREG' and channel = 'USSD' then 1 else 0 end) total_unreg_ussd 
                ,sum(case when action = 'UNREG' and channel = 'SMS' then 1 else 0 end) total_unreg_sms 
                ,sum(case when action = 'UNREG' and channel not in ('SMS','USSD') then 1 else 0 end) total_unreg_other 
                from his 
                where created_date = $trackDate and action in ('FIRST_REG','RE_REG','UNREG') and result = 0 
                group by to_number(to_char(created_time,'HH24'))) a
        left join ( select to_char(created_time,'HH24') hour, sum(case when result = 0 then 1 else 0 end) accepted 
                from mt_cross_sell where created_date = $trackDate group by to_char(created_time,'HH24') order by hour ) b
             on a.hour = b.hour
        order by a.hour"));


        $dataTotal2 = [];
        $total_reg = 0;
        $total_unreg = 0;
        $total_reg_sms = 0;
        $total_reg_invite = 0;
        $total_reg_ussd = 0;
        $total_reg_other = 0;
        $total_unreg_sms = 0;
        $total_unreg_ussd = 0;
        $total_unreg_other = 0;
        $ti_le_dk = 0;
        $accepted = 0;
        foreach($dataPhatTrienTB as $key => $value){
            $dataTotal2['total_reg'] = $total_reg += $value->total_reg;
            $dataTotal2['total_unreg'] = $total_unreg += $value->total_unreg;
            $dataTotal2['TBPT'] =$dataTotal2['total_reg'] - $dataTotal2['total_unreg'];
            $dataTotal2['total_reg_sms'] = $total_reg_sms += $value->total_reg_sms;
            $dataTotal2['total_reg_invite'] = $total_reg_invite += $value->total_reg_invite;

            $dataTotal2['total_reg_ussd'] = $total_reg_ussd += $value->total_reg_ussd;
            $dataTotal2['total_reg_other'] = $total_reg_other += $value->total_reg_other;
            $dataTotal2['total_unreg_sms'] = $total_unreg_sms += $value->total_unreg_sms;
            $dataTotal2['total_unreg_ussd'] = $total_unreg_ussd += $value->total_unreg_ussd;
            $dataTotal2['total_unreg_other'] = $total_unreg_other += $value->total_unreg_other;
            $dataTotal2['ti_le_dk'] = $ti_le_dk += $value->ti_le_dk;
            $dataTotal2['accepted'] = $accepted += $value->accepted;
        }

        $dataConvert2 = $dataTotal2;
        foreach($dataPhatTrienTB as $key => $value){
            $dataConvert2['data2'][$key]['value'] = $value;
            $dataConvert2['data2'][$key]['ti_le_dk'] = round($value->ti_le_dk, 3);
        }

        // Danh sách top 50 thuê bao dẫn đầu điểm số
        $dataTopTB = DB::connection('mysql')
            ->select(DB::raw("select * from (select MSISDN,sum(POINT) point,max(created_time) max_time from point_his 
                where avaiable_date = $trackDate group by MSISDN order by point desc, max_time asc) where rownum <= 50"));

        $listData = [$dataConvert,$dataConvert2,$dataTopTB];

        return [
            'data' => $listData,//$result->paginate($per_page),
        ];
    }

    public function apiThongKeThueBaoPending(Request $request)
    {
//        $per_page = $request->has('per_page') ? $request->get('per_page') : 20;

//        $startDate = implode(explode('-',Carbon::parse($request->get('start_date'))->format('Y-m-d')));
        $trackDate1 = implode(explode('-',Carbon::parse($request->get('track_date1'))->format('Y-m-d')));
        $trackDate2 = implode(explode('-',Carbon::parse($request->get('track_date2'))->format('Y-m-d')));

        $data = DB::connection('mysql')
            ->select(DB::raw("select created_date ngay_gui,
                    sum(1) total,
                    SUM(case when result = 0 then 1 else 0 end) thanh_cong,
                    SUM(case when result = 104 then 1 else 0 end) qua_tps,
                    SUM(case when result = -1 then 1 else 0 end) khac,
                    (select count(1) from his where action = 'UNREG' and created_date = m.created_date
                    and msisdn in (select msisdn from mt where action like 'PROMO_PENDING%')) huy
                from mt m where created_date between $trackDate1 and $trackDate2 and action like 'PROMO_PENDING%' 
                group by  created_date
                order by created_date desc"));

//        $dataConvert = [];

        $dataTotal = [];
        $huy = 0;
        $khac = 0;
        $qua_tps = 0;
        $thanh_cong = 0;
        $total = 0;
        foreach($data as $key => $value){
            $dataTotal['huy'] = $huy += $value->huy;
            $dataTotal['khac'] = $khac += $value->khac;
            $dataTotal['qua_tps'] = $qua_tps += $value->qua_tps;
            $dataTotal['thanh_cong'] = $thanh_cong += $value->thanh_cong;
            $dataTotal['total'] = $total += $value->total;

        }

        $dataConvert = $dataTotal;
        foreach($data as $key => $value){
            $dataConvert['data2'][$key]['value'] = $value;
        }
        return [
            'data' => $dataConvert,//$result->paginate($per_page),
        ];
    }

    public function apiDoiSoatTheoThang(Request $request)
    {
        $trackDate = explode('-',$request->get('track_date'));
        $month = $trackDate[0];
        $year = $trackDate[1];
        $startDate = implode(explode('-',Carbon::createFromDate($year, $month, 1)->format('Y-m-d')));
        $endDate = implode(explode('-', Carbon::createFromDate($year, $month, 1)->endOfMonth()->format('Y-m-d')));

        $data = DB::connection('mysql')
            ->select(DB::raw("select pkg_code goi,c_fee don_gia,count(1) so_luong, sum(c_fee) tong_tien from his where  created_date between $startDate and $endDate
and result =0 and c_fee >0 group by pkg_code,c_fee order by pkg_code"));

        $dataConvert = [];

        $dataTotal = [];
        $so_luong = 0;
        $tong_tien = 0;
        foreach($data as $key => $value){
            $dataTotal['so_luong'] = $so_luong += $value->so_luong;
            $dataTotal['tong_tien'] = $tong_tien += $value->tong_tien;

        }

        $dataConvert = $dataTotal;
        foreach($data as $key => $value){
            $dataConvert['data2'][$key]['value'] = $value;
        }
        return [
            'data' => $dataConvert,//$result->paginate($per_page),
        ];
    }

    public function apiTiLeChargeDichVu(Request $request)
    {
        $trackDate1 = implode(explode('-',Carbon::parse($request->get('track_date1'))->format('Y-m-d')));
//        $trackDate2 = implode(explode('-',Carbon::parse($request->get('track_date2'))->format('Y-m-d')));

        $data = DB::connection('mysql')
            ->select(DB::raw("select a.pkg_code, a.sl, b.total from 
    (select pkg_code, count(1) sl from his where created_date between $trackDate1 and $trackDate1 and action = 'RENEW' group by pkg_code) a
   join
   (select pkg_code, count(1) total from subscriber where service_state =1 and to_char(reg_time,'YYYYMMDD')<= $trackDate1 group by pkg_code) b
   on a.pkg_code = b.pkg_code"));

        $dataTotal = [];
        $sl = 0;
        $total = 0;
        foreach($data as $key => $value){
            $dataTotal['sl'] = $sl += $value->sl;
            $dataTotal['total'] = $total += $value->total;

        }

        $dataConvert = $dataTotal;
        foreach($data as $key => $value){
            $dataConvert['data2'][$key]['value'] = $value;
        }
        return [
            'data' => $dataConvert,//$result->paginate($per_page),
        ];
    }

    public function apiTheoDoiSanluongDangKi(Request $request)
    {
        $trackDate1 = implode(explode('-',Carbon::parse($request->get('track_date1'))->format('Y-m-d')));
        $trackDate2 = implode(explode('-',Carbon::parse($request->get('track_date2'))->format('Y-m-d')));

        $data = DB::connection('mysql')
            ->select(DB::raw("select b.created_date,TONG_DANG_KY,BDSCAU,BDSCUNG,VANTAICAU,VANTAICUNG,VIECLAMCAU,VIECLAMCUNG,TONG_THUE_BAO,
 TOTAL_BDSCAU,TOTAL_BDSCUNG,TOTAL_VANTAICAU,TOTAL_VANTAICUNG,TOTAL_VIECLAMCAU,TOTAL_VIECLAMCUNG, TONG_HUY,HUY_BDSCAU,HUY_BDSCUNG,HUY_VANTAICAU,
  HUY_VANTAICUNG,HUY_VIECLAMCAU,HUY_VIECLAMCUNG from 
   ( select 
            created_date,
            count(1) TONG_DANG_KY,
            sum(case when pkg_code = 'BDSCAU' then 1 else 0 end) as BDSCAU,
            sum(case when pkg_code = 'BDSCUNG' then 1 else 0 END) as BDSCUNG,
            sum(case when pkg_code = 'VANTAICAU' then 1 else 0 END) as VANTAICAU,
            sum(case when pkg_code = 'VANTAICUNG' then 1 else 0 END) as VANTAICUNG,
            sum(case when pkg_code = 'VIECLAMCAU' then 1 else 0 END) as VIECLAMCAU,
            sum(case when pkg_code = 'VIECLAMCUNG' then 1 else 0 END) as VIECLAMCUNG
        from his  where action in ('FIRST_REG','RE_REG') and created_date between $trackDate1 and $trackDate2 and cp='VASCLOUD'
        group by created_date) b
    left join 
   (select 
      $trackDate1 created_date,
     sum(total) TONG_THUE_BAO,
     SUM( case when pkg_code = 'TOTAL_BDSCAU' then total else 0 end) TOTAL_BDSCAU,
     SUM(case when pkg_code = 'TOTAL_BDSCUNG' then total else 0 end) TOTAL_BDSCUNG,
     SUM(case when pkg_code = 'TOTAL_VANTAICAU' then total else 0 end) TOTAL_VANTAICAU,
     SUM(case when pkg_code = 'TOTAL_VANTAICUNG' then total else 0 end) TOTAL_VANTAICUNG,
     SUM(case when pkg_code = 'TOTAL_VIECLAMCAU' then total else 0 end) TOTAL_VIECLAMCAU,
     SUM(case when pkg_code = 'TOTAL_VIECLAMCUNG' then total else 0 end) TOTAL_VIECLAMCUNG
   from (
   select  concat('TOTAL_',pkg_code) pkg_code, count (1)  total
   from subscriber where service_state =1 and to_char(reg_time,'YYYYMMDD') <= $trackDate1 group by  pkg_code)) a
   on a.created_date = b.created_date 
   left join 
   ( select 
            created_date,
            count(1) TONG_HUY,
            sum(case when pkg_code = 'BDSCAU' then 1 else 0 end) as HUY_BDSCAU,
            sum(case when pkg_code = 'BDSCUNG' then 1 else 0 END) as HUY_BDSCUNG,
            sum(case when pkg_code = 'VANTAICAU' then 1 else 0 END) as HUY_VANTAICAU,
            sum(case when pkg_code = 'VANTAICUNG' then 1 else 0 END) as HUY_VANTAICUNG,
            sum(case when pkg_code = 'VIECLAMCAU' then 1 else 0 END) as HUY_VIECLAMCAU,
            sum(case when pkg_code = 'VIECLAMCUNG' then 1 else 0 END) as HUY_VIECLAMCUNG
        from his  where action in ('UNREG') and created_date between $trackDate1 and $trackDate2
        group by created_date) c
    on b.created_date=c.created_date"));

        $dataTotal = [];

        $dataConvert = $dataTotal;
        foreach($data as $key => $value){
            $dataConvert['data2'][$key]['value'] = $value;
            if ($key == 0 ){
                $dataConvert['data2'][$key]['tong_thue_bao'] = $value->tong_thue_bao ;
                $dataConvert['data2'][$key]['total_bdscau'] = $value->total_bdscau ;
                $dataConvert['data2'][$key]['total_bdscung'] = $value->total_bdscung ;
                $dataConvert['data2'][$key]['total_vantaicau'] = $value->total_vantaicau ;
                $dataConvert['data2'][$key]['total_vantaicung'] = $value->total_vantaicung ;
                $dataConvert['data2'][$key]['total_vieclamcau'] = $value->total_vieclamcau ;
                $dataConvert['data2'][$key]['total_vieclamcung'] = $value->total_vieclamcung ;
            }else{
                $dataConvert['data2'][$key]['tong_thue_bao'] = $value->tong_dang_ky + $dataConvert['data2'][$key - 1]['tong_thue_bao'] - $value->tong_huy;
                $dataConvert['data2'][$key]['total_bdscau'] = $value->bdscau + $dataConvert['data2'][$key - 1]['total_bdscau'] - $value->huy_bdscau;
                $dataConvert['data2'][$key]['total_bdscung'] = $value->bdscung + $dataConvert['data2'][$key - 1]['total_bdscung'] - $value->huy_bdscung;
                $dataConvert['data2'][$key]['total_vantaicau'] = $value->vantaicau + $dataConvert['data2'][$key - 1]['total_vantaicau'] - $value->huy_vantaicau;
                $dataConvert['data2'][$key]['total_vantaicung'] = $value->vantaicung + $dataConvert['data2'][$key - 1]['total_vantaicung'] - $value->huy_vantaicung;
                $dataConvert['data2'][$key]['total_vieclamcau'] = $value->vieclamcau + $dataConvert['data2'][$key - 1]['total_vieclamcau'] - $value->huy_vieclamcau;
                $dataConvert['data2'][$key]['total_vieclamcung'] = $value->vieclamcung + $dataConvert['data2'][$key - 1]['total_vieclamcung'] - $value->huy_vieclamcung;
            }
        }
        return [
            'data' => $dataConvert,//$result->paginate($per_page),
        ];
    }

}
