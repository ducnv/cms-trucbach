<?php

namespace App\Http\Middleware;

use Cache;
use Session;
use Closure;
use App\User;
use Illuminate\Support\Facades\Route;

class PermissionDenied
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if (!$this->hasPermissions($request)) {
            if (!$request->ajax()) {
                return redirect()->back()->withErrors(['error' => 'You dont have permission']);
            }
            return response()->json(['error' => 401, 'message' => 'You dont have permission'], 401);
        }
        return $next($request);
    }

    public function hasPermissions($request)
    {
        $user_id = $request->ajax() ? $request->headers->get('AuthenticatedUser') : Session::get('user_id');

        if ($user_id == 1) {
            return true;
        }

        $controller = Route::getCurrentRoute()->action['controller'];
        list($classname, $method) = explode('@', $controller);
        $permission = annotation_reader($classname, $method);
        if (!$permission) {
            return true;
        }

        return has_permission($user_id, $permission);
    }
}
