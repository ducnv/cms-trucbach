/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
window.Vue = require("vue");
import vSelect from "vue-select";

// Register third-party component
Vue.component("v-select", vSelect);
require("./lib/vue-notify.js");

// ======= Manage User ======= //

Vue.component("manage-user", require("./components/User/ManageUsers.vue"));
Vue.component("manage-permission-user", require("./components/User/ManagePermission"));


// ======= End ======= //

// Carrer //

Vue.component("manage-media", require("./components/Media/ManageMedia"));

Vue.component("category", require("./components/Category/Manage"));

Vue.component("keywords", require("./components/Category/Keywords"));

Vue.component("create-news", require("./components/News/Create"));

Vue.component("edit-news", require("./components/News/Edit"));

Vue.component("manage-news", require("./components/News/Manage"));

Vue.component("create-pages", require("./components/Pages/Create"));

Vue.component("edit-pages", require("./components/Pages/Edit"));

Vue.component("manage-pages", require("./components/Pages/Manage"));

Vue.component("manage-setting", require("./components/Setting/Manage"));

//======  Quan tri noi dung ======//
Vue.component("manage-content", require("./components/Content/ManageContent"));
Vue.component("manage-messages", require("./components/Content/MessagesContent"));
Vue.component("thongkecauhoi", require("./components/Content/ThongKeCauHoi"));
Vue.component("manage-category", require("./components/Content/ManageCategory"));
Vue.component("manage-question", require("./components/Content/ManageQuestion"));
// ======  End ========//


//====== System ======//
Vue.component("search-sub", require("./components/System/SearchSub"));
Vue.component("log-ussd", require("./components/System/LogUssd"));
Vue.component("log-unregister", require("./components/System/LogUnregister"));
Vue.component("log-register", require("./components/System/LogRegister"));
Vue.component("log-mt", require("./components/System/LogMt"));
Vue.component("log-mo", require("./components/System/LogMo"));
Vue.component("log-charge", require("./components/System/LogCharge"));
Vue.component("black-list", require("./components/System/BlackList"));
Vue.component("config-invite", require("./components/System/ConfigInvite"));
//======End ========//


Vue.component("vue-tinymce", require("./components/Crawl/TinyMCE.vue"));
//
// Vue.component("site-client", require("./components/Crawl/SiteClient.vue"));
// Vue.component("add-category-siteclinet", require("./components/Crawl/AddCategorySiteClient.vue"));

// ======= End ======= //


import datePicker from 'vue-bootstrap-datetimepicker';
import 'bootstrap/dist/css/bootstrap.css';
import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css';
import 'vue-search-select/dist/VueSearchSelect.css'

Vue.use(datePicker);

Vue.component('range-datepickers', require('./components/DateRange.vue'));

// Register chart
import Chartkick from 'chartkick'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import VModal from 'vue-js-modal'

Vue.use(VModal)
Vue.use(VueChartkick, {Chartkick})

const app = new Vue({
    el: "#app"
});
