@extends('layouts.app')
@section('script')
    <style>
        .none{
            display: none;
        }
    </style>
    <link href="/bower_components/toastr/toastr.min.css" rel="stylesheet" type="text/css" />

    <script src="/bower_components/bootstrap-validator/bootstrapValidator.js"   ></script>
    <script src="/bower_components/jquery.blockUI.js" type="text/javascript"></script>
    <script src="/bower_components/toastr/toastr.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('#editPostSupply').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    // sdt: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: 'Bạn nhập số điện thoại'
                    //
                    //         },
                    //         numeric: {
                    //             message: 'Số điện thoại phải nhập bằng số'
                    //         },
                    //         blank: {}
                    //     }
                    // },
                    benefitPackage: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn gói dịch vụ'
                            }
                        }
                    },
                    exigency: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn nhu cầu'
                            },
                            blank: {}
                        }
                    },
                    classify: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn lĩnh vực'
                            },
                            blank: {}
                        }
                    },
                    title: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập tiêu đề'
                            },
                            stringLength: {
                                min: 6,
                                max: 100,
                                message: 'Tiêu đề phải từ 6 - 100 ký tự'
                            }
                        }
                    },
                    wage: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn mức lương'
                            }
                        }
                    },
                    price: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập giá tiền'
                            },
                            // regexp: {
                            //     regexp: /^[0-9.]+$/,
                            //     message: 'Giá tiền phải nhập bằng số'
                            // },
                            numeric: {
                                message: 'Giá tiền phải nhập bằng số'
                            },
                            stringLength: {
                                max: 12,
                                message: 'Giá tiền không vượt quá 12 số'
                            },
                            blank: {}

                        }
                    },
                    timeOut: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập thời hạn'
                            },
                            blank: {}
                        }
                    },
                    nip_province: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn thành phố'
                            }
                        }
                    },
                    nip_district: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn quận huyện'
                            },
                            blank: {}
                        }
                    },
                    nip_address: {
                        validators: {
                            // notEmpty: {
                            //     message: 'Bạn chưa nhập địa chỉ'
                            // },
                            blank: {}
                        }
                    },
                    content: {
                        validators: {
                            // notEmpty: {
                            //     message: 'Bạn chưa nhập nội dung'
                            // },
                            stringLength: {
                                max: 255,
                                message: 'Nội dung không vượt quá 255 ký tự'
                            },
                            blank: {}
                        }
                    },
                    checkY: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn đồng ý với nội dung tin đăng?'
                            },
                        }
                    },
                }
            })
                .on('mouseenter', '[name="exigency"],[name="classify"]', function (e) {
                    e.preventDefault();
                    $ckeckPackage = $('.ckeckPackage').val();

                    if ($ckeckPackage.length < 1) {
                        $('#editPostSupply').bootstrapValidator('validateField', 'benefitPackage');
                    }
                })
                .on('mouseenter', '[name="nip_district"]', function (e) {
                    e.preventDefault();
                    $ckeckProvince = $('.ckeckProvince').val();

                    if ($ckeckProvince.length < 1) {
                        $('#editPostSupply').bootstrapValidator('validateField', 'nip_province');
                    }
                })
                {{--.on('click', '.checkSDT', function (e) {--}}
                {{--    $.blockUI({--}}
                {{--        message: null,--}}
                {{--        overlayCSS: {backgroundColor: 'transparent'},--}}
                {{--        css: {--}}
                {{--            backgroundColor: 'transparent',--}}
                {{--            border: 'none',--}}
                {{--        }--}}
                {{--    });--}}
                {{--    e.preventDefault();--}}
                {{--    $sdt = $(".sdt").val();--}}

                {{--    $.ajax({--}}
                {{--        headers: {--}}
                {{--            'X-CSRF-TOKEN': $('[name="_token"]').val()--}}
                {{--        },--}}
                {{--        url: "{{route('profile.ckeckSDT')}}",--}}
                {{--        type: "POST",--}}
                {{--        data: {'sdt': $sdt},--}}
                {{--        cache: false,--}}
                {{--        complete: function () {--}}
                {{--            $.unblockUI();--}}
                {{--        },--}}
                {{--        success: function (e) {--}}
                {{--            // console.log(e.sdt)--}}

                {{--            if (e.error == true) {--}}
                {{--                $('#postSupply').bootstrapValidator('updateMessage', 'sdt', 'blank', 'SDT không hợp lệ').bootstrapValidator('updateStatus', 'sdt', 'INVALID', 'blank');--}}

                {{--            } else {--}}

                {{--                let $string = '';--}}
                {{--                $string += '<option value="">Chọn gói</option>';--}}
                {{--                $.each(e.sdt, function (key, value) {--}}
                {{--                    $string += '<option value="' + value['packages'].code + '">' + value['packages'].code + '</option>';--}}
                {{--                });--}}
                {{--                $(".ckeckPackage").html('').append($string);--}}


                {{--            }--}}

                {{--        }--}}
                {{--    });--}}
                {{--})--}}
                .on('change', '.ckeckPackage', function (e) {
                    $.blockUI({
                        message: null,
                        overlayCSS: {backgroundColor: 'transparent'},
                        css: {
                            backgroundColor: 'transparent',
                            border: 'none',
                        }
                    });
                    e.preventDefault();
                    $package = $(".ckeckPackage option:selected").val();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        url: "{{route('profile.checkPackage')}}",
                        type: "POST",
                        data: {'package': $package},
                        cache: false,
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (e) {
                            if (e.error == true) {
                                $(".exigency").html('<option value="">Chọn nhu cầu</option>')
                                $(".classify").html('<option value="">Chọn lĩnh vực</option>')
                            } else {
                                // nhu cau
                                let $string = '';
                                $string += '<option value="">Chọn nhu cầu</option>';
                                $.each(e.exigency, function (key, value) {
                                    $string += '<option value="' + value.id + '">' + value.name + '</option>';
                                });
                                $(".exigency").html('').append($string);

                                //linh vuc
                                let $string2 = '';
                                $string2 += '<option value="">Chọn lĩnh vực</option>';
                                $.each(e.classify, function (key2, value2) {
                                    $string2 += '<option value="' + value2.id + '">' + value2.name + '</option>';
                                });
                                $(".classify").html('').append($string2);

                                if (e.package == 'VIECLAMCUNG' || e.package == 'VIECLAMCAU'){
                                    $(".checkJobPrice").addClass('none').removeClass('block');
                                    $(".checkJobWage").removeClass('none').addClass('block');
                                }else{
                                    $(".checkJobWage").addClass('none').removeClass('block');
                                    $(".checkJobPrice").removeClass('none').addClass('block');
                                }
                            }
                            $('#editPostSupply').bootstrapValidator('updateMessage', 'exigency', 'blank', 'Vui lòng chọn đầy đủ thông tin').bootstrapValidator('updateStatus', 'exigency', 'INVALID', 'blank');
                            $('#editPostSupply').bootstrapValidator('updateMessage', 'classify', 'blank', 'Vui lòng chọn đầy đủ thông tin').bootstrapValidator('updateStatus', 'classify', 'INVALID', 'blank');


                        }
                    });
                })
                .on('change', '.ckeckProvince', function (e) {
                    $.blockUI({
                        message: null,
                        overlayCSS: {backgroundColor: 'transparent'},
                        css: {
                            backgroundColor: 'transparent',
                            border: 'none',
                        }
                    });
                    e.preventDefault();
                    $province = $(".ckeckProvince option:selected").val();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        url: "{{route('profile.ckeckProvince')}}",
                        type: "POST",
                        data: {'province': $province},
                        cache: false,
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (e) {
                            if (e.error == true) {
                                $(".nip_district").html('<option value="">Chọn quận huyện</option>')
                            } else {
                                // nhu cau
                                let $string = '';
                                $string += '<option value="">Chọn quận huyện</option>';
                                $.each(e.district, function (key, value) {
                                    $string += '<option value="' + value.id + '">' + value.name + '</option>';
                                });
                                $(".nip_district").html('').append($string);

                            }
                            $('#editPostSupply').bootstrapValidator('updateMessage', 'nip_district', 'blank', 'Vui lòng chọn đầy đủ thông tin').bootstrapValidator('updateStatus', 'nip_district', 'INVALID', 'blank');


                        }
                    });
                })
                .on('change', '.date', function () {
                    $date = $(".date").val();
                    $timeNow = '{{\Carbon\Carbon::now()->toDateString()}}';
                    if($date < $timeNow ){
                        $('#postSupply').bootstrapValidator('updateMessage', 'timeOut', 'blank', 'Thời hạn phải lớn hơn ngày hiện tại').bootstrapValidator('updateStatus', 'timeOut', 'INVALID', 'blank');
                    }
                })
                .on('keyup change', '.checkJobPrice', function (e) {

                    e.preventDefault();
                    $checkPrice = $(".checkPrice").val();
                    $denominations = $(".denomination option:selected").val();

                    //format price
                    var v = Number($checkPrice);
                    if (isNaN(v)) {
                        return $checkPrice;
                    }
                    var sign = (v < 0) ? '-' : '';
                    var res = Math.abs(v).toString().split('').reverse().join('').replace(/(\d{3}(?!$))/g, '$1,').split('').reverse().join('');
                    $price = sign + res;
                    //end

                    $(".totalPrice").html('').append($price +' '+ $denominations);

                    if(parseInt($checkPrice) == 0){
                        $('#postSupply').bootstrapValidator('updateMessage', 'price', 'blank', 'Bạn chưa nhập giá tiền').bootstrapValidator('updateStatus', 'price', 'INVALID', 'blank');
                    }

                });
        });
    </script>
    @include('layouts.message')

@endsection
@section('content')
    <section class="sectionSame">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="boxForm">
                        <div class="boxForm-title" style="text-align: center">
                            <h3>Cập nhât tin đăng</h3>
                        </div>
                        <p style="color: #fff; margin-bottom: 15px;margin-left: 55px;" class="btn btn-primary">Mã tin : <span style="color: #fff">{{$post->post_code}}</span></p>
                        <form id="editPostSupply" class="form-horizontal" action=""
                              method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" class="form-control sdt hidden" name="sdt" value="{{$post->msisdn}}"
                                   placeholder="Nhập số điện thoại"
                                   autocomplete="off">
                            <div class="box-body clearfix">
                                <div class="col-m-12">
{{--                                    <div class="form-group selectContainer">--}}

{{--                                        <label for="" class="col-sm-3 control-label"> Số điện thoại <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>--}}
{{--                                        <div class="col-sm-7">--}}

{{--                                            <p>Lưu ý: SĐT bao gồm cả các gói hết hạn</p>--}}

{{--                                        </div>--}}
{{--                                        <div class="col-md-2">--}}
{{--                                            <a  class="btn btn-success checkSDT">Check SDT</a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="form-group selectContainer">
                                        <label class="col-sm-3 control-label">Gói dịch vụ <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control ckeckPackage " name="benefitPackage">
                                                <option value="">Chọn gói</option>
                                                @foreach($package as $key => $value)
                                                    <option {{$post->pkg_code == $value['packages']->code ? "selected" : ''}}  value="{{$value['packages']->code}}">{{Config::get('package.namePackageView.'.$value['packages']->code)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group selectContainer">
                                        <label class="col-sm-3 control-label">Nhu cầu <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control exigency" name="exigency">
                                                <option value="">Chọn nhu cầu</option>
                                                @foreach($showMeta['exigency'] as $key => $value)
                                                <option value="{{$value->id}}" {{$valueRelationships[Config::get('terms.' . $post->pkg_code . '.exigency')] == $value->id ? "selected" : ''}}>
                                                    {{$value->name}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group selectContainer">
                                        <label class="col-sm-3 control-label">Lĩnh vực/ Phân loại <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control classify" name="classify">
                                                <option value="">Chọn lĩnh vực</option>
                                                @foreach($showMeta['classify'] as $key => $value)
                                                    <option value="{{$value->id}}" {{$valueRelationships[Config::get('terms.' . $post->pkg_code . '.classify')] == $value->id ? "selected" : ''}}>
                                                        {{$value->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Tiêu đề <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="title" value="{{$post->post_title}}"
                                                   placeholder="VD: Tuyển lao động phổ thông tại Bình Thuận"
                                                   autocomplete="off">
                                        </div>
                                    </div>
                                    @if(!empty($valueRelationships['nip_wage']))
                                        <div class="form-group checkJobWage ">
                                            <label for="" class="col-sm-3 control-label">Mức lương <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                            <div class="col-sm-9">
                                                <select class="form-control " name="wage">
                                                    <option value="">Chọn mức lương</option>
                                                    @foreach($wage as $key => $value)
                                                        <option value="{{$value->id}}" {{$valueRelationships['nip_wage'] == $value->id? "selected" :''}}>{{$value->name}} VND</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group checkJobPrice none">

                                            <label for="" class="col-sm-3 control-label">Giá <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                            <div class="col-sm-9">
                                                <div class="row">
                                                    <div class="col-xs-8" style="padding-right: 0">
                                                        <input type="text" class="form-control price checkPrice" name="price" placeholder="VD: 6.000.000" value=""
                                                               autocomplete="off">
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <select class="form-control denomination" name="denominations" style="padding-right: 0px">
                                                            <option selected value="Đồng">Đồng</option>
                                                            <option  value="Triệu">Triệu</option>
                                                            <option value="Tỷ">Tỷ</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <p style="color: #1c7430;margin-top: 3px;">Tổng giá tiền: <span class="totalPrice">0 Triệu</span></p>
                                            </div>
                                        </div>
                                    @else
                                        <div class="form-group checkJobWage none">
                                            <label for="" class="col-sm-3 control-label">Mức lương <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                            <div class="col-sm-9">
                                                <select class="form-control " name="wage">
                                                    <option value="">Chọn mức lương</option>
                                                    @foreach($wage as $key => $value)
                                                        <option value="{{$value->id}}">{{$value->name}} VND</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group checkJobPrice ">

                                            <label for="" class="col-sm-3 control-label">Giá <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                            <div class="col-sm-9">
                                                <div class="row">
                                                    <div class="col-xs-8" style="padding-right: 0">
                                                        <input type="text" class="form-control price checkPrice" name="price" placeholder="VD: 6.000.000" value="{{$price}}"
                                                               autocomplete="off">
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <select class="form-control denomination" name="denominations" style="padding-right: 0px">
                                                            <option selected value="Đồng">Đồng</option>
                                                            <option  value="Triệu">Triệu</option>
                                                            <option value="Tỷ">Tỷ</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <p style="color: #1c7430;margin-top: 3px;">Tổng giá tiền: <span class="totalPrice">{{$price}} Đồng</span></p>
                                            </div>
                                        </div>
                                        {{--<div class="form-group checkJobPrice">--}}
                                            {{--<label for="" class="col-sm-3 control-label">Giá :</label>--}}
                                            {{--<div class="col-sm-9">--}}
                                                {{--<input type="text" class="form-control price" name="price" value="{{response()->formatPrice($price)}}"--}}
                                                       {{--placeholder="VD: 6.000.000" autocomplete="off">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    @endif

                                    <div class="form-group">

                                        <label for="" class="col-sm-3 control-label"> Thời hạn <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control date" name="timeOut" value="{{date("Y-m-d", strtotime($post->expired_time))}}"
                                                   placeholder="" autocomplete="off" style="width: 100%">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Thành phố <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control ckeckProvince" name="nip_province">
                                                <option value="">Chọn thành phố</option>
                                                @foreach($province as $key => $value)
                                                    <option value="{{$value->id}}" {{$valueRelationships['nip_province'] == $value->id? "selected" :''}}>{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Quận huyện <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control nip_district" name="nip_district">
                                                <option value="">Chọn quận huyện</option>
                                                @foreach($district as $key => $value)
                                                    <option value="{{$value->id}}" {{$valueRelationships['nip_district'] == $value->id? "selected" :''}}>{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Địa chỉ liên lạc :</label>
                                        <div class="col-sm-9">
                                            <textarea rows="4" cols="50" class="form-control" name="nip_address">{{!empty($postMeta['nip_post_address'])?$postMeta['nip_post_address']:''}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Nội dung :</label>
                                        <div class="col-sm-9">
                                            <textarea rows="4" cols="50" class="form-control" name="content">{{!empty($post->post_content)?$post->post_content:''}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9" style="margin-top: 5px;">
                                            <input style="position: relative;top: 1px" type="checkbox" name="checkY" checked /> Tôi đồng ý và chịu trách nhiệm hoàn toàn với nội dung tin đăng.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!-- /.box-body -->
                            <div class="box-footer text-center" style="background: transparent;">
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection