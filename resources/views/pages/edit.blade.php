@extends('layouts.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <edit-pages ref="createNews"></edit-pages>
    </section>
@endsection

@push('json_var')
    window.pages = {!! json_encode($pages) !!}
@endpush