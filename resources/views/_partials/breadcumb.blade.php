@if(isset($breadcumb))
    <section class="content-header">
        <h1>
            {{ $breadcumbMain }}
            <small>{{ $title }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> {{ __('home.dashboard') }}</a></li>
            @foreach($breadcumb as $item)
                <li class="breadcrumb-item">
                    @if($item['url'] != '#')
                        <a href="{{ $item['url'] }}">{{ $item['label'] }}</a>
                    @else
                        {{ $item['label'] }}
                    @endif
                </li>
            @endforeach
        </ol>
    </section>
@endif