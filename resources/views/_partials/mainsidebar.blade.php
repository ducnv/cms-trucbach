@php
    $sidebar = collect(parse_package_config('menu'));
    $sidebar = $sidebar->sortByDesc('group')->groupBy('group');
@endphp
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="/">
                    <i class="fa fa-dashboard"></i> <span>{{ __('home.dashboard') }}</span>
                </a>
            </li>
            @foreach($sidebar as $group => $menu)
                @foreach($menu as $nav)
                    <li class="@if($nav['type'] == 'dropdown') treeview @endif @if(Request::is($nav['active']) || strpos(Request::fullUrl(),$nav['url']) !== false) menu-open @endif">
                        <a href="javascript:void(0);">
                            <i class="{{ $nav['icon'] }}"></i>
                            <span>{{$nav['label']}}</span>
                            <span class="pull-right-container">
                                @if($nav['type'] == 'dropdown')
                                    <i class="fa fa-angle-left pull-right"></i>
                                @endif
                        </span>
                        </a>
                        @if($nav['type'] == 'dropdown')
                            <ul class="treeview-menu"
                                style="@if(Request::is($nav['active']) || strpos(Request::fullUrl(),$nav['url']) !== false) display: block; @endif">
                                @foreach($nav['child'] as $child)
                                    <li class="@if(strpos(Request::fullUrl(),$child['url']) !== false) active @endif">
                                        <a href="{{ $child['url'] }}"><i
                                                    class="fa fa-angle-double-right"></i>
                                            {{ __($child['label']) }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            @endforeach

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>