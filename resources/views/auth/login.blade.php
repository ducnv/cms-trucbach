@extends('layouts.auth')

@section('content')
<div class="login-box">
  <div class="login-logo">
    <a href="/"><b>TrucBach</b>Jsc</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">{{ trans('auth.login_msg') }}</p>

    <form method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
        <input type="email" name="email" class="form-control" placeholder="{{ trans('auth.email') }}" value="{{ old('email') }}" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
        <input type="password" name="password" class="form-control" placeholder="{{ trans('auth.password') }}" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ trans('auth.remember_me') }}
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('auth.login') }}</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <!-- <div class="social-auth-links text-center">
      <p>- HOẶC -</p>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> {{ trans('auth.login_with_google') }}</a>
    </div> -->
    <!-- /.social-auth-links -->

    <a href="{{ route('password.request') }}">{{ trans('auth.forgot_password') }}</a><br>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@endsection
