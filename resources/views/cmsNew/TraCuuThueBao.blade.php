<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>GUI - Tra cứu thuê bao</title>
    <link media="screen" rel="stylesheet" href="/cmsNew/css/reset.css"/>
    <link media="screen" rel="stylesheet" href="/cmsNew/css/style.css"/>
    <script language='javascript' src='/cmsNew/js/jquery-1.7.1.min.js'></script>
    <script type="text/javascript" src="/cmsNew/js/jtabber.js"></script>
</head>
<body onload="mymenupopup()">
<div class='contain_popup'>
    <div class='p8'>
        <div class='title_containpopup'>
            <h1>GUI - SanLocVang</h1>
        </div>
        <div class='containdatapopup'>
            <div class='supportarea'>
                <li class='labelsp'>Đầu mối giải quyết khiếu nại dịch vụ: </li>
                <li class='username'>Nguyễn Thế Hải</li>
                <li class='email'>haint@vienthongtrucbach.vn</li>
                <li class='mobile'>0888182238</li>
                <li style="float: left"><a href="/dang-xuat">Đăng xuất</a></li>
            </div>
            <div id="menutabs1" class='mt10'>
                <a class="selected" href="{{route('traCuuThueBao')}}"><img class='icon1' src='/cmsNew/images/icon1.png'><span>Tra cứu thuê bao</span></a>
                <a class="" href="{{route('lichSuDangKyHuy')}}"><img class='icon2' src='/cmsNew/images/icon2.png'><span>Tra cứu sử dụng dịch vụ</span></a>
                <a class="" href="{{route('thongTinDichVu')}}"><img class='icon4'
                                                                                    src='/cmsNew/images/icon4.png'><span>Thông tin dịch vụ</span></a>

                {{--<a class="" href="GUI - Cài đặt dịch vụ.html"><img class='icon3' src='/cmsNew/images/icon3.png'><span>Cài đặt dịch vụ</span></a>--}}
                {{--<a class="" href="GUI%20-%20Thông%20tin%20dịch%20vụ.blade.php"><img class='icon4' src='/cmsNew/images/icon4.png'><span>FAQ</span></a>--}}
                {{--<a class="" href="GUI%20-%20Thông%20tin%20dịch%20vụ.blade.php"><img class='icon4' src='/cmsNew/images/icon4.png'><span>Thông tin liên hệ</span></a>--}}
            </div>
            <div class='fillterarea'>
                <table>
                    <tr>
                        <form action="">
                            <td width='100'>Số thuê bao:</td>
                            <td><input type='text' name="msisdn" class='textbox' value="{{Session::has('sdt')?Session::get('sdt'):Request::get('msisdn')}}"></td>
                            <td align='right' width='200'>
                                <input type="submit" value="Tra cứu" class="btn_search">
                            </td>
                            </td>
                        </form>
                    </tr>
                </table>
            </div>
            <table class='tbl_style'>
                <thead>
                <tr>
                    <th>Dịch vụ</th>
                    <th>Gói cước</th>
                    <th>Trạng thái</th>
                    <th>Ngày đăng ký</th>
                    <th>Ngày hủy</th>
                    <th>Hiệu lực gói cước</th>
                    <th>Kênh đăng ký/hủy</th>
                    <th>Cước phí</th>
                </tr>
                </thead>
                @if(isset($sub))
                    @foreach($sub as $item)
                        <tr>
                            <td align='center'><span>SAN LOC VANG</span></td>
                            <td align='center'><span>{{$item->pkg_code}}</span></td>
                            <td><span>{{service_state($item->service_state)}}</span></td>
                            <td align='center'><span>{{$item->reg_time}}</span></td>
                            <td align='center'><span>{{showTimeUnreg($item->service_state,$item->unreg_time)}}</span>
                            </td>
                            <td align='center'><span>{{$item->expired_time}}</span></td>
                            <td align='center'><span>{{$item->channel}} / {{$item->unreg_channel}} </span></td>
                            <td align='center'><span>{{find_packages($item->pkg_code)}}</span></td>
                        </tr>
                    @endforeach
                @endif
            </table>

                @if(isset($sub))
                        <div class="clearfix">{{ $sub->appends($appends)->links() }}</div>
                @endif


            {{--<div class='paginator'>--}}

                {{--<ul>--}}
                    {{--<li><a href='#1'>3</a></li>--}}
                    {{--<li><a href='#1'>2</a></li>--}}
                    {{--<li><a href='#1' class='active'>1</a></li>--}}
                    {{--<li>Trang:</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
</body>
</html>
