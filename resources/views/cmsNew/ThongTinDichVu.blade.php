<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GUI - Thông tin dịch vụ</title>
<link media="screen" rel="stylesheet" href="/cmsNew/css/reset.css" />
<link media="screen" rel="stylesheet" href="/cmsNew/css/style.css" />
<script language='javascript' src='/cmsNew/js/jquery-1.7.1.min.js'></script>
<script type="text/javascript" src="/cmsNew/js/jtabber.js"></script>
</head>
<body onload="mymenupopup()">
<div class='contain_popup'>
	<div class='p8'>
		<div class='title_containpopup'>
            <h1>GUI - SanLocVang</h1>
		</div>
		<div class='containdatapopup'>
            <div class='supportarea'>
                <li class='labelsp'>Đầu mối giải quyết khiếu nại dịch vụ: </li>
                <li class='username'>Nguyễn Thế Hải</li>
                <li class='email'>haint@vienthongtrucbach.vn</li>
                <li class='mobile'>0888182238</li>
				<li style="float: left"><a href="/dang-xuat">Đăng xuất</a></li>
            </div>
			<div id="menutabs1" class='mt10'>
				<a class="" href="{{route('traCuuThueBao')}}"><img class='icon1' src='/cmsNew/images/icon1.png'><span>Tra cứu thuê bao</span></a>
				<a class="" href="{{route('lichSuDangKyHuy')}}"><img class='icon2' src='/cmsNew/images/icon2.png'><span>Tra cứu sử dụng dịch vụ</span></a>
				<a class="selected" href="{{route('thongTinDichVu')}}"><img class='icon4' src='/cmsNew/images/icon4.png'><span>Thông tin dịch vụ</span></a>

				{{--<a class="" href="GUI - Cài đặt dịch vụ.html"><img class='icon3' src='/cmsNew/images/icon3.png'><span>Cài đặt dịch vụ</span></a>--}}
				{{--<a class="" href="GUI%20-%20Thông%20tin%20dịch%20vụ.blade.php"><img class='icon4' src='/cmsNew/images/icon4.png'><span>FAQ</span></a>--}}
				{{--<a class="" href="GUI%20-%20Thông%20tin%20dịch%20vụ.blade.php"><img class='icon4' src='/cmsNew/images/icon4.png'><span>Thông tin liên hệ</span></a>--}}
			</div>
		</div>
	</div>
</div>
</body>
</html>
