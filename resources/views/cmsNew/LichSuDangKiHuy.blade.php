<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>GUI - Tra cứu sử dụng dịch vụ</title>
    <link media="screen" rel="stylesheet" href="/cmsNew/css/reset.css"/>
    <link media="screen" rel="stylesheet" href="/cmsNew/css/style.css"/>
    <script language='javascript' src='/cmsNew/js/jquery-1.7.1.min.js'></script>
    <script type="text/javascript" src="/cmsNew/js/jtabber.js"></script>
</head>
<body onload="mymenupopup()">
<div class='contain_popup'>
    <div class='p8'>
        <div class='title_containpopup'>
            <h1>GUI - SanLocVang</h1>
        </div>
        <div class='containdatapopup'>
            <div class='supportarea'>
                <li class='labelsp'>Đầu mối giải quyết khiếu nại dịch vụ:</li>
                <li class='username'>Nguyễn Thế Hải</li>
                <li class='email'>haint@vienthongtrucbach.vn</li>
                <li class='mobile'>0888182238</li>
                <li style="float: left"><a href="/dang-xuat">Đăng xuất</a></li>
            </div>
            <div id="menutabs1" class='mt10'>
                <a class="" href="{{route('traCuuThueBao')}}"><img class='icon1' src='/cmsNew/images/icon1.png'><span>Tra cứu thuê bao</span></a>
                <a class="selected" href="{{route('lichSuDangKyHuy')}}"><img class='icon2'
                                                                             src='/cmsNew/images/icon2.png'><span>Tra cứu sử dụng dịch vụ</span></a>
                <a class="" href="{{route('thongTinDichVu')}}"><img class='icon4' src='/cmsNew/images/icon4.png'><span>Thông tin dịch vụ</span></a>

                {{--<a class="" href="GUI - Cài đặt dịch vụ.html"><img class='icon3' src='/cmsNew/images/icon3.png'><span>Cài đặt dịch vụ</span></a>--}}
                {{--<a class="" href="GUI%20-%20Thông%20tin%20dịch%20vụ.blade.php"><img class='icon4' src='/cmsNew/images/icon4.png'><span>FAQ</span></a>--}}
                {{--<a class="" href="GUI%20-%20Thông%20tin%20dịch%20vụ.blade.php"><img class='icon4' src='/cmsNew/images/icon4.png'><span>Thông tin liên hệ</span></a>--}}
            </div>
            <div class='tabtracuusddv'>
                <li><a href='{{route('lichSuDangKyHuy')}}' class='select'>Lịch sử Đăng ký / Hủy</a></li>
                <li><a href='{{route('lichSuTruCuoc')}}'>Lịch sử trừ cước</a></li>
                {{--<li><a href='{{route('lichSuSuDungNguonCau')}}'>Lịch sử sử dụng nguồn cầu</a></li>--}}
                <li><a href='/cskh/thoi-gian-giu-loc'>Thời gian giữ lộc</a></li>
                <li><a href='/cskh/thue-bao-giu-loc-hien-tai'>Thuê bao giữ lộc</a></li>
                <li><a href='{{route('lichSuMoMt')}}'>Lịch sử MO /MT</a></li>
            </div>
            <div class='fillterarea'>
                <form action="" method="GET">
                    <table>
                        <tr>
                            <td width='100'>Số thuê bao:</td>
                            <td colspan=5><input style='width:147px' type='number' class='textbox' name="isdn"
                                                 value="{{Session::has('sdt')?Session::get('sdt'):Request::get('isdn')}}">
                            </td>
                            <td rowspan='2' align='right' width='200'>
                                <input type="submit" value="Tra cứu" class="btn_search">
                            </td>
                        </tr>
                        <tr>
                            <td>Gói cước:</td>
                            <td>
                                <select style='width:150px' class="dropdownlist" name="pkg_code">
                                    <option value="">-- Chọn gói cước --</option>
                                    @foreach($package as $key => $value)
                                        <option {{Request::get('pkg_code') == $value->value?'selected':''}}  value="{{$value->value}}">{{$value->text}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>Thời gian bắt đầu:</td>
                            <td><input type="date" class="datetime" name="start_date"
                                       value="{{Request::has('start_date')?Request::get('start_date'):\Carbon\Carbon::now()->subDay(6)->format('Y-m-d')}}">
                            </td>
                            <td>Thời gian kết thúc</td>
                            <td><input type="date" class="datetime" name="end_date"
                                       value="{{Request::has('end_date')?Request::get('end_date'):\Carbon\Carbon::now()->format('Y-m-d')}}">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <table class='tbl_style'>
                <thead>
                <tr>
                    <th>Thời gian giao dịch</th>
                    <th>Loại giao dịch</th>
                    <th>Gói cước</th>
                    <th>Tình trạng</th>
                    <th>Kênh</th>
                    {{--<th>Ứng dụng/Sự kiện</th>--}}
                    {{--<th>Tài khoản/CT</th>--}}
                    {{--<th>User IP</th>--}}
                </tr>
                </thead>
                @if(isset($data))
                    @foreach($data as $key => $value)
                        <tr>
                            <td align='center'><span>{{$value->created_time}} </span></td>
                            <td align='center'><span>{{convert_vi($value->action)}}</span></td>
                            <td align='center'><span>{{$value->pkg_code}}</span></td>
                            <td align='center'><span>{{(int)$value->result === 0 ? 'Thành công' : 'Thất bại'}}</span>
                            </td>
                            <td align='center'><span>{{$value->channel}}</span></td>
                            {{--<td></td>--}}
                            {{--<td align='center'><span></span></td>--}}
                            {{--<td></td>--}}
                        </tr>
                    @endforeach
                @endif

            </table>
            @if(isset($data))
                <div class="clearfix">{{ $data->appends($appends)->links() }}</div>
            @endif
        </div>
    </div>
</div>
</body>
</html>

