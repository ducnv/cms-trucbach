@extends('layouts.app')
@section('script')
    <link href="/bower_components/toastr/toastr.min.css" rel="stylesheet" type="text/css" />

    <script src="/bower_components/bootstrap-validator/bootstrapValidator.js"   ></script>
    <script src="/bower_components/jquery.blockUI.js" type="text/javascript"></script>
    <script src="/bower_components/toastr/toastr.min.js" type="text/javascript"></script>
    <script>

        $(document).ready(function () {
            $('#profileSupply').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    sdt: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn nhập số điện thoại'

                            },
                            numeric: {
                                message: 'Số điện thoại phải nhập bằng số'
                            },
                            blank: {}
                        }
                    },
                    nip_name: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập Họ và tên/ Tên công ty'
                            },
                            stringLength: {
                                min: 6,
                                message: 'Tên phải lớn hơn 6 ký tự'
                            }
                        }
                    },
                    nip_email: {
                        validators: {
                            emailAddress: {
                                message: 'Địa chỉ email không hợp lệ'
                            },
                            blank: {}
                        }
                    },
                    nip_address: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập địa chỉ liên lạc'
                            },
                        }
                    },
                }
            })
                .on('change', '.checkSDT', function (e) {
                    $.blockUI({
                        message: null,
                        overlayCSS: {backgroundColor: 'transparent'},
                        css: {
                            backgroundColor: 'transparent',
                            border: 'none',
                        }
                    });
                    e.preventDefault();
                    $sdt = $(".sdt").val();

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        url: "{{route('profile.ckeckHSSDTSupply')}}",
                        type: "POST",
                        data: {'sdt': $sdt},
                        cache: false,
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (e) {
                            console.log(e)

                            if (e.error == true) {
                                $('#profileSupply').bootstrapValidator('updateMessage', 'sdt', 'blank', 'SDT không hợp lệ').bootstrapValidator('updateStatus', 'sdt', 'INVALID', 'blank');

                            }

                        }
                    });
                })
        });
    </script>
    @include('layouts.message')

@endsection
@section('content')

    <section class="sectionSame">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="boxForm">
                        <p style="margin-top: 20px">Số điện thoại không hợp lệ khi : <br>
                            + Đã tồn tại hồ sơ<br>
                            + Chưa đăng kí dịch vụ<br>
                            + Đã hủy dịch vụ</p>
                        <div class="boxForm-title" style="text-align: center">
                            <h3>Hồ sơ nguồn cung</h3>

                        </div>
                        <form id="profileSupply" class="form-horizontal" action="{{route('profile.profileSupply')}}"
                              method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body clearfix">
                                <div class="col-m-12">

                                    <div class="form-group selectContainer">

                                        <label for="" class="col-sm-3 control-label"> Số điện thoại <span style="color: red">(<span style="position: relative;top: 4px;">*</span>)</span> :</label>
                                        <div class="col-sm-9">

                                            <input type="text" class="form-control sdt checkSDT" name="sdt" value=""
                                                   placeholder="Nhập số điện thoại"
                                                   autocomplete="off" >
                                            <p>Lưu ý: SĐT bao gồm cả các gói hết hạn</p>

                                        </div>
{{--                                        <div class="col-md-2">--}}
{{--                                            <a  class="btn btn-success checkSDT">Check SDT</a>--}}
{{--                                        </div>--}}
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Họ và tên/Tên công ty : </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nip_name" value=""
                                                   placeholder="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Email :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nip_email" value=""
                                                   placeholder="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Địa chỉ liên lạc : </label>
                                        <div class="col-sm-9">
                                            <textarea rows="4" cols="50" class="form-control" name="nip_address"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!-- /.box-body -->
                            <div class="box-footer text-center" style="background: transparent">
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection