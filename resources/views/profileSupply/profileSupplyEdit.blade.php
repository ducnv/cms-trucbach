@extends('layouts.app')
@section('script')
    <link href="/bower_components/toastr/toastr.min.css" rel="stylesheet" type="text/css" />

    <script src="/bower_components/bootstrap-validator/bootstrapValidator.js"   ></script>
    <script src="/bower_components/jquery.blockUI.js" type="text/javascript"></script>
    <script src="/bower_components/toastr/toastr.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('#profileSupply').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    nip_name: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập Họ và tên/ Tên công ty'
                            },
                            stringLength: {
                                min: 6,
                                message: 'Tên phải lớn hơn 6 ký tự'
                            }
                        }
                    },
                    nip_email: {
                        validators: {
                            emailAddress: {
                                message: 'Địa chỉ email không hợp lệ'
                            },
                            blank: {}
                        }
                    },
                    nip_address: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập địa chỉ liên lạc'
                            },
                        }
                    },
                }
            });
        });
    </script>
    @include('layouts.message')

@endsection
@section('content')
    @if(Session::has('status'))

        <div class="modal fade" id="checkStatus" role="dialog">
            <div class="modal-dialog " style="margin-top: 100px">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="padding-right: 30px;padding-left: 30px;text-align: center">
                        {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                        <h4 class="modal-title">Thông báo</h4>
                    </div>
                    <div class="modal-body" style="padding-right: 50px;padding-left: 50px;text-align: center">
                        <p>Cập nhật Hồ sơ thành công. Mời QK truy cập <a href="{{route('post.getAddPost')}}" style="color: #337ab7;text-decoration: underline;">{{route('post.getAddPost')}}</a> để đăng tin nhanh nhất.</p>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                    </div>
                </div>

            </div>
        </div>
    @endif
    <section class="sectionSame">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="boxForm">
                        <p style="margin-top: 20px">Số điện thoại không hợp lệ khi : <br>
                            + Đã tồn tại hồ sơ<br>
                            + Chưa đăng kí dịch vụ<br>
                            + Đã hủy dịch vụ</p>
                        <div class="boxForm-title" style="text-align: center">
                            <h3>Hồ sơ nguồn cung</h3>
                        </div>
                        <form id="profileSupply" class="form-horizontal" action="{{route('profile.profileSupplyEdit')}}"
                              method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="sdt" value="{{$sdt}}">

                            <div class="box-body clearfix">
                                <div class="col-m-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Họ và tên/Tên công ty : </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nip_name" value="{{$profile['_nip_name']}}"
                                                   placeholder="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Email :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nip_email" value="{{$profile['_nip_email']}}"
                                                   placeholder="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Địa chỉ liên lạc : </label>
                                        <div class="col-sm-9">
                                            <textarea rows="4" cols="50" class="form-control" name="nip_address">{{$profile['_nip_address']}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!-- /.box-body -->
                            <div class="box-footer text-center" style="background: transparent">
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection