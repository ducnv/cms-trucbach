@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        {{ __('user.user') }}
        <small>{{ __('user.manage_user') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> {{ __('home.dashboard') }}</a></li>
        <li class="active">{{ __('home.setting') }}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-plus-square"></i> {{ __('user.create_user') }}</h3>
                </div>
                <div class="box-body">
                    <create-or-update-user :roles="{{ $roles }}"></create-or-update-user>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-address-book"></i> {{ __('user.browse_user') }}</h3>
                </div>
                <div class="box-body">
                    <manage-user :roles="{{ $roles }}"></manage-user>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection