@extends('layouts.app')

@section('content')
<!-- Main content -->
<section class="content">
    <manage-question></manage-question>
</section>
@endsection
@push('json_var')
    window.timeoutlevel = {!! json_encode($level) !!}
@endpush