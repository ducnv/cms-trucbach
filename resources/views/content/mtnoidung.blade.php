@extends('layouts.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <manage-content></manage-content>
    </section>
@endsection