@extends('layouts.app')
@section('script')
    <link href="/bower_components/toastr/toastr.min.css" rel="stylesheet" type="text/css" />

    <script src="/bower_components/bootstrap-validator/bootstrapValidator.js"   ></script>
    <script src="/bower_components/jquery.blockUI.js" type="text/javascript"></script>
    <script src="/bower_components/toastr/toastr.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('#profileDemandSources').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    nip_name: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập Họ và tên/ Tên công ty'
                            },
                            stringLength: {
                                min: 6,
                                message: 'Tên phải lớn hơn 6 ký tự'
                            }
                        }
                    },
                    nip_address: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập địa chỉ liên lạc'
                            },
                        }
                    },
                    benefitPackage: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn gói dịch vụ'
                            }
                        }
                    },
                    exigency: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn nhu cầu'
                            },
                            blank: {}
                        }
                    },
                    classify: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn lĩnh vực'
                            },
                            blank: {}
                        }
                    },
                    wage: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa chọn mức lương'
                            }
                        }
                    },
                    price: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập giá tiền'
                            },
                            // regexp: {
                            //     regexp: /^[0-9.]+$/,
                            //     message: 'Giá tiền phải nhập bằng số'
                            // },
                            numeric: {
                                message: 'Giá tiền phải nhập bằng số'
                            },
                            stringLength: {
                                max: 12,
                                message: 'Giá tiền không vượt quá 12 số'
                            },
                            blank: {}

                        }
                    },
                    nip_province: {
                        validators: {
                            notEmpty: {
                                message: 'Bạn chưa nhập địa điểm tìm kiếm'
                            },
                            blank: {}
                        }
                    },
                }
            })
            .on('mouseenter', '[name="exigency"],[name="classify"]', function (e) {
                e.preventDefault();
                $ckeckPackage = $('.ckeckPackage').val();

                if ($ckeckPackage.length < 1) {
                    $('#profileDemandSources').bootstrapValidator('validateField', 'benefitPackage');
                }
            })
            .on('change', '.ckeckPackage', function (e) {
                $.blockUI({
                    // message: $('#tallContent'),
                    message: null,
                    overlayCSS: { backgroundColor: 'transparent' },
                    css: {
                        backgroundColor: 'transparent',
                        border: 'none',
                        // top:  ($(window).height() - 70) /2 + 'px',
                        // left: ($(window).width() - 70) /2 + 'px',
                        // width: '70px',
                    }
                });
                e.preventDefault();
                $package = $( ".ckeckPackage option:selected" ).val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    url:"{{route('profile.checkPackage')}}",
                    type:"POST",
                    data:{'package':$package},
                    cache: false,
                    complete: function() {
                        $.unblockUI();
                    },
                    success:function(e){
                        if(e.error == true){
                            $(".exigency").html('<option value="">Chọn nhu cầu</option>')
                            $(".classify").html('<option value="">Chọn lĩnh vực</option>')
                        }else{
                            // nhu cau
                            let $string = '';
                            $string += '<option value="">Chọn nhu cầu</option>';
                            $.each(e.exigency , function (key, value){
                                $string += '<option value="'+value.id+'">'+value.name+'</option>';
                            });
                            $(".exigency").html('').append($string);

                            //linh vuc
                            let $string2 = '';
                            $string2 += '<option value="">Chọn lĩnh vực</option>';
                            $.each(e.classify , function (key2, value2){
                                $string2 += '<option value="'+value2.id+'">'+value2.name+'</option>';
                            });
                            $(".classify").html('').append($string2);

                            // if (e.package == 'VIECLAMCUNG' || e.package == 'VIECLAMCCAU'){
                            //     $(".checkJobPrice").addClass('none').removeClass('block');
                            //     $(".checkJobWage").removeClass('none').addClass('block');
                            // }else{
                            //     $(".checkJobWage").addClass('none').removeClass('block');
                            //     $(".checkJobPrice").removeClass('none').addClass('block');
                            // }
                        }
                        $('#profileDemandSources').bootstrapValidator('updateMessage','exigency','blank','Mời bạn chọn nhu cầu').bootstrapValidator('updateStatus','exigency','INVALID','blank');
                        $('#profileDemandSources').bootstrapValidator('updateMessage','classify','blank','Mời bạn chọn lĩnh vực').bootstrapValidator('updateStatus','classify','INVALID','blank');


                    }
                });
            }).on('keyup change', '.checkJobPrice', function (e) {

                e.preventDefault();
                $checkPrice = $(".checkPrice").val();
                $denominations = $(".denomination option:selected").val();

                //format price
                var v = Number($checkPrice);
                if (isNaN(v)) {
                    return $checkPrice;
                }
                var sign = (v < 0) ? '-' : '';
                var res = Math.abs(v).toString().split('').reverse().join('').replace(/(\d{3}(?!$))/g, '$1,').split('').reverse().join('');
                $price = sign + res;
                //end

                $(".totalPrice").html('').append($price +' '+ $denominations);

                if(parseInt($checkPrice) == 0){
                    $('#postSupply').bootstrapValidator('updateMessage', 'price', 'blank', 'Bạn chưa nhập giá tiền').bootstrapValidator('updateStatus', 'price', 'INVALID', 'blank');
                }

            });
        });
    </script>
    @include('layouts.message')

@endsection
@section('content')
    {{--<div id="tallContent" style="cursor: default; display: none;">--}}
        {{--<img src="{{asset('/frontend/images/loading3.gif')}}" alt="">--}}
    {{--</div>--}}
    <section class="sectionSame">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="boxForm">
                        <p style="margin-top: 20px">Số điện thoại không hợp lệ khi : <br>
                            + Đã tồn tại hồ sơ<br>
                            + Chưa đăng kí dịch vụ<br>
                            + Đã hủy dịch vụ</p>
                        <div class="boxForm-title" style="text-align: center">
                            <h3>Hồ sơ vận tải</h3>
                        </div>
                        <p style="color: #fff; margin-bottom: 15px" class="btn btn-primary">Mã hồ sơ : <span style="color: #fff">{{!empty($post->post_code)?$post->post_code:''}}</span></p>
                        <form id="profileDemandSources" class="form-horizontal "
                              action="{{route('profile.editDemandSources')}}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name='postId' value="{{$postId}}">
                            <input type="hidden" name='sdt' value="{{$sdt}}">

                            <div class="box-body clearfix">
                                <div class="col-m-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Họ và tên/Tên công ty :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nip_name" value="{{$userMeta['_nip_name']}}"
                                                   placeholder="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"> Địa chỉ liên lạc :</label>
                                        <div class="col-sm-9">
                                            <textarea rows="4" cols="50" class="form-control"
                                                      name="nip_address">{{$userMeta['_nip_address']}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group selectContainer">
                                        <label class="col-sm-3 control-label">Gói dịch vụ :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control ckeckPackage " name="benefitPackage">
                                                <option value="">Chọn gói</option>
                                                <option value="{{$package->code}}" selected>{{Config::get('package.namePackageView.'.$package->code)}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group selectContainer">
                                        <label class="col-sm-3 control-label">Nhu cầu :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control exigency" name="exigency">
                                                <option value="">Chọn nhu cầu</option>
                                                @foreach($showMeta['exigency'] as $key => $value)
                                                    <option value="{{$value->id}}" {{$valueRelationships[Config::get('terms.' . $post->pkg_code . '.exigency')] == $value->id ? "selected" : ''}}>
                                                        {{$value->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group selectContainer">
                                        <label class="col-sm-3 control-label">Lĩnh vực/ Phân loại :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control classify" name="classify">
                                                <option value="">Chọn lĩnh vực</option>
                                                @foreach($showMeta['classify'] as $key => $value)
                                                    <option value="{{$value->id}}" {{$valueRelationships[Config::get('terms.' . $post->pkg_code . '.classify')] == $value->id ? "selected" : ''}}>
                                                        {{$value->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group checkJobPrice ">

                                        <label for="" class="col-sm-3 control-label">Giá :</label>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-xs-8" style="padding-right: 0">
                                                    <input type="text" class="form-control price checkPrice" name="price" placeholder="VD: 6.000.000" value="{{$price}}"
                                                           autocomplete="off">
                                                </div>
                                                <div class="col-xs-4">
                                                    <select class="form-control denomination" name="denominations" style="padding-right: 0px">
                                                        <option selected value="Đồng">Đồng</option>
                                                        <option  value="Triệu">Triệu</option>
                                                        <option value="Tỷ">Tỷ</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <p style="color: #1c7430;margin-top: 3px;">Tổng giá tiền: <span class="totalPrice">{{$price}} Đồng</span></p>
                                        </div>
                                    </div>

                                    <div class="form-group selectContainer">
                                        <label class="col-sm-3 control-label">Địa điểm tìm kiếm :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="nip_province">
                                                <option value="">Chọn địa điểm</option>
                                                @foreach($province as $key => $value)
                                                    <option value="{{$value['id']}}" {{$valueRelationships['nip_province'] == $value['id']? "selected" :''}}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!-- /.box-body -->
                            <div class="box-footer text-center" style="background: transparent">
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection