@extends('layouts.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <edit-news ref="createNews"></edit-news>
    </section>
@endsection

@push('json_var')
    window.article = {!! json_encode($article) !!}
@endpush