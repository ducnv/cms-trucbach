@if ($paginator->hasPages())
    <div style="float: right" class='paginator '>
        {{-- Previous Page Link --}}
        <ul>
            {{--@if ($paginator->onFirstPage())--}}
            {{--<li class="disabled"><span>&laquo;</span></li>--}}
            {{--@else--}}
            {{--<li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>--}}
            {{--@endif--}}

            {{-- Pagination Elements --}}
            <li><span>Trang: </span></li>
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li><a href="#" class="active"><span>{{ $page }}</span></a></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach


            {{-- Next Page Link --}}
            {{--@if ($paginator->hasMorePages())--}}
            {{--<li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>--}}
            {{--@else--}}
            {{--<li class="disabled"><span>&raquo;</span></li>--}}
            {{--@endif--}}
        </ul>
    </div>
@endif
<style>
    .paginator li {
        float: left;
    }
</style>
