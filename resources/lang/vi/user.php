<?php

return [
    'user' => 'Người dùng',
    'manage_user' => 'Quản lý người dùng',
    'browse_user' => 'Danh sách người dùng',
    'create_user' => 'Tạo người dùng mới',
    'update_user' => 'Chỉnh sửa',
    'deactive_user' => 'Vô hiệu hóa',
];