<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => 'Đăng nhập',
    'forgot_password' => 'Quên mật khẩu?',
    'remember_me' => 'Nhớ trạng thái',
    'login_with_google' => 'Đăng nhập với Google+',
    'password' => 'Mật khẩu',
    'password_confirmation' => 'Xác thực mật khẩu',
    'email' => 'Địa chỉ email',
    'login_msg' => 'Đăng nhập để bắt đầu sử dụng',
    'reset_password' => 'Lấy lại mật khẩu',
];
